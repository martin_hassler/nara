Nara is an implementation of a classical Heisenberg code with different types of interactions based on 
Monte Carlo methods (Metropolis algorithm).

It allows the simulation of materials with an arbitrary crystal structure. 
Beyond the isotropic exchange term, the implemented Hamiltonian is capable of treating single ion anisotropy, 
Zeeman energy and some particular types of anisotropic exchange. 

Nara can use either "SI"-ish units like eV an K or natural units.

The project is written in modern Fortran.

Nara stands for Not a real acronym for my project. Obviously it is not an acronym but it is
a backronym.

I will update this Readme and make it prettier as I have time. 
For now everything on how to use Nara is explained in the manual. There is even a tutorial... yay

Have fun.