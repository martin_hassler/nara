!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Module containing general math routines
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module NaraMath

    use MHKonstanten

    implicit none

    private
    public:: ConvPlane3PHesse


    contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Solving
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        !Converts definition of Plane from 3 Points to Hesse normal form
        !https://de.wikipedia.org/w/index.php?title=Normalenform&oldid=182451932#Aus_der_Dreipunkteform
        !https://en.wikipedia.org/w/index.php?title=Plane_(geometry)&oldid=889307189#Point-normal_form_and_general_form_of_the_equation_of_a_plane
        subroutine ConvPlane3PHesse(dots,para)
            real(kind=KPosi),dimension(3,3),intent(in)::dots!dots defining 3d Plane
            real(kind=KCalc),dimension(4),intent(out)::para!parameters a,b,c,d for the hesse normal form n*x+d=0 n=(a;b;c)

            real(kind=KCalc),dimension(3)::normVC,normVD
            real(kind=KCalc)::length

            !n=(q-p)x(r-p)
            !d=-n\cdot p
            normVC=real(dots(:,2)-dots(:,1),KCalc)
            normVD=real(dots(:,3)-dots(:,1),KCalc)

            para(1)=(normVC(2)*normVD(3)-normVC(3)*normVD(2))
            para(2)=(normVC(3)*normVD(1)-normVC(1)*normVD(3))
            para(3)=(normVC(1)*normVD(2)-normVC(2)*normVD(1))
            length=dot_product(para(:3),para(:3))
            para(:3)=para(:3)/sqrt(length)
            para(4)=-dot_product(para(:3),real(dots(:,1),KCalc))
        end subroutine ConvPlane3PHesse


end module NaraMath
