!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Contains the used Subroutines except for Input and Output operations
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module NaraRoutines
    use MHKonstanten
    use NaraTypes
    use NaraError
    use NaraMath
    use NaraPRNG

    implicit none

    public

    contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Allocate Memory
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        subroutine AllocPara(maxDataPoint)!Allocates Para, using the maximum number of steps and initializes it with 0.0
            use NaraData, only:PData,Para
            integer(kind=KQInd),intent(out)::maxDataPoint!number of maximum Data Points .... largest %ndata in PData
            integer(kind=KDefI)::i,k, allocstat

            !get maximum number of steps
            k=size(PData)
            maxDataPoint=0
            do i=1,k
                if (maxDataPoint<PData(i)%ndata) maxDataPoint=PData(i)%ndata
            end do

            !allocate and initialize
            allocate(Para(4,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating Para")
            Para=0.0_KCalc
        end subroutine AllocPara

        subroutine AllocObs(maxDataPoint)!Allocates memory for observables
            use NaraData,only:ANum,NMCSteps,CData
            use NaraData,only:AccSteps
            use NaraData,only:Ener,MeanEner
            use NaraData,only:SpecHeat,Suscept
            use NaraData,only:Mag,MeanMag,MeanTotMag
            use NaraData,only:StagMag,MeanStagMag,StagSuscept
            use NaraData,only:HMomEner,HMomMag
            use NaraData,only:BinderEner,BinderStagMag,BinderTotMag
            integer(kind=KQInd),intent(in)::maxDataPoint!number of maximum Data Points.... largest %ndata in PData
            integer(kind=KDefI)::allocstat
            !Allocate Energy observables:

            !allocate and initialize
            allocate(NMCSteps(2,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating NMCSteps")
            NMCSteps=0
            allocate(AccSteps(4,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating AccSteps")
            AccSteps=0.0_KCMin

            allocate(Ener(0:ANum,CData%NAver),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating Ener")
            Ener=0.0_KCalc
            allocate(MeanEner(0:ANum,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating MeanEner")
            MeanEner=0.0_KCalc
            allocate(HMomEner(0:ANum,4),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating HMomEner")
            HMomEner=0.0_KCalc

            allocate(SpecHeat(0:ANum,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating SpecHeat")
            SpecHeat=0.0_KCalc

            allocate(HMomMag(0:ANum,4),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating HMomMag")
            HMomMag=0.0_KCalc
            allocate(MeanTotMag(0:ANum,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating MeanTotMag")
            MeanTotMag=0.0_KCalc
            allocate(Suscept(0:ANum,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating Suscept")
            Suscept=0.0_KCalc

            allocate(MeanStagMag(maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating MeanStagMag")
            MeanStagMag=0.0_KCalc
            allocate(StagSuscept(maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating StagSuscept")
            StagSuscept=0.0_KCalc

            allocate(Mag(3,0:ANum,CData%NAver),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating Mag")
            Mag=0.0_KCalc
            allocate(StagMag(3,CData%NAver),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating StagMag")
            StagMag=0.0_KCalc
            allocate(MeanMag(3,0:ANum,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating MeanMag")
            MeanMag=0.0_KCalc
            allocate(BinderEner(0:ANum,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating BinderEner")
            BinderEner=0.0_KCalc
            allocate(BinderTotMag(0:ANum,maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating BinderTotMag")
            BinderTotMag=0.0_KCalc
            allocate(BinderStagMag(maxDataPoint),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating BinderStagMag")
            BinderStagMag=0

        end subroutine AllocObs

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Initialization Subroutines and other stuff
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !Inits the spin direction &calculates the energy of the spins for the first time
        subroutine InitSpins(modus)
            use NaraData,only:PNum,SpinP,SData,AuxSpin
            integer(kind=KDefI),intent(in)::modus!default: z-aligned; aligned states: 1(x),2(y),3(z),4(-x),5(-y),6(-z); random states 7(+-x),8(+-y),9(+-z); 10: random in 4Pi

            integer(kind=KDefI)::i
            real(kind=KSDir)::sdir
            real(kind=KSDir),dimension(3)::spindir

            call WriteLogAndStdO("Initializing Spins")
            call WriteLogNoAdv("Used Method: ")

            select case (modus)
                case(1)!aligned in +x
                    call WriteLog("Aligned in +x")
                    SpinP(:)%sDir(1)=1.0_KSdir
                    SpinP(:)%sDir(2)=0.0_KSdir
                    SpinP(:)%sDir(3)=0.0_KSdir
                case(2)!aligned in +y
                    call WriteLog("Aligned in +y")
                    SpinP(:)%sDir(1)=0.0_KSdir
                    SpinP(:)%sDir(2)=1.0_KSdir
                    SpinP(:)%sDir(3)=0.0_KSdir
                case(3)!aligned in +z
                    call WriteLog("Aligned in +z")
                    SpinP(:)%sDir(1)=0.0_KSdir
                    SpinP(:)%sDir(2)=0.0_KSdir
                    SpinP(:)%sDir(3)=1.0_KSdir
                case(4)!aligned in -x
                    call WriteLog("Aligned in -x")
                    SpinP(:)%sDir(1)=-1.0_KSdir
                    SpinP(:)%sDir(2)=0.0_KSdir
                    SpinP(:)%sDir(3)=0.0_KSdir
                case(5)!aligned in -y
                    call WriteLog("Aligned in -y")
                    SpinP(:)%sDir(1)=0.0_KSdir
                    SpinP(:)%sDir(2)=-1.0_KSdir
                    SpinP(:)%sDir(3)=0.0_KSdir
                case(6)!aligned in -z
                    call WriteLog("Aligned in -z")
                    SpinP(:)%sDir(1)=0.0_KSdir
                    SpinP(:)%sDir(2)=0.0_KSdir
                    SpinP(:)%sDir(3)=-1.0_KSdir
                case(7)!random in +-x
                    call WriteLog("Random +-x")
                    SpinP(:)%sDir(2)=0.0_KSdir
                    SpinP(:)%sDir(3)=0.0_KSdir
                    do i=1,PNum
                        call GetReal32(sdir)
                        SpinP(i)%sDir(1)=sign(1.0_KSdir,sdir-0.5_KSDir)
                    end do
                case(8)!random in +-y
                    call WriteLog("Random +-y")
                    SpinP(:)%sDir(1)=0.0_KSdir
                    SpinP(:)%sDir(3)=0.0_KSdir
                    do i=1,PNum
                        call GetReal32(sdir)
                        SpinP(i)%sDir(2)=sign(1.0_KSdir,sdir-0.5_KSDir)
                    end do
                case(9)!random in +-z
                    call WriteLog("Random +-z")
                    SpinP(:)%sDir(1)=0.0_KSdir
                    SpinP(:)%sDir(2)=0.0_KSdir
                    do i=1,PNum
                        call GetReal32(sdir)
                        SpinP(i)%sDir(3)=sign(1.0_KSdir,sdir-0.5_KSDir)
                    end do
                case(10)!random
                    call WriteLog("Random on Sphere")
                    do i=1,PNum
                        call CalcRandVoSp(SpinP(i)%sDir)
                        SpinP(i)%sDir=SpinP(i)%sDir
                    end do
                case(11)!According to sub lattice +-x
                    call WriteLog("According to sub lattice +-x")
                    do i=1,PNum
                        if(SpinP(i)%subLat) then
                            SpinP(i)%sDir=(/1.0_KSdir,0.0_KSdir,0.0_KSdir/)
                        else
                            SpinP(i)%sDir=(/-1.0_KSdir,0.0_KSdir,0.0_KSdir/)
                        end if
                    end do
                case(12)!According to sub lattice +-y
                    call WriteLog("According to sub lattice +-y")
                    do i=1,PNum
                        if(SpinP(i)%subLat) then
                            SpinP(i)%sDir=(/0.0_KSdir,1.0_KSdir,0.0_KSdir/)
                        else
                            SpinP(i)%sDir=(/0.0_KSdir,-1.0_KSdir,0.0_KSdir/)
                        end if
                    end do
                case(13)!According to sub lattice +-z
                    call WriteLog("According to sub lattice +-z")
                    do i=1,PNum
                        if(SpinP(i)%subLat) then
                            SpinP(i)%sDir=(/0.0_KSdir,0.0_KSdir,1.0_KSdir/)
                        else
                            SpinP(i)%sDir=(/0.0_KSdir,0.0_KSdir,-1.0_KSdir/)
                        end if
                    end do
                case(14)!+sub lattice direction
                    call WriteLog("According to the specified crystal directions")
                    call ConvCrystDir(SData%sublatSpindir,SData%uCVectors,spindir)
                    do i=1,PNum
                        SpinP(i)%sDir=spindir
                    end do
                case(15)!+-sub lattice direction
                    call WriteLog("According to the specified crystal directions,"//&
                    &" inverted for each sublattice")
                    call ConvCrystDir(SData%sublatSpindir,SData%uCVectors,spindir)
                    do i=1,PNum
                        if(SpinP(i)%subLat) then
                            SpinP(i)%sDir=spindir
                        else
                            SpinP(i)%sDir=-spindir
                        end if
                    end do
                case(16)!copy atom specific direction from Snara
                    call WriteLog("According atom specified direction in Snara")
                    do i=1,PNum
                        SpinP(i)%sDir=SData%aData(AuxSpin(i)%aDatInd)%defSdir
                    end do
                case(17)!copy atom specific direction from Snara, Invert for each sub lattice
                    call WriteLog("According atom specified direction in Snara,"//&
                    &" inverted for other sub lattice")
                    do i=1,PNum
                        if(SpinP(i)%subLat) then
                            SpinP(i)%sDir=SData%aData(AuxSpin(i)%aDatInd)%defSdir
                        else
                            SpinP(i)%sDir=-SData%aData(AuxSpin(i)%aDatInd)%defSdir
                        end if
                    end do
                case default!alligned in +z
                    call WriteLogAndStdO("Default: Aligned +z")
                    SpinP(:)%sDir(1)=0.0_KSdir
                    SpinP(:)%sDir(2)=0.0_KSdir
                    SpinP(:)%sDir(3)=1.0_KSdir
            end select
        end subroutine InitSpins


    !Generates list of all Neighbouring Atoms of sort aSort within the spherical shell defined by rad around the Atom with index aInd
    subroutine GetNeighR()
        use NaraData,only: SData,IData

        integer(kind=KInde):: aInd!atoms whose neighbour we are looking for
        type(IntSh)::twopart!two particle interaction
        integer(kind=KDefI),dimension(3)::minNCells!minimum number of necessary neighbour cells
        real(kind=KPosi),dimension(6)::distSurf
        real(kind=KPosi),dimension(3,3)::nVect
        real(kind=KPosi),dimension(3)::apos,testPos
        real(kind=KPosi),dimension(2)::rad2!squared radii
        real(kind=KPosi):: dist
        integer(kind=KInde),allocatable,dimension(:,:):: neighAt,neighAtCopy
        integer(kind=KDefI):: i,j,jj,k,l,nInde,maxInde
        logical(kind=KDefL),dimension(3):: modcells

        call WriteIPartnerHeader()!write header for Log file

        call GetSurfVect(SData,nVect)!Calculate surface vectors

        do aInd=1,SData%nAtoms!loop over all atoms
            do jj=1,size(IData(SData%aData(aInd)%iaSort)%twopart)!loop over all interactions
                twopart=IData(SData%aData(aInd)%iaSort)%twopart(jj)

                rad2=twopart%rad**2

                !get minimum number of necessary neighbour cells
                minNCells=1
                apos=SData%aData(aInd)%aPos+SData%uCOffset
                do
                    modcells=.FALSE.
                    call GetDistSurf(SData,(2*minNCells-1),apos,distSurf,nVect)
                    do i=1,3
                        if(distSurf(i)<0.0_KPosi) call HCFR(101,2,"Either, you made a mistake and one atom"&
                        &//" is defined to be outside of the unitcell, or the UC vectors are not defined the"&
                        &//" way I expect them")
                        if(distSurf(3+i)<0.0_KPosi) call HCFR(101,2,"Either, you made a mistake and one atom"&
                        &//" is defined to be outside of the unitcell, or the UC vectors are not defined the"&
                        &//" way I expect them")
                        if(distSurf(i)<twopart%rad(2)) modcells(i)=.TRUE.
                        if(distSurf(3+i)<twopart%rad(2)) modcells(i)=.TRUE.
                    end do

                    do i=1,3
                        if(modcells(i)) minNCells(i)=minNCells(i)+1!expand if necessary
                    end do
                    if(modcells(1).OR.modcells(2).OR.modcells(3)) then
                        apos=SData%aData(aInd)%aPos+(minNCells(1)-1)*SData%uCVectors(:,1)&
                        &+(minNCells(2)-1)*SData%uCVectors(:,2)+(minNCells(3)-1)*SData%uCVectors(:,3)&
                        &+SData%uCOffset!calculate new position in test supercell
                        cycle
                    else
                        exit !if no further expansion needed exit
                    end if
                end do

                !calculate maximum possible number of neighbour atoms and allocate array
                maxInde=product(2*minNCells-1)*SData%nAtoms
                nInde=1
                allocate(neighAt(maxInde,5))
                neighAt=0
                minNCells=minNCells-1!get symmetric numbering around 0
                Typeloop: do l=1,SData%nAtoms
                    if(SData%aData(l)%iaSort/=twopart%iaSort) cycle Typeloop !not an interaction partner
                    !loop over all Unit cells
                    do k=-minNCells(3),minNCells(3)
                        do j=-minNCells(2),minNCells(2)
                            do i=-minNCells(1),minNCells(1)
                                testPos=SData%aData(l)%aPos+i*SData%uCVectors(:,1)+j*SData%uCVectors(:,2)&
                                &+k*SData%uCVectors(:,3)-SData%aData(aInd)%aPos

                                dist=dot_product(testPos,testPos)
                                if(dist<rad2(1))cycle
                                if(dist>rad2(2))cycle
                                neighAt(nInde,1)=i
                                neighAt(nInde,2)=j
                                neighAt(nInde,3)=k
                                neighAt(nInde,4)=l
                                nInde=nInde+1
                                call WriteIPartnerLog(aInd,i,j,k,l,sqrt(dist))!write finding to log file
                            end do
                        end do
                    end do
                end do Typeloop
                neighAt(:,5)=twopart%eInd
                nInde=nInde-1
                if(nInde>maxInde) then
                    call HCFR(-1,3,"")
                end if
                if(allocated(SData%aData(aInd)%neighAt)) then!add found interaction partners to already present list
                    maxInde=size(SData%aData(aInd)%neighAt,1)
                    allocate(neighAtCopy(maxInde+nInde,5))
                    neighAtCopy(:maxInde,:)=SData%aData(aInd)%neighAt
                    neighAtCopy(maxInde+1:,:)=neighAt(:nInde,:)
                    call move_alloc(neighAtCopy,SData%aData(aInd)%neighAt)
!                    deallocate(neighAtCopy) !not needed since move_alloc should deallocate "From" argument
                else
                    allocate(SData%aData(aInd)%neighAt(nInde,5))
                    SData%aData(aInd)%neighAt=neighAt(:nInde,:)
                end if

                deallocate(neighAt)
            end do
        end do
    end subroutine GetNeighR

        !generates Simulation structure
        !additional allocates Spin and AuxSpin array
        !TODO Add Comments make it understandable.
        !TODO remove BC from routine arguments
        subroutine CreateCryst(bCs)
            use NaraData,only:PNum,ANum,APSNum,SpinP,AuxSpin,IData,CData,SData
            integer(kind=KDefI),dimension(3),intent(in)::bCs!boundary Conditions

            integer(kind=KDefI)::g,h,i,j,k,l
            integer(kind=KInde)::inde,neighInde,num
            integer(kind=KInde),dimension(5):: neighAt
            integer(kind=KInde),allocatable,dimension(:,:):: indListCopy
            logical(kind=KDefL)::cyc,sublat,selfinter
            logical(kind=KDefL),dimension(3)::perio!periodicity for CrystPlanesSubl

            call WriteLogAndStdO("Assembling the Crystal")

            allocate(APSNum(0:ANum))

            allocate(SpinP(PNum),AuxSpin(PNum))
            APSNum=0
            APSNum(0)=PNum
            sublat=.FALSE.
            selfinter=.FALSE.

            if(CData%subLatgen>=4) call CrystPlanesSubl(perio)!choose subklattice generation based on crystal planes

            do k=1,SData%NCells(3)
                do j=1,SData%NCells(2)
                    do i=1,SData%NCells(1)
                        do l=1,SData%nAtoms
                            inde=SData%nAtoms*(i+(j-1)*SData%NCells(1)&
                            &+(k-1)*SData%NCells(1)*SData%NCells(2)-1)+l

                            AuxSpin(inde)%partPos=SData%uCOffset+SData%aData(l)%aPos&
                            &+(i-1)*SData%uCVectors(:,1)+(j-1)*SData%uCVectors(:,2)&
                            &+(k-1)*SData%uCVectors(:,3)

                            AuxSpin(inde)%aDatInd= l

                            SpinP(inde)%iaSort=SData%aData(l)%iaSort

                            APSNum(SpinP(inde)%iaSort)=APSNum(SpinP(inde)%iaSort)+1!count atom types

                            SpinP(inde)%anzNeigh=size(SData%aData(l)%neighAt,1)
                            allocate(SpinP(inde)%indList(2,SpinP(inde)%anzNeigh))
                            num=0
                            NeighList: do h=1,SpinP(inde)%anzNeigh
                                neighAt=SData%aData(l)%neighAt(h,:)

                                !handle Boundary conditions
                                call HandleBC(bCs,i,j,k,neighAt,cyc)
                                if(cyc) cycle NeighList

                                !calculate index of interaction partner
                                neighInde=SData%nAtoms*(i+neighAt(1)&
                                &+(j+neighAt(2)-1)*SData%NCells(1)&
                                &+(k+neighAt(3)-1)*SData%NCells(1)*SData%NCells(2)-1)&
                                &+neighAt(4)

                                if(neighInde==inde) selfinter=.TRUE. !warning Self Interaction

                                do g=1,num !Force minimum image convention...sort of... more like ensure that there are no double entries
                                    if((SpinP(inde)%indList(1,g)==neighInde).AND.&
                                    &SpinP(inde)%indList(2,num)==neighAt(5)) cycle NeighList!already interacting with that particle with the same interaction matrix
                                end do

                                !add new Neighbour to list
                                num=num+1
                                SpinP(inde)%indList(1,num)=neighInde
                                SpinP(inde)%indList(2,num)=neighAt(5)
                            end do NeighList

                            if(num<SpinP(inde)%anzNeigh)then!if necessary shortn list
                                allocate(indListCopy(2,num))
                                indListCopy=SpinP(inde)%indList
                                deallocate(SpinP(inde)%indList)
                                allocate(SpinP(inde)%indList(2,num))
                                SpinP(inde)%indList=indListCopy
                                deallocate(indListCopy)
                                SpinP(inde)%anzNeigh=num
                            end if

                            !copy additional interaction data
                            SpinP(inde)%pInde=IData(SpinP(inde)%iaSort)%pInde

                            !determine Sub-lattice
                            select case(CData%subLatgen)
                                case(1)!invert neighbouring cells
                                    !.NEQV. = XOR
                                    sublat=btest(i,0).NEQV.btest(j,0).NEQV.btest(k,0)
                                    SpinP(inde)%subLat=SData%aData(l)%subLat.NEQV.sublat
                                case(2)!invert each atom
                                    SpinP(inde)%subLat=sublat
                                    sublat=.NOT.sublat!invert
                                case(3)!invert each unit cell
                                    sublat=btest(i,0).NEQV.btest(j,0).NEQV.btest(k,0)
                                    SpinP(inde)%subLat=sublat
                                case(4)!sublattice based on crystal plane
                                    sublat=perio(1).AND.(.NOT.btest(i,0))!invert and even uc index
                                    sublat=sublat.NEQV.(perio(2).AND.(.NOT.btest(j,0)))!invert and even uc index
                                    sublat=sublat.NEQV.(perio(3).AND.(.NOT.btest(k,0)))!invert and even uc index
                                    SpinP(inde)%subLat=SData%aData(l)%subLat.NEQV.sublat
                                case default!copy data from input
                                    SpinP(inde)%subLat=SData%aData(l)%subLat
                            end select
                        end do
                    end do
                end do
            end do

            if(selfinter) call UMsg(4,0,"") !inform user that at least one case with self interaction occured

        end subroutine CreateCryst

        subroutine CrystPlanesSubl(perio)!generate sub lattice based on supplied miller indices... modifies sDat%aData(j)%subLat !FIXME VERFY THAT it is sound
            use NaraData,only:SData
            logical(kind=KDefL),dimension(3),intent(out):: perio!.T. if to be periodic sublat has to be inverted along this axis for each new Unit cell

            integer(kind=KDefI)::i,j,k
            integer(kind=KDefI),dimension(3)::miller
            real(kind=KPosi),dimension(3,3)::pPoints!plane defining points
            real(kind=KPosi),dimension(3)::ePointCryst!relative endpoint of unit cell
            real(kind=KPosi),dimension(3)::oPointCryst!relative point of origin of unitcell
            real(kind=KCalc),dimension(4)::para!parameters of plane a,b,c,d nx+d=0
            real(kind=KCalc)::distO,distE,dist!distances of Origin end Point and Point to plane

            miller=SData%sublatPlane
            perio=btest(miller,0)!if miller index is odd we must invert for periodicity

            !Get Miller indices equal to 0
            !and get first plane defining points
            !and get relative point of origin and endpoint (changes for negative indices)

            k=0!number of miller indices =0
            ePointCryst=0.0_KPosi!relative endpoint of unitcell
            oPointCryst=0.0_KPosi!relative point of origin
            pPoints=0.0_KPosi!plane defining points

            do i=1,3
                if(miller(i)>0) then!positive miller indix
                    pPoints(:,i)=SData%uCVectors(:,i)/miller(i)
                    ePointCryst=ePointCryst+SData%uCVectors(:,i)!endpoint of unit cell
                elseif(miller(i)==0) then!miller indices == 0
                    k=k+1!number of indices == 0
                    j=i!last index == 0
                    ePointCryst=ePointCryst+SData%uCVectors(:,i)!endpoint of unit cell
                else !negative miller index
                    pPoints(:,i)=SData%uCVectors(:,i)/miller(i)
                    oPointCryst=oPointCryst+SData%uCVectors(:,i)
                end if
            end do

            !handle cases with miller==0
            if(k==1)then!one index == 0... plane parallel to one axes
                if(j==1)then
                    pPoints(:,1)=pPoints(:,3)+SData%uCVectors(:,1)
                else
                    pPoints(:,j)=pPoints(:,j-1)+SData%uCVectors(:,j)
                end if
            elseif(k==2) then!two indices ==0 ... plane parallel to two axes
                do i=1,3!get index / =0
                    if(miller(i)/=0) then
                        j=i
                        exit
                    end if
                end do
                if(j==1) then
                    pPoints(:,2)=pPoints(:,1)+SData%uCVectors(:,2)
                    pPoints(:,3)=pPoints(:,1)+SData%uCVectors(:,3)
                elseif(j==2) then
                    pPoints(:,1)=pPoints(:,2)+SData%uCVectors(:,1)
                    pPoints(:,3)=pPoints(:,2)+SData%uCVectors(:,3)
                else
                    pPoints(:,2)=pPoints(:,3)+SData%uCVectors(:,2)
                    pPoints(:,1)=pPoints(:,3)+SData%uCVectors(:,1)
                end if
            elseif(k>=3) then!we are screwed this case should never happen
                call HCFR(-1,-1,"Miller Indices == 0")
            end if

            do i=1,3!add offset
                pPoints(:,i)=pPoints(:,i)+oPointCryst
            end do

            call ConvPlane3PHesse(pPoints,para)

            distO=dot_product(para(:3),real(oPointCryst,KCalc))+para(4)
            distE=dot_product(para(:3),real(ePointCryst,KCalc))+para(4)
            if(abs(distO)<=EpsKCalc) then
                !FIXME Origin part of plane
                distO=distE
            end if

            do i=1,SData%nAtoms
                dist=dot_product(para(:3),real(SData%aData(i)%aPos,KCalc))+para(4)!Calc distance to plane
                dist=dist/distO!distance from plane compared to distance from origin
                if(abs(dist-anint(dist))>=EpsKCalc) then!not part of sublattice
                    SData%aData(i)%subLat=.FALSE.
                else
                    SData%aData(i)%subLat=(1==(-1)**abs(nint(dist)))!every odd plane belongs to one sublattice
                end if
            end do
        end subroutine CrystPlanesSubl

        !Checks whether one has to apply BCs and modifies the indices of the unit cell of the interaction partner
        !negative BC: periodic only in one negative direction. supplied value is the index of the unit cell which we mirror...
        !default: no BC /open Edge
        !1: toroidal/periodic BC
        !TODO Simplify... use masks etc.; remove bCs argument and use already saved one.
        subroutine HandleBC(bCs,p1,p2,p3,neighAt,noInteraction)
            use NaraData,only: SData
            integer(kind=KDefI),dimension(3),intent(in)::bCs!boundary Conditions
            integer(Kind=KDefI),intent(in):: p1,p2,p3!current unit cells indices
            integer(kind=KInde),dimension(5),intent(inout):: neighAt! xyz index of cells + index of basis index 5 is not relevant(index of interaction matrix)
            logical(kind=KDefL),intent(out)::noInteraction!tell calling routine that no interaction with this particle

            noInteraction=.FALSE.
            do !loop to be able to handle multiple periodicity
                if((p1+neighAt(1))>SData%NCells(1))then !relative UC index is larger than total number of Unit Cells.
                    select case (bCs(1))! 1: Periodic BC; defaul : Open edge
                        case(1)
                            neighAt(1)=neighAt(1)-SData%NCells(1)!get new relative UC index
!                           neighAt(1)=p1+neighAt(1)-SData%NCells(1)!get new absolute UC index
                        case default
                            noInteraction=.TRUE.
                            return!Since particle is in periodic image we don't need further checks to see that we don't interact with it.
                    end select
                    if((p1+neighAt(1))>SData%NCells(1)) cycle! in case that one periodic image is not enough e.g.: interaction with the next nearest image

               elseif((p1+neighAt(1))<1)then!relative UC index is smaller than total number of Unit Cells.
                    select case (bCs(1))! 1: Periodic BC; defaul : Open edge
                        case(1)
                            neighAt(1)=neighAt(1)+SData%NCells(1)!get new relative UC index
!                           neighAt(1)=p1+neighAt(1)+SData%NCells(1)!get new absolute UC index
                        case default
                            noInteraction=.TRUE.
                            return!Since particle is in periodic image we don't need further checks to see that we don't interact with it.
                    end select
                    if((p1+neighAt(1))<1) cycle

!                else !No Else block since at least along this crystal axis particle is not in periodic image
                endif

                if((p2+neighAt(2))>SData%NCells(2))then!for comments see first if-block in loop
                    select case (bCs(2))
                        case(1)
                            neighAt(2)=neighAt(2)-SData%NCells(2)
                        case default
                            noInteraction=.TRUE.
                            return
                    end select
                    if((p2+neighAt(2))>SData%NCells(2)) cycle
               elseif((p2+neighAt(2))<1)then
                    select case (bCs(2))
                        case(1)
                            neighAt(2)=neighAt(2)+SData%NCells(2)
                        case default
                            noInteraction=.TRUE.
                            return
                    end select
                    if((p2+neighAt(2))<1)cycle
                endif

                if((p3+neighAt(3))>SData%NCells(3))then!for comments see first if-block in loop
                    select case (bCs(3))
                        case(1)
                            neighAt(3)=neighAt(3)-SData%NCells(3)
                        case default
                            noInteraction=.TRUE.
                            return
                    end select
                    if((p3+neighAt(3))>SData%NCells(3)) cycle
                elseif((p3+neighAt(3))<1)then
                    select case (bCs(3))
                        case(1)
                            neighAt(3)=neighAt(3)+SData%NCells(3)
                        case default
                            noInteraction=.TRUE.
                            return
                    end select
                    if((p3+neighAt(3))<1) cycle
                end if

                exit!no further adjustment of relative indices is needed.

            end do
        end subroutine HandleBC

        ! calculates the distances to the 6 Supercell surfaces of the crystal
        !positive if inside of parallelepiped
        subroutine GetDistSurf(sData,nCells,partPos,dist,nVect)
            type(StructData),intent(in):: sData!structure data
            integer(kind=KInde),dimension(3),intent(in)::nCells!Supercell size
            real(kind=KPosi),dimension(3),intent(in):: partPos!Particle position
            real(kind=KPosi),dimension(6),intent(out)::dist!distances to crystal surfaces: -yz,-xz,-xy,yz,xz,xy surfaces (for cubic) general: bc,ac,ab,-bc,-ac,-ab
            real(kind=KPosi),dimension(3,3),intent(in),optional::nVect!surface vectors

            real(kind=KPosi),dimension(3,3)::normVect!surface vectors
            logical(kind=KDefL)::opt

            opt=present(nVect)
            if(opt) then
                normVect=nVect
            else!if surface vectors are not supplied by caller Calculate surface vectors
                call GetSurfVect(sData,normVect)
            end if

            !surfaces that include the point of origin
            dist(1)=dot_product(normVect(:,1),partPos-sData%uCOffset)
            dist(2)=dot_product(normVect(:,2),partPos-sData%uCOffset)
            dist(3)=dot_product(normVect(:,3),partPos-sData%uCOffset)

            !opposite surfaces
            dist(4)=dot_product(normVect(:,1),sData%uCOffset+nCells(1)*sData%uCVectors(:,1)-partPos)
            dist(5)=dot_product(normVect(:,2),sData%uCOffset+nCells(2)*sData%uCVectors(:,2)-partPos)
            dist(6)=dot_product(normVect(:,3),sData%uCOffset+nCells(3)*sData%uCVectors(:,3)-partPos)
        end subroutine GetDistSurf

        !calculates the surface unit Vectors of the supplied structure
        subroutine GetSurfVect(sData,normVect)
            type(StructData),intent(in):: sData!structure data
            real(kind=KPosi),dimension(3,3),intent(out)::normVect!surface vectors

            normVect(1,1)=sData%uCVectors(2,2)*sData%uCVectors(3,3)-sData%uCVectors(3,2)*sData%uCVectors(2,3)
            normVect(2,1)=sData%uCVectors(3,2)*sData%uCVectors(1,3)-sData%uCVectors(1,2)*sData%uCVectors(3,3)
            normVect(3,1)=sData%uCVectors(1,2)*sData%uCVectors(2,3)-sData%uCVectors(2,2)*sData%uCVectors(1,3)

            normVect(1,2)=sData%uCVectors(2,3)*sData%uCVectors(3,1)-sData%uCVectors(3,3)*sData%uCVectors(2,1)
            normVect(2,2)=sData%uCVectors(3,3)*sData%uCVectors(1,1)-sData%uCVectors(1,3)*sData%uCVectors(3,1)
            normVect(3,2)=sData%uCVectors(1,3)*sData%uCVectors(2,1)-sData%uCVectors(2,3)*sData%uCVectors(1,1)

            normVect(1,3)=sData%uCVectors(2,1)*sData%uCVectors(3,2)-sData%uCVectors(3,1)*sData%uCVectors(2,2)
            normVect(2,3)=sData%uCVectors(3,1)*sData%uCVectors(1,2)-sData%uCVectors(1,1)*sData%uCVectors(3,2)
            normVect(3,3)=sData%uCVectors(1,1)*sData%uCVectors(2,2)-sData%uCVectors(2,1)*sData%uCVectors(1,2)

            normVect(:,1)=normVect(:,1)/sqrt(sum(normVect(:,1)**2))
            normVect(:,2)=normVect(:,2)/sqrt(sum(normVect(:,2)**2))
            normVect(:,3)=normVect(:,3)/sqrt(sum(normVect(:,3)**2))
        end subroutine GetSurfVect
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Finalization  Subroutines
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 ! TODO Check if everything is deallocated
        subroutine Finalize()!deallocates all allocated variables
            use NaraData,only: Para,PData,PDataOrig, APSNum ,CopySpinP
            use NaraData,only: Ener,MeanEner
            use NaraData,only: SpecHeat,Suscept
            use NaraData,only: Mag, MeanMag
            use NaraData,only: MeanTotMag
            use NaraData,only: NMCSteps,SpinP,AuxSpin,ExchMat,SIAMat,ASort
            use NaraData,only: IData,MagMom,LandeG
            use NaraData,only: AccSteps
            use NaraData,only: StagMag,MeanStagMag,StagSuscept
            use NaraData,only: HMomEner,HMomMag
            use NaraData,only: BinderEner,BinderStagMag,BinderTotMag
            integer(kind=KDefI)::dealloc_error

!Template for further use
!            dealloc_error=0
!            if(allocated()) deallocate(,stat=dealloc_error)
!            if(dealloc_error/=0) call UMsg(2,dealloc_error,"1")

            dealloc_error=0
            deallocate(Para,PData,PDataOrig,APSNum,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"2")

            dealloc_error=0
            deallocate(SpinP,AuxSpin,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"3")

            dealloc_error=0
            deallocate(Ener,MeanEner,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"4")

            dealloc_error=0
            deallocate(Mag,MeanMag,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"5")

            dealloc_error=0
            if(allocated(CopySpinP))deallocate(CopySpinP,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"6")

            dealloc_error=0
            deallocate(ExchMat,SIAMat,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"7")

            dealloc_error=0
            deallocate(ASort,IData,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"8")

            dealloc_error=0
            deallocate(NMCSteps,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"9")

            dealloc_error=0
            deallocate(LandeG,MagMom,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"10")

            dealloc_error=0
            deallocate(AccSteps,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"11")

            dealloc_error=0
            deallocate(SpecHeat,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"12")

            dealloc_error=0
            deallocate(Suscept,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"13")

            dealloc_error=0
            deallocate(MeanTotMag,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"14")

            dealloc_error=0
            deallocate(StagSuscept,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"15")

            dealloc_error=0
            deallocate(MeanStagMag,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"16")

            dealloc_error=0
            deallocate(StagMag,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"17")

            dealloc_error=0
            deallocate(HMomEner,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"18")

            dealloc_error=0
            deallocate(HMomMag,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"19")

            dealloc_error=0
            deallocate(BinderEner,BinderStagMag,BinderTotMag,stat=dealloc_error)
            if(dealloc_error/=0) call UMsg(2,dealloc_error,"20")

            call FinalizePRNG()

        end subroutine Finalize


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! MC Step
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        subroutine Thermalize(ntherm,sPara,accCount,nMCStep)
            use NaraData,only: CData

            integer(kind=KInde),intent(in)::ntherm !MC steps for datapoint
            real(kind=KCalc),dimension(4),intent(in)::sPara!Simulation Parameter
            integer(kind=KInde),dimension(3),intent(inout)::accCount!acceptance Counter for each Trial Step
            integer(kind=KDefI),intent(out)::nMCStep!number of MC steps used

            integer(kind=KDefI)::i,joey
            real(kind=KCalc),dimension(2*CData%NmAv+CData%NmAvDel)::obsASO!Array for moving average for single obs
            real(kind=KCalc),dimension(2,2*CData%NmAv+CData%NmAvDel)::obsA!Array for moving average
            real(kind=KCalc)::observableSO,relChSO!moving average
            real(kind=KCalc),dimension(2)::movAv,movAv2,observable,relCh!moving average
            integer(kind=KDefI):: indOffsetMvAv,indOffsetMvAv2!index offset often used
            character(len=65)::wpara

            indOffsetMvAv=CData%NmAv+CData%NmAvDel
            indOffsetMvAv2=CData%NmAv+1


            do i=1,CData%NMinTherm
                call MCStepM(ntherm,sPara,accCount)
                nMCStep=CData%NMinTherm*ntherm
            end do

            select case(CData%autTherm)!what is the order parameter? !caution set to default value in input routine
                case(1)! considering only TotMag
                    joey=2*CData%NmAv+CData%NmAvDel

                    movAv=0.0_KCalc
                    obsASO=0.0_KCalc
                    relChSO=0.0_KCalc

                    do i=0,joey-1!prefill array
                        call MCStepM(ntherm,sPara,accCount)!calls the MC subroutine
                        call TCalcMagniMag(obsASO(joey-i))
                    end do

                    !calculate moving averages...
                    obsASO=obsASO/real(CData%NmAv,KCalc)
                    movAv(1)=sum(obsASO(:CData%NmAv))
                    movAv(2)=sum(obsASO(indOffsetMvAv+1:))

                    do i=joey+1,CData%NMaxTherm
                        relChSO=abs((movAv(1)-movAv(2))/movAv(2))

                        if(relChSO<CData%RelChOPa)then
                            nMCStep=(CData%NMinTherm+i)*ntherm
                            call WriteLogStdOTherm(sPara,i-1+CData%NMinTherm)!write Log Msg
                            return
                        end if

                        call MCStepM(ntherm,sPara,accCount)!calls the MC subroutine
                        call TCalcMagniMag(observableSO)
                        movAv(2)=movAv(2)+obsASO(indOffsetMvAv)-obsASO(joey)!new moving average 2
                        obsASO(2:)=obsASO(:joey-1)!shift the array
                        obsASO(1)=observableSO/real(CData%NmAv,KCalc)
                        movAv(1)=movAv(1)+obsASO(1)-obsASO(indOffsetMvAv2)!new moving average
                    end do

                    nMCStep=(CData%NMinTherm+CData%NMaxTherm)*ntherm
                    if(relChSO<CData%RelChOPa)then!reached goal at last step
                        call WriteLogStdOTherm(sPara,CData%NMaxTherm+CData%NMinTherm)!write Log Msg
                    else !did not reach goal
                        write(wpara,'(3(f0.5,X),f0.5)') sPara
        !                write(wpara,*) sPara
                        call WriteLogAndStdO("Stopped thermalization due to reaching the maximum number"//&
                        &" of steps, paramter-set: "//trim(adjustl(wpara)))
                    end if

                case(2)! considering only StagMag
                    joey=2*CData%NmAv+CData%NmAvDel

                    movAv=0.0_KCalc
                    obsASO=0.0_KCalc
                    relChSO=0.0_KCalc

                    do i=0,joey-1!prefill array
                        call MCStepM(ntherm,sPara,accCount)!calls the MC subroutine
                        call TCalcStagMag(obsASO(joey-i))
                    end do

                    !calculate moving averages...
                    obsASO=obsASO/real(CData%NmAv,KCalc)
                    movAv(1)=sum(obsASO(:CData%NmAv))
                    movAv(2)=sum(obsASO(indOffsetMvAv+1:))

                    do i=joey+1,CData%NMaxTherm
                        relChSO=abs((movAv(1)-movAv(2))/movAv(2))

                        if(relChSO<CData%RelChOPa)then
                            nMCStep=(CData%NMinTherm+i)*ntherm
                            call WriteLogStdOTherm(sPara,i-1+CData%NMinTherm)!write Log Msg
                            return
                        end if

                        call MCStepM(ntherm,sPara,accCount)!calls the MC subroutine
                        call TCalcStagMag(observableSO)
                        movAv(2)=movAv(2)+obsASO(indOffsetMvAv)-obsASO(joey)!new moving average 2
                        obsASO(2:)=obsASO(:joey-1)!shift the array
                        obsASO(1)=observableSO/real(CData%NmAv,KCalc)
                        movAv(1)=movAv(1)+obsASO(1)-obsASO(indOffsetMvAv2)!new moving average
                    end do

                    nMCStep=(CData%NMinTherm+CData%NMaxTherm)*ntherm
                    if(relChSO<CData%RelChOPa)then!reached goal at last step
                        call WriteLogStdOTherm(sPara,CData%NMaxTherm+CData%NMinTherm)!write Log Msg
                    else !did not reach goal
                        write(wpara,'(3(f0.5,X),f0.5)') sPara
        !                write(wpara,*) sPara
                        call WriteLogAndStdO("Stopped thermalization due to reaching the maximum number"//&
                        &" of steps, paramter-set: "//trim(adjustl(wpara)))
                    end if

                case(3)! considering Mag+StagMag
                    joey=2*CData%NmAv+CData%NmAvDel

                    movAv=0.0_KCalc
                    movAv2=0.0_KCalc
                    obsA=0.0_KCalc
                    relCh=0.0_KCalc

                    do i=0,joey-1!prefill array
                        call MCStepM(ntherm,sPara,accCount)!calls the MC subroutine
                        call TNewCalcSTMag(obsA(:,joey-i))
                    end do

                    !calculate moving averages...
                    obsA=obsA/real(CData%NmAv,KCalc)
                    movAv=sum(obsA(:,:CData%NmAv),2)
                    movAv2=sum(obsA(:,indOffsetMvAv+1:),2)

                    do i=joey+1,CData%NMaxTherm
                        relCh=abs((movAv-movAv2)/movAv2)

                        if((relch(1)<CData%RelChOPa).AND.(relCh(2)<CData%RelChOPa))then
                            nMCStep=(CData%NMinTherm+i)*ntherm
                            call WriteLogStdOTherm(sPara,i-1+CData%NMinTherm)!write Log Msg
                            return
                        end if

                        call MCStepM(ntherm,sPara,accCount)!calls the MC subroutine
                        call TNewCalcSTMag(observable)
                        movAv2=movAv2+obsA(:,indOffsetMvAv)-obsA(:,joey)!new moving average 2
                        obsA(:,2:)=obsA(:,:joey-1)!shift the array
                        obsA(:,1)=observable/real(CData%NmAv,KCalc)
                        movAv=movAv+obsA(:,1)-obsA(:,indOffsetMvAv2)!new moving average
                    end do

                    nMCStep=(CData%NMinTherm+CData%NMaxTherm)*ntherm
                    if((relch(1)<CData%RelChOPa).AND.(relCh(2)<CData%RelChOPa))then!reached goal at last step
                        call WriteLogStdOTherm(sPara,CData%NMaxTherm+CData%NMinTherm)!write Log Msg
                    else !did not reach goal
                        write(wpara,'(3(f0.5,X),f0.5)') sPara
        !                write(wpara,*) sPara
                        call WriteLogAndStdO("Stopped thermalization due to reaching the maximum number"//&
                        &" of steps, paramter-set: "//trim(adjustl(wpara)))
                    end if
                end select
        end subroutine Thermalize

        subroutine MCStepM(ntherm,sPara,accCount) !FIXME Loss of precission classical Metropolis algorithm
            use NaraData,only:PNum,SpinP
            integer(kind=KDefI),intent(in)::ntherm !Steps for thermalization
            real(kind=KCalc),dimension(4),intent(in)::sPara!Simulation System Parameter Temperature and magnetic field
            integer(kind=KInde),dimension(3),intent(inout)::accCount!acceptance Counter for each Trial Step

            integer(kind=KInde)::j,l
            integer(kind=KDefI)::k
            integer(kind=KInde),dimension(3)::cTrial
            real(kind=KSDir),dimension(3)::testSpin!calculated Testspin
            real(kind=KCalc)::dener,prob!DeltaEnergy, test probability

            do k=1, ntherm
                do l=1,PNum
                    call GetInt32UpBound(j,PNum)!NOTE same here might be better to reference to a general routine...
                    !Trial step
                    call TrialStep(testSpin,j,cTrial)!NOTE sweeping through lattice not random point picking
                    !Acceptance Test
                    call CalcHamDener(j,testSpin,sPara(2:),dener)
                    if(0.0_KCalc>dener) then
                        call GetReal64(prob)
                        if(exp(dener/sPara(1))<prob) cycle !discard new position
                    end if
                    !if accepted update properties
                    SpinP(j)%sDir=testSpin
                    accCount=accCount+cTrial
                end do
            end do
        end subroutine MCStepM


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Hamiltonian
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        !Calculates the energy of the Spin with index sIndex with a given Testspin and the resulting difference in the energy with the actual Spin direction
        !Full Hamiltonian: Energy of particle i: E_i=-S_i(H'+K_i S_i+\sum_{interaction partner: j}[J_{ij}S_j])
        !where[J_{ij}S_j]=[J_{ij}^x*S_j^x,J_{ij}^y*S_j^y,J_{ij}^z*S_J^z}] is a vector... Diaagonal Terms of full tensorial exchange
        !K_i is the full self interaction matrix
        subroutine CalcHamDener(sIndex,tSpin,hfield,dener)
            use NaraData,only:SpinP,ExchMat,SIAMat,LandeG
            integer(kind=KInde),intent(in):: sIndex!index of Spin
            real(kind=KSDir),dimension(3),intent(in)::tSpin!Testspin used to calculate Energy
            real(kind=KCalc),dimension(3),intent(in)::hfield!H field
            real(kind=KCalc),intent(out)::dener!calculated Energy difference

            integer(kind=KInde)::i,neI,exI,siaI
            real(kind=KCalc),dimension(3)::cEner!intermediate result of energy per dimension

            cEner=0.0_KCalc
            siaI=SpinP(sIndex)%pInde(1)!index for SIA Matrix
            do i=1,SpinP(sIndex)%anzNeigh! two particle interaction
                neI=SpinP(sIndex)%indList(1,i)!neighbour index
                exI=SpinP(sIndex)%indList(2,i)!interaction matrix with neighbour
                !H_{ij}=S_iM_{ij}S_j
                !ener=ener-dot_product(tSpin,matmul(ExchMat(exI)%exTen,SpinP(neI)%sDir))
                !reduced to isotropic for speed reasons
                cEner=cEner+ExchMat(exI)%exTen*SpinP(neI)%sDir
            end do
            cEner=cEner+hfield*LandeG(SpinP(sIndex)%iaSort)!NOTE maybe handle case when no unit conversion is needed

            !calculate ener of spin and delta ener
            dener=sum((tSpin-SpinP(sIndex)%sDir)*cEner)!exchange+Hfield

            if (siaI/=0) then!Full SIA when needed SIA index 0 == no SIA
                dener=dener+&
                sum(SIAMat(siaI)%intMat(1:3)*(tSpin(:)*tSpin(1)-SpinP(sIndex)%sDir(:)*SpinP(sIndex)%sDir(1)))+&
                sum(SIAMat(siaI)%intMat(4:5)*(tSpin(2:)*tSpin(2)-SpinP(sIndex)%sDir(2:)*SpinP(sIndex)%sDir(2)))+&
                SIAMat(siaI)%intMat(6)*(tSpin(3)**2-SpinP(sIndex)%sDir(3)**2)
            end if
        end subroutine CalcHamDener

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Moves %%revised all contained subroutines 26.04.2018
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        subroutine TrialStep(testSpin,sIndex,counter)!Picks one of the Trial Moves at Random
            use NaraData,only:CData
            real(kind=KSDir),dimension(3),intent(out)::testSpin!calculated Testspin
            integer(kind=KInde),intent(in)::sIndex!Index of SpinParticle
            integer(kind=KInde),dimension(3),intent(out)::counter!1.0 if corresponding Trialstep was chosen

            real(kind=KCalc)::prob

            call GetReal64(prob)
            if(prob<CData%wMFlip)then !select case with reals is not permitted
                counter(1)=1
                counter(2:)=0
                call MFlip(sIndex,testSpin)
            elseif(prob<CData%WMDevi+CData%wMFlip) then !FIXME add those probabilites somewhere... no need to add them at each call of this subroutine...
                counter(1)=0
                counter(2)=1
                counter(3)=0
                call MSmallDevi(sIndex,testSpin)
            else
                counter(:2)=0
                counter(3)=1
                call MRand(sIndex,testSpin)
            end if
        end subroutine TrialStep

        subroutine MFlip(sIndex,testSpin) !Move: Spin flip... Inversion
            use NaraData,only:SpinP
            integer(kind=KInde),intent(in):: sIndex!index of spin
            real(kind=KSDir),dimension(3),intent(out)::testSpin!calculated Testspin

            testSpin=-SpinP(sIndex)%sDir
        end subroutine MFlip


        subroutine MRand(sIndex,testSpin)!Move: Random Spin... preserving length of spin
            integer(kind=KInde),intent(in):: sIndex!index of spin,just here to keep an uniform interface
            real(kind=KSDir),dimension(3),intent(out)::testSpin!calculated Testspin

            call CalcRandVoSp(testSpin)
        end subroutine MRand

        ! may produce erroneous results if opening angle gets near to 90� or is larger than 90�!! Strange things start to happen  for angles larger than 45� (because norming can additionally flip the spin
        subroutine MSmallDevi(sIndex,testSpin)!Move: Small deviation... in cone around current direction
            use NaraData,only:SpinP,ConeRad
            integer(kind=KInde),intent(in):: sIndex!index of spin
            real(kind=KSDir),dimension(3),intent(out)::testSpin!calculated Testspin

            real(kind=KSDir),dimension(3)::rvect

            call CalcRandVoSp(rvect)

            testSpin=SpinP(sIndex)%sDir+rvect*ConeRad !add random vector on sphere surface with desired length.
            testSpin=testSpin/sqrt(sum(testSpin**2))!re-normalize
            !NOTE Maybe it is necessary to check if renormalization was successfully?
        end subroutine MSmallDevi

        !Algorithm from "Point Picking and Distributing on the Disc and Sphere" by Mary K Arthur
        subroutine CalcRandVoSp(rvect)!returns an equally distributed point on the unit sphere surface
            real(kind=KSDir),dimension(3),intent(out):: rvect

            real(kind=KSDir),dimension(2)::rpic
            real(kind=KSDir)::x22

            call GetReal32x2(rpic)!might not be a good idea to explicitly  require 32bit float... if I change that to a 64 bit at one point...
            x22=sqrt(1.0_KSDir-rpic(2)**2)

            rvect(1)=x22*cos(rpic(1))
            rvect(2)=x22*sin(rpic(1))
            rvect(3)=rpic(2)
        end subroutine CalcRandVoSp

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Math Stuff
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        subroutine ConvCrystDir(hkl,uCVectors,crystDir)!converts crystal direction in millerindices into unitvector in cartesian corrdinates
            integer(kind=KDefI),dimension(3),intent(in)::hkl
            real(kind=KPosi),dimension(3,3),intent(in)::uCVectors!unit cell vectors in Cartesian coordinates stored row-wise
            real(kind=KSDir),dimension(3),intent(out)::crystDir

            crystDir=0.0_KSDir

            crystDir=real(hkl(1)*uCVectors(:,1)+hkl(2)*uCVectors(:,2)+hkl(3)*uCVectors(:,3),KSDir)
            !norm
            crystDir=crystDir/sqrt(sum(crystDir**2))
        end subroutine ConvCrystDir


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! System Parameters
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !calculates the parameter (temperature and external field) steps and returns the corresponding array, minimal Temperature is T_1+\Delta T,
        subroutine CalcParaArray(pDataIni,pDataFin,para)
            type(ParaData),intent(in):: pDataIni,pDataFin! 1..initial 2.. Final
            real(kind=KCalc),dimension(:,:),intent(out)::para
            integer(kind=KDefI)::i

            do i=1, pDataFin%ndata
                para(1,i)=real(i,KCalc)*(pDataFin%temppoint-pDataIni%temppoint)/real(pDataFin%ndata,KCalc)&
                &+pDataIni%temppoint!T1+\Delta T to T2
!                para(1,i)=real(i-1,KCalc)*(pDataFin%temppoint-pDataIni%temppoint)/real(pDataFin%ndata-1,KCalc)&
!                &+pDataIni%temppoint!T1+ to T2

                para(2:4,i)=real(i,KCalc)*(pDataFin%hpoint-pDataIni%hpoint)/real(pDataFin%ndata,KCalc)&
                &+pDataIni%hpoint!H1+\Delta H to H2
            end do
        end subroutine CalcParaArray


        !Prepares unit conversion (if necessaty otherwise it initilizes Conversion factors so that they don't affect the outcome
        !performs unit conversion on PData...   T'[meV]=T[K]*k_B   H'[meV]=H[A/m]*mu_0*mu_B
        !and does other necessary preperations for the calculations with units (sets variables to correct values)
        subroutine InitUnitConversion()
            use NaraData,only:CData,PData,LandeG,MagMom,VolPSpin,SData,UCMagn,UCSuscept

            integer(kind=KDefI)::nPdata,i
            real(kind=KPosi),dimension(3)::vect

            if(CData%unitConv)then !Convert Units convert everything to meV
                !convert PData
                nPdata=size(PData)
                do i=1,nPdata
                    PData(i)%temppoint=KBoltzmEV*PData(i)%temppoint
                    PData(i)%hpoint=BmagnemEV*VacuumPer*PData(i)%hpoint
                end do
                !Init LandeG, since it is used only in CalcHam we can simplify multiplications there
                LandeG=LandeG*MagMom !g'[meV*m/A]=g*mu_B(i) with mu_B(i) in units of mu_B

            else!Don't convert units
                LandeG=1.0_KCalc
                MagMom=1.0_KCalc
            end if

            if(CData%siMagUnit) then !Si units for magnetic observables

                !calculate Unitcell volume
                !vect=a2 x a3
                vect(1)=SData%uCVectors(2,2)*SData%uCVectors(3,3)&
                &-SData%uCVectors(3,2)*SData%uCVectors(2,3)
                vect(2)=SData%uCVectors(3,2)*SData%uCVectors(1,3)&
                &-SData%uCVectors(1,2)*SData%uCVectors(3,3)
                vect(3)=SData%uCVectors(1,2)*SData%uCVectors(2,3)&
                &-SData%uCVectors(2,2)*SData%uCVectors(1,3)

                !Vol =a1* a2 x a3
                VolPSpin=dot_product(SData%uCVectors(:,1),vect)
                VolPSpin=real(SData%nAtoms,KCalc)/VolPSpin*1.0E30_KCalc!inverse Volume per spin in 1/m**3...converted from 1/ang**3

                UCMagn=BmagnetJ*VolPSpin
                UCSuscept=VacuumPer*BmagnemEV*BmagnetJ*VolPSpin
            else
                VolPSpin=1.0_KCalc
                UCMagn=1.0_KCalc
                UCSuscept=1.0_KCalc
            end if

        end subroutine InitUnitConversion



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Other Quantities  %%revised all contained subroutines 30.04.2018
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        subroutine ObsMag(dpi)!Calculates the Magnetization and staggered Magnetization of the system (0,:) and each atomsort (1:ANum,:)...not normalized
            use NaraData,only:PNum,ANum,SpinP,CData,MagMom,StagMag,Mag
            integer(kind=KQInd),intent(in)::dpi!datapoint index

            real(kind=KCalc),dimension(3,ANum)::msub1,msub2!magnetizations for each sublattice and each atomsort
            integer(kind=KInde)::i

            msub1=0.0_KCalc
            msub2=0.0_KCalc

            do i=1,PNum
                if(SpinP(i)%subLat)then!claculate staggered Magnetization
                    msub1(:,SpinP(i)%iaSort)=msub1(:,SpinP(i)%iaSort)+SpinP(i)%sDir
                else
                    msub2(:,SpinP(i)%iaSort)=msub2(:,SpinP(i)%iaSort)+SpinP(i)%sDir
                end if
            end do

            if(CData%unitConv)then
                do i=1,ANum
                    msub1(:,i)=msub1(:,i)*MagMom(i)
                    msub2(:,i)=msub2(:,i)*MagMom(i)
                end do
            end if

            Mag(:,1:,dpi)=msub1+msub2
            StagMag(:,dpi)=sum(msub1-msub2,2)

            Mag(:,0,dpi)=sum(Mag(:,1:ANum,dpi),2)!calc system magnetization
        end subroutine ObsMag


        subroutine OCalcMeanMag(ndata,ndataOld,dpi)!Calculate mean value of Magnetization
            use NaraData,only: ANum,Mag,MeanMag
            integer(kind=KQInd),intent(in)::ndata!number of DataPoints to add
            integer(kind=KQInd),intent(in)::ndataOld!already added Datapoints
            integer(kind=KQInd),intent(in)::dpi!DataPoint index

            integer(kind=KQInd)::i,ndataNew
            real(kind=KCalc),dimension(3,0:ANum)::deltaN
            ndataNew=ndataOld
            do i=1,ndata
                ndataNew=ndataNew+1
                deltaN=(Mag(:,:,i)-MeanMag(:,:,dpi))/ndataNew
                MeanMag(:,:,dpi)= MeanMag(:,:,dpi)+deltaN
            end do
        end subroutine OCalcMeanMag

        !normalizes Mean and Std and performes Unit Conversion if necessary
        !also calculates mean of the system and its STD
        subroutine ONormMag(ndata)
            use NaraData,only: CData,ANum,APSNum,UCMagn
            use NaraData,only: MeanMag

            integer(kind=KQInd),intent(in)::ndata
            integer(kind=KDefI)::i
            integer(kind=KQInd)::j

            !unit Conversion mean;multiply with for unitconversion necessary parameters
            if(CData%unitConv) then
                MeanMag(:,1:,:ndata)=MeanMag(:,1:,:ndata)*UCMagn  !multiply with mu_B/V_0
            end if


            do j=1,ndata
                MeanMag(:,0,j)=sum(MeanMag(:,1:,j),2)!calculate system Mean
                do i=0,ANum!normalize for particle number
                    MeanMag(:,i,j)=MeanMag(:,i,j)/real(APSNum(i),KCalc)
                end do
            end do

!            !unit Conversion std ;multiply with for unitconversion necessary parameters
!            if(CData%unitConv) then
!                StDMag(:,1:,:ndata)=StDMag(:,1:,:ndata)*UCMagn  !multiply with mu_B/V_0
!            end if
!
!            do j=1,ndata
!                StDMag(:,0,j)=sum(StDMag(:,1:,j),2)!calculate system STD
!                do i=0,ANum!normalize for particle number
!                    StDMag(:,i,j)=StDMag(:,i,j)/real(APSNum(i),KCalc)
!                end do
!            end do
        end subroutine ONormMag

        !Calculate higher statistical moments (2nd 3rd and 4th) of total Magnetization and its mean
        !! Title:     Computing skewness and kurtosis in one pass
        !! Author:    John D. Cook
        !! Date:      2013
        !! Availability: https://www.johndcook.com/blog/skewness_kurtosis/ (26.05.2019)
        !! original paper: P�bay: Formulas for robust, one-pass parallel computation of covariances and arbitrary-order statistical moments. 10.2172/1028931
        !partly normalized
        subroutine OHMomToTMag(ndata,ndataOld)
            use NaraData,only: ANum,Mag,HMomMag,CData
            integer(kind=KQInd),intent(in)::ndata!number of DataPoints to add
            integer(kind=KQInd),intent(in)::ndataOld!already added Datapoints

            integer(kind=KQInd)::i,ndataNew,ndnd
            real(kind=KCalc),dimension(0:ANum)::delta,deltaN,deltaN2,dxi
            real(kind=KCalc),dimension(0:ANum)::term

            ndataNew=ndataOld
            if(CData%totMBinder) then !calculate higher moments
                if(CData%ttradMBinder) then!traditional Binder Cumulant
                    do i=1,ndata
                        ndnd=ndataNew
                        ndataNew=ndataNew+1
                        dxi=sum(Mag(:,:,i)**2,1)
                        delta=sqrt(dxi)-HMomMag(:,1)
                        deltaN=delta/ndataNew
                        term=(delta**2/ndataNew)*ndnd

                        HMomMag(:,4)=HMomMag(:,4)+(dxi**2-HMomMag(:,4))/ndataNew
                        HMomMag(:,3)=HMomMag(:,3)+(dxi-HMomMag(:,3))/ndataNew
                        HMomMag(:,2)=HMomMag(:,2)+term
                        HMomMag(:,1)= HMomMag(:,1)+deltaN
                    end do

                else!connected 4th order Cumulant
                    do i=1,ndata
                        ndnd=ndataNew
                        ndataNew=ndataNew+1
                        delta=sqrt(sum(Mag(:,:,i)**2,1))-HMomMag(:,1)
                        deltaN=delta/ndataNew
                        deltaN2=deltaN**2
                        term=delta*deltaN*ndnd

                        HMomMag(:,4)=HMomMag(:,4)+term*deltaN2*(ndataNew**2-3*ndataNew+3)&
                        &-4*deltaN*HMomMag(:,3)+6*deltaN2*HMomMag(:,2)
                        HMomMag(:,3)=HMomMag(:,3)+term*deltaN*(ndataNew-2)-3*deltaN*HMomMag(:,2)
                        HMomMag(:,2)=HMomMag(:,2)+term
                        HMomMag(:,1)= HMomMag(:,1)+deltaN
                    end do
                end if

            else!no 4th order Cumulant
                do i=1,ndata
                    ndnd=ndataNew
                    ndataNew=ndataNew+1
                    delta=sqrt(sum(Mag(:,:,i)**2,1))-HMomMag(:,1)
                    deltaN=delta/ndataNew
                    term=delta*deltaN*ndnd

                    HMomMag(:,2)=HMomMag(:,2)+term
                    HMomMag(:,1)= HMomMag(:,1)+deltaN

                end do
            end if

        end subroutine OHMomToTMag

        subroutine OExHMomToTMag(dpi,ndataOld)!Extract Susceptibility and mean tot mag from higher moment
            use NaraData,only: HMomMag,Suscept,MeanTotMag,BinderTotMag,CData
            integer(kind=KQInd),intent(in)::dpi!DataPoint index
            integer(kind=KQInd),intent(in)::ndataOld!already added Datapoints

            MeanTotMag(:,dpi)=HMomMag(:,1)
            Suscept(:,dpi)=HMomMag(:,2)/ndataOld


            if(CData%ttradMBinder) then!calculate binder cumulant
                BinderTotMag(:,dpi)=(HMomMag(:,4)/HMomMag(:,3)**2)
            elseif(CData%totMBinder) then!calculate connected binder cumulant
                BinderTotMag(:,dpi)=(HMomMag(:,4)/HMomMag(:,2)**2)*ndataOld
            end if

        end subroutine OExHMomToTMag

        !norm susceptibility + unitconversion if necessary
        subroutine ONormSuscept(ndata) !todo check if it is faster to norm in OCalc routine
            use NaraData,only: CData,Suscept,APSNum,Para,UCSuscept
            integer(kind=KQInd),intent(in)::ndata!number of DataPoints to consider
            integer(kind=KQInd)::i

            do i=1,ndata
                Suscept(:,i)=Suscept(:,i)/(Para(1,i)*APSNum)
            end do

            if(CData%unitConv)then
                Suscept(:,:ndata)=Suscept(:,:ndata)*UCSuscept!mu_B [J/T] to cancels the J from T and [meV/T] to cancel the meV from the Temperature
            end if

        end subroutine ONormSuscept

        !Calculate higher statistical moments (2nd 3rd and 4th) of staggered Magnetization and its mean
        !! Title:     Computing skewness and kurtosis in one pass
        !! Author:    John D. Cook
        !! Date:      2013
        !! Availability: https://www.johndcook.com/blog/skewness_kurtosis/ (26.05.2019)
        !! original paper: P�bay: Formulas for robust, one-pass parallel computation of covariances and arbitrary-order statistical moments. 10.2172/1028931
        !partly normalized
        subroutine OHMomStagMag(ndata,ndataOld)
            use NaraData,only: StagMag,HMomStagMag,CData
            integer(kind=KQInd),intent(in)::ndata!number of DataPoints to add
            integer(kind=KQInd),intent(in)::ndataOld!already added Datapoints

            integer(kind=KQInd)::i,ndataNew,ndnd
            real(kind=KCalc)::delta,deltaN,deltaN2,dxi
            real(kind=KCalc)::term

            ndataNew=ndataOld

            if(CData%stagMBinder) then!calculate higher moments
                if(CData%stradMBinder) then
                    do i=1,ndata
                        ndnd=ndataNew
                        ndataNew=ndataNew+1
                        dxi=sum(StagMag(:,i)**2,1)
                        delta=sqrt(dxi)-HMomStagMag(1)
                        deltaN=delta/ndataNew
                        term=(delta**2/ndataNew)*ndnd

                        HMomStagMag(4)=HMomStagMag(4)+(dxi**2-HMomStagMag(4))/ndataNew
                        HMomStagMag(3)=HMomStagMag(3)+(dxi-HMomStagMag(3))/ndataNew
                        HMomStagMag(2)=HMomStagMag(2)+term
                        HMomStagMag(1)= HMomStagMag(1)+deltaN
                    end do

                else
                    do i=1,ndata
                        ndnd=ndataNew
                        ndataNew=ndataNew+1
                        delta=sqrt(sum(StagMag(:,i)**2,1))-HMomStagMag(1)
                        deltaN=delta/ndataNew
                        deltaN2=deltaN**2
                        term=delta*deltaN*ndnd

                        HMomStagMag(4)=HMomStagMag(4)+term*deltaN2*(ndataNew**2-3*ndataNew+3)&
                        &-4*deltaN*HMomStagMag(3)+6*deltaN2*HMomStagMag(2)
                        HMomStagMag(3)=HMomStagMag(3)+term*deltaN*(ndataNew-2)-3*deltaN*HMomStagMag(2)
                        HMomStagMag(2)=HMomStagMag(2)+term
                        HMomStagMag(1)= HMomStagMag(1)+deltaN
                    end do
                end if
            else
                do i=1,ndata
                    ndnd=ndataNew
                    ndataNew=ndataNew+1
                    delta=sqrt(sum(StagMag(:,i)**2,1))-HMomStagMag(1)
                    deltaN=delta/ndataNew
                    term=delta*deltaN*ndnd

                    HMomStagMag(2)=HMomStagMag(2)+term
                    HMomStagMag(1)= HMomStagMag(1)+deltaN
                end do
            end if

        end subroutine OHMomStagMag

        subroutine OExHMomStagMag(dpi,ndataOld)!Extract Susceptibility and mean staggered mag from higher moment
            use NaraData,only: HMomStagMag,StagSuscept,MeanStagMag,BinderStagMag,CData
            integer(kind=KQInd),intent(in)::dpi!DataPoint index
            integer(kind=KQInd),intent(in)::ndataOld!already added Datapoints

            MeanStagMag(dpi)=HMomStagMag(1)
            StagSuscept(dpi)=HMomStagMag(2)/ndataOld


            if(CData%stagMBinder) then !calculate binder cumulant
                BinderStagMag(dpi)=HMomStagMag(4)/HMomStagMag(3)**2
            elseif(CData%stagMBinder) then!calculate connected binder cumulant
                BinderStagMag(dpi)=(HMomStagMag(4)/HMomStagMag(2)**2)*ndataOld
            end if

        end subroutine OExHMomStagMag

        !norm susceptibility + unitconversion if necessary
        subroutine ONormStagSuscept(ndata) !todo check if it is faster to norm in OCalc routine
            use NaraData,only: CData,StagSuscept,PNum,Para,UCSuscept
            integer(kind=KQInd),intent(in)::ndata!number of DataPoints to consider

            StagSuscept(:ndata)=StagSuscept(:ndata)/(Para(1,:ndata)*PNum)

            if(CData%unitConv)then
                StagSuscept(:ndata)=StagSuscept(:ndata)*UCSuscept!mu_B [J/T] to cancels the J from T and [meV/T] to cancel the meV from the Temperature
            end if

        end subroutine ONormStagSuscept

        !normalize staggered magnetization
        subroutine ONormStagMag(ndata)
            use NaraData,only:MeanStagMag,PNum,CData,UCMagn
            integer(kind=KQInd),intent(in)::ndata

            !unit Conversion mean;multiply with for unitconversion necessary parameters
            if(CData%unitConv) then
                MeanStagMag(:ndata)=MeanStagMag(:ndata)*UCMagn
            end if
            MeanStagMag(:ndata)=MeanStagMag(:ndata)/real(PNum,KCalc)!normalize for particle number
        end subroutine ONormStagMag



        !normalize Total magnetization
        subroutine ONormTotMag(ndata)
            use NaraData,only:MeanTotMag,APSNum,ANum,CData,UCMagn
            integer(kind=KQInd),intent(in)::ndata

            integer(kind=KDefI)::i

            !unit Conversion mean;multiply with for unitconversion necessary parameters
            if(CData%unitConv) then
                MeanTotMag(:,:ndata)=MeanTotMag(:,:ndata)*UCMagn
            end if

            !normalize for particle number
            do i=0,ANum
                MeanTotMag(i,:ndata)=MeanTotMag(i,:ndata)/real(APSNum(i),KCalc)
            end do

        end subroutine ONormTotMag

        subroutine ObsEner(ener,paraind)!Calculates the Energy of the system (0,:) and each atomsort (1:ANum,:)...not normalized
            use NaraData,only:PNum,ANum,SpinP,ExchMat,SIAMat,LandeG,Para,DoubleC
            real(kind=KCalc),dimension(0:ANum),intent(out)::ener!total Energy (0,:) System, each atom sort: (1:ANum,:)
            integer(kind=KQInd),intent(in)::paraind

            integer(kind=KQInd)::i
            integer(kind=KInde)::j,neI,exI,siaI
            real(kind=KCalc),dimension(3)::cEner!intermediate result of energy per dimension

            ener=0.0_KCalc
            do j=1,PNum
                cEner=0.0_KCalc
                siaI=SpinP(j)%pInde(1)!index for SIA Matrix
                do i=1,SpinP(j)%anzNeigh! two particle interaction
                    neI=SpinP(j)%indList(1,i)!neighbour index
                    exI=SpinP(j)%indList(2,i)!interaction matrix with neighbour
                    !H_{ij}=S_iM_{ij}S_j
                    !sener=sener-dot_product(tSpin,matmul(ExchMat(exI)%exTen,SpinP(neI)%sDir))
                    !reduced to isotropic for speed reasons
                    cEner=cEner+ExchMat(exI)%exTen*SpinP(neI)%sDir
                end do
                cEner=cEner/DoubleC+Para(2:,paraind)*LandeG(SpinP(j)%iaSort)!NOTE maybe handle case when no unit conversion is needed
                !calculate ener of spin and delta ener
                ener(SpinP(j)%iaSort)=ener(SpinP(j)%iaSort)-sum(SpinP(j)%sDir*cEner)
                if (siaI/=0) then!Full SIA when needed SIA index 0 == no SIA
                    ener(SpinP(j)%iaSort)=ener(SpinP(j)%iaSort)-&
                    (SpinP(j)%sDir(1)*sum(SpinP(j)%sDir*SIAMat(siaI)%intMat(1:3))+&
                    &SpinP(j)%sDir(2)*sum(SpinP(j)%sDir(2:3)*SIAMat(siaI)%intMat(4:5))+&
                    &SpinP(j)%sDir(3)**2*SIAMat(siaI)%intMat(6))
                end if

            end do
            ener(0)=sum(ener(1:ANum))

        end subroutine ObsEner

        !Normalizes the Mean Energy and its STD
        !also calculates the system Mean and its STD
        !There is no unit conversion here since this is done in CalcHam
        subroutine ONormEner(ndata)
            use NaraData,only: APSNum
            use NaraData,only: MeanEner
            integer(kind=KQInd),intent(in)::ndata
            integer(kind=KQInd)::j

            do j=1,ndata!norm for particle number
                MeanEner(0,j)=sum(MeanEner(1:,j),1)!calculate system Mean
                MeanEner(:,j)=MeanEner(:,j)/real(APSNum,KCalc)
            end do

!            do j=1,ndata!norm for particle number
!                StDEner(0,j)=sum(StDEner(1:,j),1)!calculate system std
!                StDEner(:,j)=StDEner(:,j)/real(APSNum,KCalc)
!            end do

        end subroutine ONormEner


        !resets data structure of Higher Moments
        subroutine OHMomReset()
            use NaraData,only:HMomEner,HMomMag,HMomStagMag

            HMomEner=0.0_KCalc
            HMomMag=0.0_KCalc
            HMomStagMag=0.0_KCalc
        end subroutine OHMomReset

        !Calculate higher statistical moments (2nd 3rd and 4th) of Energy and its mean
        !! Title:     Computing skewness and kurtosis in one pass
        !! Author:    John D. Cook
        !! Date:      2013
        !! Availability: https://www.johndcook.com/blog/skewness_kurtosis/ (26.05.2019)
        !! original paper: P�bay: Formulas for robust, one-pass parallel computation of covariances and arbitrary-order statistical moments. 10.2172/1028931
        !partly normalized
        subroutine OHMomEner(ndata,ndataOld)
            use NaraData,only: ANum,Ener,HMomEner,CData
            integer(kind=KQInd),intent(in)::ndata!number of DataPoints to add
            integer(kind=KQInd),intent(in)::ndataOld!already added Datapoints

            integer(kind=KQInd)::i,ndataNew,ndnd
            real(kind=KCalc),dimension(0:ANum)::delta,deltaN,deltaN2
            real(kind=KCalc),dimension(0:ANum)::term

            ndataNew=ndataOld

            if(CData%enerBinder) then !calculate higher moments
                do i=1,ndata
                    ndnd=ndataNew
                    ndataNew=ndataNew+1
                    delta=Ener(:,i)-HMomEner(:,1)
                    deltaN=delta/ndataNew
                    deltaN2=deltaN**2
                    term=delta*deltaN*ndnd

                    HMomEner(:,4)=HMomEner(:,4)+term*deltaN2*(ndataNew**2-3*ndataNew+3)&
                    &-4*deltaN*HMomEner(:,3)+6*deltaN2*HMomEner(:,2)

                    HMomEner(:,3)=HMomEner(:,3)+term*deltaN*(ndataNew-2)-3*deltaN*HMomEner(:,2)

                    HMomEner(:,2)=HMomEner(:,2)+term
                    HMomEner(:,1)= HMomEner(:,1)+deltaN

                end do
            else
                do i=1,ndata
                    ndnd=ndataNew
                    ndataNew=ndataNew+1
                    delta=Ener(:,i)-HMomEner(:,1)
                    deltaN=delta/ndataNew
                    term=delta*deltaN*ndnd
                    HMomEner(:,2)=HMomEner(:,2)+term
                    HMomEner(:,1)= HMomEner(:,1)+deltaN
                end do
            end if
        end subroutine OHMomEner


        subroutine OExHMomEner(dpi,ndataOld)!get higher moments of the energy
            use NaraData,only: HMomEner,SpecHeat,CData,BinderEner
            integer(kind=KQInd),intent(in)::dpi!DataPoint index
            integer(kind=KQInd),intent(in)::ndataOld!already added Datapoints

            SpecHeat(:,dpi)=HMomEner(:,2)/ndataOld
            if(CData%enerBinder) then!calculate binder cumulant
                BinderEner(:,dpi)=HMomEner(:,4)*ndataOld/HMomEner(:,2)**2
            end if
        end subroutine OExHMomEner

        subroutine OExHMomEnerC(dpi)!get higher moments of the energy copy for extraction of mean energy
            use NaraData,only: HMomEner,MeanEner
            integer(kind=KQInd),intent(in)::dpi!DataPoint index

            MeanEner(:,dpi)=HMomEner(:,1)
        end subroutine OExHMomEnerC

        subroutine ONormSpecHeat(ndata) !todo check if it is faster to norm in OCalc routine
            use NaraData,only: CData,SpecHeat,APSNum,Para
            integer(kind=KQInd),intent(in)::ndata!number of DataPoints to consider

            integer(kind=KQInd)::j

            if(CData%unitConv) SpecHeat(:,:ndata)=SpecHeat(:,:ndata)*KBoltzmEV!convert to [meV/K]
             do j=1,ndata !factor 2 to compensate for double counting
                SpecHeat(:,j)=SpecHeat(:,j)/(APSNum(:)*Para(1,j)**2)
             end do

        end subroutine ONormSpecHeat

        subroutine NormAcc(ndata)!normalize Acceptance Rate
            use NaraData,only:AccSteps,CData,PNum
            integer(kind=KQInd),intent(in)::ndata!number of DataPoints to consider

            AccSteps(4,:ndata)=sum(AccSteps(:3,:ndata),1)
            AccSteps(:,:ndata)=AccSteps(:,:ndata)/PNum

            if(CData%WMFlip>EpsKCalc) AccSteps(1,:ndata)=AccSteps(1,:ndata)/real(CData%WMFlip,KCMin)
            if(CData%WMDevi>EpsKCalc) AccSteps(2,:ndata)=AccSteps(2,:ndata)/real(CData%WMDevi,KCMin)
            if(CData%WMRand>EpsKCalc) AccSteps(3,:ndata)=AccSteps(3,:ndata)/real(CData%WMRand,KCMin)

        end subroutine NormAcc


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!  Quantities  suitable for Thermalize()
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        subroutine TNewCalcSTMag(mag)!calculates Total and staggered magnetization for new thermalization routine
            use NaraData,only: SpinP,PNum
            real(kind=KCalc),dimension(2),intent(out)::mag
            real(kind=KCalc),dimension(3)::stagMag1,stagMag2!stag Magnetization
            real(kind=KCalc),dimension(3)::totMag!total Magnetization
            integer(kind=KInde)::i

            stagMag1=0.0_KCalc
            stagMag2=0.0_KCalc
            do i=1,PNum
                if(SpinP(i)%subLat)then!claculate staggered Magnetization
                    stagMag1=stagMag1+SpinP(i)%sDir
                else
                    stagMag2=stagMag2+SpinP(i)%sDir
                end if
            end do
            totMag=stagMag1+stagMag2
            stagMag1=stagMag1-stagMag2
            mag(1)=sqrt(totMag(1)**2+totMag(2)**2+totMag(3)**2)
            mag(2)=sqrt(stagMag1(1)**2+stagMag1(2)**2+stagMag1(3)**2)
        end subroutine TNewCalcSTMag


        subroutine TCalcMagniMag(mag)!calculates Total magnetization for new thermalization routine
            use NaraData,only: SpinP,PNum
            real(kind=KCalc),intent(out)::mag
            real(kind=KCalc),dimension(3)::totMag!total Magnetization
            integer(kind=KInde)::i

            totMag=0.0_KCalc
            do i=1,PNum
                totMag=totMag+SpinP(i)%sDir
            end do
            mag=sqrt(totMag(1)**2+totMag(2)**2+totMag(3)**2)
        end subroutine TCalcMagniMag


        !calculates staggered magnetization for new thermalization routine
        subroutine TCalcStagMag(mag)
            use NaraData,only: SpinP,PNum
            real(kind=KCalc),intent(out)::mag
            real(kind=KCalc),dimension(3)::stagMag1,stagMag2!stag Magnetization
            integer(kind=KInde)::i

            stagMag1=0.0_KCalc
            stagMag2=0.0_KCalc
            do i=1,PNum
                if(SpinP(i)%subLat)then!claculate staggered Magnetization
                    stagMag1=stagMag1+SpinP(i)%sDir
                else
                    stagMag2=stagMag2+SpinP(i)%sDir
                end if
            end do
            stagMag1=stagMag1-stagMag2
            mag=sqrt(stagMag1(1)**2+stagMag1(2)**2+stagMag1(3)**2)
        end subroutine TCalcStagMag

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Debug Routines
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!TODO Move to IO
    subroutine SaveSublat()!creates xyz file with the two sub lattices just that nothing more
        use NaraData,only:PNum,SpinP,AuxSpin
        integer(kind=KInde)::i
        open(unit=500,file="sublat.xyz",action="write",status="unknown")
        write(500,*) PNum
        write(500,*)
        do i=1,PNum
            if(SpinP(i)%subLat)then
                write(500,*) "A", AuxSpin(i)%partPos,SpinP(i)%sDir
            else
                write(500,*) "B", AuxSpin(i)%partPos,SpinP(i)%sDir
            end if
        end do
        close(500)

    end subroutine SaveSublat

end module NaraRoutines
