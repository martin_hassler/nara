!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Contains Error Handling Subroutines
!!!! If you edit them: keep them mildly insulting and informative
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module NaraError
    use MHKonstanten
    use NaraData,only:uNLog! Unit number for Log file... allows direct writing
    use iso_fortran_env,only: output_unit, compiler_version!get standard output unit

    implicit none

    public
    private:: MinUNr,MaxUNr,MaxUNrTrys,MaxUsrRead

    integer(kind=KDefI),parameter:: EMsgLen=120!length of ErrorMessage
    integer(kind=KDefI),parameter:: IOEMsgLen=100!length of IO error message length from IO operations

    integer(kind=KDefI), parameter:: MinUNr = 11 !minimum Unit Number for search
    integer(kind=KDefI), parameter:: MaxUNr = 1000 !Maximum Unit Number for search
    integer(kind=KDefI), parameter:: MaxUNrTrys = 3 !Maximum Numbers of attempts to find a free unit number
    integer(kind=KDefI), parameter:: MaxUsrRead = 3 !Maximum number of attempts to enter data during user interaction.

    contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Auxiliary Routines
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !Init function for write operations. Checks is already opened and if we are allowed to write to it.
        !In case of an error it aborts execution
        subroutine InitOpenW(fname)
            character(len=*),intent(in):: fname!file to test

            integer(kind=KDefI):: ioEStat
            logical(kind=KDefL):: fstat,fexist
            character(len=IOEMsgLen):: ioEMsg
            character(len=7)::wstatus

            ioEStat=0
            fstat=.True.
            fexist=.True.
            !Test if file is present
            inquire(file=fname,opened=fstat,iostat=ioEStat,iomsg=ioEMsg,Write=wstatus,exist=fexist)
            if(fstat)  call HCFIO(13,0,fname,"") !File is already opened
            if((scan(wstatus,"YUyu")==0).AND.fexist) then!not allowed to write to file.. scan whether it conatins a Y[es] or U[nknown]
                call HCFIO(14,0,fname,"")
            end if
            if(ioEStat/=0) call HCFIO(15,ioEStat,fname,ioEMsg) !Error while inquiring
        end subroutine InitOpenW


        !Init function for read operations. Checks if file exists and if we are allowed to write to it.
        !In case of an error it aborts execution
        subroutine InitOpenR(fname)
            character(len=*),intent(in):: fname!file to test

            integer(kind=KDefI):: ioEStat
            logical(kind=KDefL):: fexist
            character(len=IOEMsgLen):: ioEMsg
            character(len=7)::rstatus

            ioEStat=0
            fexist=.True.
            !Test if file is present
            inquire(file=fname,exist=fexist,iostat=ioEStat,iomsg=ioEMsg,Read=rstatus)
            if(.NOT.fexist) call HCFIO(17,0,fname,"")!File does not exist
            if(scan(rstatus,"YUyu")==0) then!not allowed to read from file... .. scan whether it conatins a Y[es] or U[nknown]
                call HCFIO(14,1,fname,"")
            end if
            if(ioEStat/=0) call HCFIO(15,ioEStat,fname,ioEMsg) !Error while inquiring
        end subroutine InitOpenR


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! LogFile
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !write to log file
        subroutine WriteLog(msg)
            character(len=*),intent(in)::msg!string to write
            write(UNLog,*)msg!//achar(13)//achar(10)!!LF+CR
        end subroutine WriteLog

        !write to log file advance no
        subroutine WriteLogNoAdv(msg)
            character(len=*),intent(in)::msg!string to write
            write(UNLog,fmt='(A)',advance='no')msg
        end subroutine WriteLogNoAdv

        !write to logfile and Standard Output
        subroutine WriteLogAndStdO(msg)
            character(len=*),intent(in)::msg! string to write
            write(UNLog,*)msg!//achar(13)//achar(10)
            write(*,*)msg
        end subroutine WriteLogAndStdO

        !writes Character Array to Log file
        subroutine WriteLogChAr(n,msg)
            integer(kind=KDefI),intent(in)::n!String length
            character(len=n),dimension(:),intent(in)::msg!Character Array

            integer(kind=KDefI)::i

            do i=1,size(msg)
                write(UNLog,*) trim(adjustl(msg(i)))
            end do
        end subroutine WriteLogChAr

        !writes Character Array to Log file and std output
        subroutine WriteLogChArStd(n,msg)
            integer(kind=KDefI),intent(in)::n!String length
            character(len=n),dimension(:),intent(in)::msg!Character Array

            integer(kind=KDefI)::i

            do i=1,size(msg)
                write(UNLog,*) trim(adjustl(msg(i)))
                write(*,*) trim(adjustl(msg(i)))
            end do
        end subroutine WriteLogChArStd

        !Log message for finishing thermalization
        subroutine WriteLogStdOTherm(wPara,wSteps)
            real(kind=KCalc),dimension(4),intent(in)::wPara!Simulation Parameter
            integer(kind=KDefI),intent(in)::wSteps

            write(UNLog,*) "Finished thermalization after ",wSteps," steps."
            write(UNLog,*) "Parameter set: ", wPara

            write(*,*) "Finished thermalization after ",wSteps," steps."
            write(*,*) "Parameter set: ", wPara
        end subroutine WriteLogStdOTherm

        !writes the read Data from Cnara file to Log file
        subroutine WriteLogConfig()
            use NaraData,only:CData
            call WriteLogSpacer()
            write(UNLog,*)"I will run the simulation with the following configuration: "
            write(UNLog,'(A,X,I0)')  "Number of lattice/MC-sweeps per measurement:", CData%NMCSweep
            write(UNLog,'(A,X,I0)')  "Number of averaging runs (measurements per Datapoint):", CData%NAver
            write(UNLog,'(A,X,I0)')  "Number of runs pre first thermalization :", CData%NPreTherm
            write(UNLog,'(A,X,L1)')  "Automatic Thermalization:", CData%advTherm
            write(UNLog,'(A,X,I0)')  "Thermalization method:", CData%autTherm
            write(UNLog,'(A,X,I0)')  "Minimum number of thermalization runs (without steps needed for autTherm):", CData%NMinTherm
            write(UNLog,'(A,X,I0)')  "Maximum number of thermalization runs:", CData%NMaxTherm
            write(UNLog,'(A,X,I0)')  "Number of runs for moving average during"//&
            &" thermalization :", CData%NmAv
            write(UNLog,'(A,X,I0)')  "Delay between moving averages:", CData%NmAvDel
            write(UNLog,'(A,X,f0.10)')  "Maximum relative change of order parameter (compared to"//&
            &" moving average) to stop thermalization:", CData%RelChOPa
            write(UNLog,'(A)')  "Probabilities for the different Trial steps (normalized) :"
            write(UNLog,'(3X,A,X,f0.10)')  "Flipping:",CData%WMFlip
            write(UNLog,'(3X,A,X,f0.10)')  "Small deviation:",CData%WMDevi
            write(UNLog,'(3X,A,X,f0.10)')  "Random point on unit sphere:",CData%WMRand
            write(UNLog,'(A,X,f0.5)')  "Half of the cone opening angle for the small"//&
            &" deviation step (in rad):",CData%ConeAngle
            write(UNLog,'(A,X,I0)')  "Spin initialization Nr.:", CData%InitSpins
            write(UNLog,'(A,3(X,I0))')  "Boundary Condition Nr.:", CData%BCs
            write(UNLog,'(A,X,I0)') "Sublattice gen. method:", CData%subLatgen
            write(UNLog,'(A,X,L1)') "Unit Conversion:", CData%unitConv
            if (CData%unitConv) write(UNLog,'(A,X,L1)') "use SI magnetic units:", CData%siMagUnit
            write(UNLog,'(A,X,L1)') "Binder Cumulant Total Magnetization:", CData%totMBinder
            write(UNLog,'(A,X,L1)') "Traditional Binder Cumulant Total Magnetization:", CData%ttradMBinder
            write(UNLog,'(A,X,L1)') "Binder Cumulant Staggered Magnetization:", CData%stagMBinder
            write(UNLog,'(A,X,L1)') "Traditional Binder Cumulant Staggered Magnetization:", CData%stradMBinder
            write(UNLog,'(A,X,L1)') "Binder Cumulant Energy:", CData%enerBinder
            write(UNLog,'(A,X,L1)') "Save Raw data to file:", CData%saveRaw
            write(UNLog,'(A,X,I0)') "Chosen PRNG:", CData%PRNG

            write(UNLog,*)
        end subroutine WriteLogConfig

        !writes the read Data from Pnara file to Log file
        subroutine WriteLogPara()
            use NaraData,only:PData
            integer(kind=KDefI)::i
            call WriteLogSpacer()
            write(UNLog,*)"I have read the following data from the supplied Pnara file: "
            do i=1,size(PData)
                write(UNLog,'(A,X,I0)') "Parameter Dataset Nr.:",i
                if(PData(i)%newsimrun) then
                    write (UNLog,*)"  Start new Run; %NEW%-Tag: ", PData(i)%newsimrun
                else
                    write (UNLog,*)"  %NEW%-Tag: ", PData(i)%newsimrun
                end if
                if(PData(i)%runlstate) then
                    write (UNLog,*)"  Saved this state; %SAVE%-Tag: ",&
                    & PData(i)%runlstate
                else
                    write (UNLog,*)"  %SAVE%-Tag: ", PData(i)%runlstate
                end if
                if(PData(i)%savestate) then
                    write (UNLog,*)"  Start from last saved state; %START%-Tag: ",&
                    & PData(i)%savestate
                else
                    write (UNLog,*)"  %START%-Tag: ", PData(i)%savestate
                end if
                write(UNLog,'(3X,A,X,I0)') "Number of datapoints to this point:",&
                &PData(i)%ndata
                write(UNLog,'(3X,A,X,f0.10)') "Temperature:",PData(i)%temppoint
                write(UNLog,'(3X,A,3(X,f0.10))') "Magnetic field:",PData(i)%hpoint
            end do
            write(UNLog,*)
        end subroutine WriteLogPara

        !writes the logfile of the Structure Data
        subroutine WriteLogStructure()
            use NaraData,only:ASort,SData,MagMom,LandeG
            integer(kind=KInde)::i

            call WriteLogSpacer()
            write(UNLog,'(A)')"I have read the following data from the supplied"//&
            &" structure and auxiliary structure files: "
            write(UNLog,'(A)')"Unit cell vectors in Cartesian coordinates: "
            do i=1,3
                write(UNLog,'(3X,A,X,I0,A,3(X,f0.10))') "Vector ",i,":",SData%uCVectors(:,i)
            end do
            write(UNLog,'(A,3(X,f0.10))') "Unitcell offset:",SData%uCOffset
            write(UNLog,'(A,3(X,I0))') "Number of unitcells for super cell:",SData%NCells
            write(UNLog,'(A,I0)')"Number of Atoms in Unit cell: ",SData%nAtoms
            write(UNLog,'(A,3(I0,1X))')"Sub lattice plane in miller indices: ", SData%sublatPlane
            write(UNLog,'(A,3(I0,1X))')"Spindirection in miller indices: ", SData%sublatSpindir
            do i=1,SData%nAtoms
                write(UNLog,'(3X,A,X,I0)') "Atom number:",i
                write(UNLog,'(3X,A)') "Type: "//ASort(SData%aData(i)%iaSort)
                write(UNLog,'(3X,A,X,f0.10)')"Magnetic moment:",MagMom(SData%aData(i)%iaSort)
                write(UNLog,'(3X,A,X,f0.10)')"Land� g- factor:",LandeG(SData%aData(i)%iaSort)
                write(UNLog,'(3X,A,X,L1)')"Default sub-lattice:",SData%aData(i)%subLat
                write(UNLog,'(3X,A,3(1X,f0.10))')"Default spin direction:",SData%aData(i)%defSdir
                write(UNLog,'(3X,A,3(X,f0.10))') "Atom position in Cartesian coordinates:"&
                &,SData%aData(i)%aPos
            end do
            write(UNLog,*)
        end subroutine WriteLogStructure

        !writes the log file of the Interaction Data
        subroutine WriteLogInter()
            use NaraData,only:ExchMat,SIAMat,ASort,IData
            integer(kind=KDefI)::i,j

            call WriteLogSpacer()
            write(UNLog,'(A)')"I have read the following data from the supplied interaction files: "
            write(UNLog,'(A,I0)')"Number of SIA Matrices: ",size(SIAMat)
            do i=0,size(SIAMat)-1
                write(UNLog,'(3X,A,I0)') "SIA Matrix Nr.: ",i
                write(UNLog,'(2X,3(X,f0.10))') SIAMat(i)%intMat(1:3)
                write(UNLog,'(2X,3(X,f0.10))') 0.0_KInMa, SIAMat(i)%intMat(4:5)
                write(UNLog,'(2X,3(X,f0.10))') 0.0_KInMa, 0.0_KInMa, SIAMat(i)%intMat(6)
                write(UNLog,*)
            end do
            write(UNLog,'(A,I0)')"Number of exchange tensors: ",size(ExchMat)
            write(UNLog,'(A)')"Note: Only the diagonal terms are shown since the others"//&
            &" are not used in the current Version."

            do i=1,size(ExchMat)
                write(UNLog,'(3X,A,I0)') "Diagonal terms of exchange tensor Nr.: ",i
                    write(UNLog,'(2X,3(X,f0.10))') ExchMat(i)%exTen(1),&
                    &ExchMat(i)%exTen(2),ExchMat(i)%exTen(3)
                write(UNLog,*)
            end do

            do i=1,size(IData)
                write(UNLog,'(2A)') "Atom type: ",ASort(i)
                write(UNLog,'(3X,A,X,I0)') "SIA Matrix Nr.:",IData(i)%pInde
                write(UNLog,'(3X,A,X,I0)') "Partner Exch.Mat. min. Radius max. Radius"
                write(UNLog,'(3X,A)') repeat("-",39)
                do j=1,size(IData(i)%twopart)
                    write(UNLog,'(3X,A7,X,I9,2(X,f0.10))') ASort(IData(i)%twopart(j)%iaSort),&
                    & IData(i)%twopart(j)%eInd,IData(i)%twopart(j)%rad
                end do
                write(UNLog,*)
            end do
        end subroutine WriteLogInter

        !write header for interaction partner table to log
        subroutine WriteIPartnerHeader()
            call WriteLog("Found interaction partners")
            call WriteLog("   Atom_i Atom_j UC_index_x UC_i_y UC_i_z Base_index Distance")
        end subroutine WriteIPartnerHeader

        !write interaction partner info to logfile
        subroutine WriteIPartnerLog(aind,i,j,k,l,dist)
            use NaraData,only:ASort,SData
            integer(kind=KInde),intent(in)::aind!centre atom whose partners we are writing down
            integer(kind=KDefI),intent(in)::i,j,k,l!Unit cell index x,y,z relative to centre atom , l= base index of interaction partner
            real(kind=KPosi),intent(in)::dist

            character(len=4)::le
            write(le,'(A,I3)') "A",AtNL

            write(UNLog,'(4X,'//le//',X,'//le//',X,4(I0,X),f0.7)') ASort(SData%aData(aInd)%iaSort),&
            & ASort(SData%aData(l)%iaSort), i,j,k,l,dist
        end subroutine WriteIPartnerLog


        !Init Log file... open get unit number and !FIXME Error handling when one cannot open LogFile
        subroutine InitLog()
            use NaraData,only:FnLog
            integer(kind=KDefI):: ioEStat
            character(len=IOEMsgLen):: ioEMsg
            character(len=8):: datum
            character(len=10):: time

            call date_and_time(date=datum,time=time)

            call InitOpenW(FnLog)!test and init open

            open(newunit=UNLog,file=FnLog,status="unknown",position="append",&
            &action="write",iostat=ioEStat,iomsg=ioEMsg)

            if(ioEStat/=0) call HCFIO(100,ioEStat,FnLog,ioEMsg)!error while opening Log file
            write(UNLog,'(A)') repeat("-_",45)//"-"!spacer
            write(UNLog,*)
            write(UNLog,*)"Starting Project Nara @: "//datum(7:8)//"."//datum(5:6)//"."//datum(:4)//" "//time(:2)&
            &//":"//time(3:4)//":"//time(5:)
            write(UNLog,*) "Nara Version: "//VERSION
            write(UNLog,*)'Compiled with: '//COMPILER_VERSION()
            write(UNLog,*) "Have fun!"
            call WriteLogSpacer()

        end subroutine InitLog

        !Finishes Log file
        subroutine FinLog()
            character(len=8):: datum
            character(len=10):: time

            call WriteLogSpacer()
            call date_and_time(date=datum,time=time)
            write(UNLog,*)"Finished @: "//datum(7:8)//"."//datum(5:6)//"."//datum(:4)//" "//time(:2)&
            &//":"//time(3:4)//":"//time(5:)
            write(UNLog,*) "Bye, honey pie."
            write(UNLog,'(A)') repeat("-_",45)//"-"!spacer
            close(UNLog)
        end subroutine FinLog

        subroutine DisplayLicense()
            write(*,*)'NARA version '//VERSION//' Copyright (C) '//CYEARS//'  Martin F.T. Hassler'
            write(*,*)'Compiled with: '//COMPILER_VERSION()
            write(*,*)
            write(*,*)'NARA is free software: you can redistribute it and/or modify'
            write(*,*)'it under the terms of the GNU Affero General Public License as'
            write(*,*)'published by the Free Software Foundation, either version 3 of the License,'
            write(*,*)'or (at your option) any later version.'
            write(*,*)
            write(*,*)'NARA comes with ABSOLUTELY NO WARRANTY.'
            write(*,*)'See the GNU Affero General Public License for more details.'
            write(*,*)
            write(*,*)'You should have received a copy of the GNU Affero General Public License'
            write(*,*)'along with this program.  If not, see <https://www.gnu.org/licenses/>.'
            write(*,*)
            write(*,'(A)')repeat("-",91)
        end subroutine DisplayLicense

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Error Messages
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !Halt and Catch Fire, stops program execution and displays errormsg... for IO operations
        subroutine HCF(intEC,thrownEC,eMsg)
            integer(kind=KDefI),intent(in)::intEC!internal Error code to determine where Error occured and customize Message ex.: 1... error while reading file
            integer(kind=KDefI),intent(in)::thrownEC!error condition ...ex.: iostat from IO operation
            character(len=*),intent(in)::eMsg!Error Message

            character(len=EMsgLen)::iEMsg
            character(len=10)::cThrownEc
            character(len=EMsgLen),dimension(6)::msg


            call WriteError()!writes Error Header

            msg=""

            select case (intEC)

                case(100) !default case for iostat in IO Operations
                    write(*,*) "**Error**"
                    write(*,*) "Riiight, I've tried to make that IO operation for ya"
                    write(*,*) "....just no."
                    write(cThrownEc,"(I0)") thrownEC
                    iEMsg = "iostat /=0 in Subroutine "//trim(adjustl(eMsg))
                    iEMsg = iEMsg//"... IOStat = "//trim(adjustl(cThrownEc))

                case(104) !Missing files
                    write(*,*) "**Error**"
                    write(*,*) "You twit forgot an important input file"
                    select case (thrownEC)
                        case(-4)
                            write(*,*) "No parameter file .Pnara was found."
                        case(-3)
                            write(*,*) "No interaction file .Inara was found."
                        case(-2)
                            write(*,*) "No structure file was specified."
                        case(-1)
                            write(*,*) "Configuration file .Cnara is missing."
                        case(1)
                            write(*,*) "One file is missing."
                        case(2)
                            write(*,*) "Two files are missing."
                        case(3)
                            write(*,*) "Three files are missing."
                        case(4)
                            write(*,*) "Four files are missing."
                        case default
                            write(*,*) "This shouldn't be displayed, I have no idea what happened."
                            write(*,*) "Sorry for calling you a twit, since it is probably my fault" &
                            & //" that this message got displayed."
                    end select

                    iEMsg=adjustl(eMsg)

                case(105) !Too many Files
                    write(*,*) "**Error**"
                    write(*,*) "Why did you do that to me, was one file not enough for you?"
                    select case (thrownEC)
                        case(1)
                            write(*,*) "Too many Configuration files."
                        case default
                            write(*,*) "Whatever, I am going home."
                    end select
                    iEMsg=adjustl(eMsg)

                case(106) !More atoms in Sdata than in corresponding structure file
                    write(*,*) "**Error**"
                    write(*,*) "You specified more atoms in the auxiliary structure file than in the"
                    write(*,*) "corresponding structure file?"
                    write(*,*) "Yeah well, screw the whole thing"
                    iEMsg=adjustl(eMsg)

                case(107) !User Error in Input File
                    write(*,*) "**Error**"
                    write(*,*) "I have to stop the simulation early."
                    write(*,*) "Since, we got wrong numbers."
                    write(*,*) "So I can't run the simulation."
                    write(*,*) "Well anyway, you're here now, but it's gonna take hours to fix this,  "
                    iEMsg=adjustl(eMsg)
                case(108) !empty file
                    write(*,*) "**Error**"
                    write(*,*) ">> Please insert a funny error message<<"
                    iEMsg=adjustl(eMsg)
                case(109) !severe Error
                    write(*,*) "**Error**"
                    write(*,*) "I have no idea what happened."
                    iEMsg=adjustl(eMsg)
                case(110)!file already opened; or cannot write to it
                    write(*,*) "**Error**"
                    write(*,*)
                    iEMsg=adjustl(eMsg)
                case default
                    write(*,*) "**Error... Stopping Simulation**"
                    write(*,*) "internal Errorcode: ", thrownEC
                    write(*,*) "This Video might provide helpful Information to solve this unknown problem:"
                    write(*,*) "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                    iEMsg=adjustl(eMsg)
            end select

            write(*,*) trim(iEMsg)
            flush(output_unit)!only flush Standard output, since we don't know where the error occured
            flush(UNLog)
            stop
        end subroutine HCF

!Halt and Catch Fire, stops program execution and displays errormsg... for IO operations
        subroutine HCFIO(intEC,thrownEC,intEMsg,eMsg)
            integer(kind=KDefI),intent(in)::intEC!internal Error code to determine where Error occured and customize Message ex.: 1... error while reading file
            integer(kind=KDefI),intent(in)::thrownEC!error condition ...ex.: iostat from IO operation
            character(len=*),intent(in)::intEMsg!internal Error Message
            character(len=*),intent(in)::eMsg!Error Message

            character(len=EMsgLen)::iEMsg
            character(len=10)::cThrownEc
            character(len=EMsgLen),dimension(6)::msg


            call WriteError()!writes Error Header

            msg=""

            select case (intEC)

                case(1) !Error while reading file called when iostat after read is positive
                    msg(1)="No! I can't and won't do that disgusting thing your filthy mind has planned."
                    msg(2)="Anyway there was an error while reading the following file:"
                    msg(3)=trim(adjustl(intEMsg))
                    write(cThrownEc,"(I0)") thrownEC
                    msg(4)="IOSTAT = "//trim(adjustl(cThrownEc))//" | IntEC = "
                    write(cThrownEc,"(I0)") intEC
                    msg(4)=trim(msg(4))//cThrownEc
                    msg(5)="IOMSG = "//trim(adjustl(eMsg))
                    msg(6)="Bye!"
                case(2) !File got closed while reading, while error handling
                    msg(1)="You are in deep trouble my child!"
                    msg(2)="The file got closed while I was reading key words. Filename: "
                    msg(3)=trim(adjustl(intEMsg))
                    write(cThrownEc,"(I0)") intEC
                    msg(4)="IntEC = "//cThrownEc
                    msg(5)="Stay put for further information!"
                    call WriteLogChArStd(EMsgLen,msg(:5))!write Error Msg to logfile
                    return!TODO warum return?
                case(3) !invalid unit number, while error handling
                    msg(1)="I am in deep trouble my child!"
                    msg(2)="My unit number got invalid while I was reading key words. Filename: "
                    msg(3)=trim(adjustl(intEMsg))
                    write(cThrownEc,"(I0)") intEC
                    msg(4)="IntEC = "//cThrownEc
                    msg(5)="Stay put for further information!"
                    call WriteLogChArStd(EMsgLen,msg(:5))!write Error Msg to logfile
                    return
                case(4) !inquire threw error while error handling
                    msg(1)="I have no idea what is going on here! While I was trying to figuring out"
                    msg(2)=" what file cause the troubles, INQUIRE() threw a fit. File: "
                    msg(3)=trim(adjustl(intEMsg))
                    write(cThrownEc,"(I0)") thrownEC
                    msg(4)="IOSTAT = "//trim(adjustl(cThrownEc))//" | IntEC = "
                    write(cThrownEc,"(I0)") intEC
                    msg(4)=trim(msg(4))//cThrownEc
                    msg(5)="IOMSG = "//trim(adjustl(eMsg))
                    msg(6)="Stay put for further information!"
                    call WriteLogChArStd(EMsgLen,msg)!write Error Msg to logfile
                    return
!                case(10) !Problem with finding a unit number
!                    msg(1)="I am afraid, I gave the last unit number to the nice lady behind you."
!                    msg(2)="Maybe, we will have a few fresh ones tomorrow."
!                    msg(3)="Failed to find free unit number. Search range:"
!                    msg(4)=adjustl(intEMsg)
!                    write(cThrownEc,"(I0)") thrownEC
!                    msg(5)="IOSTAT = "//trim(adjustl(cThrownEc))
!                    msg(6)="IOMSG = "//trim(adjustl(eMsg))
!                    call WriteLogChArStd(EMsgLen,msg)!write Error Msg to logfile
!                case(11) !Problem with finding a unit number stop at user request
!                    msg(1)="I am afraid, I gave the last unit number to the nice lady behind you."
!                    msg(2)="Maybe, we will have a few fresh ones tomorrow."
!                    msg(3)="Failed to find free unit number. Original Search range:"
!                    msg(4)=adjustl(intEMsg)
!                    msg(5)="Asked user to enter new unit number range. Stopped at useres request"
!                    call WriteLogChArStd(EMsgLen,msg(:5))!write Error Msg to logfile
!                case(12) !Problem with finding a unit number , user cannot enter valid rangee
!                    msg(1)="You failed multiple times to enter a valid unit number range."
!                    msg(2)="Come back if you know how to behave"
!                    msg(3)="Failed to find free unit number. Original Search range:"
!                    msg(4)=adjustl(intEMsg)
!                    msg(5)="Asked user to enter new unit number range. The User failed."
!                    call WriteLogChArStd(EMsgLen,msg(:5))!write Error Msg to logfile
                case(13) !file already opened
                    msg(1)="Ahhh forget this stupid simulation, let's do some Jazzercise"
                    msg(2)="File already opened. File name: "
                    msg(3)=adjustl(intEMsg)
                    call WriteLogChArStd(EMsgLen,msg(:3))!write Error Msg to logfile
                case(14) !cannot write to/read from file
                    msg(1)="Sort that out. I am not allowed to do what you told me to do."
                    if(thrownEC==0) then
                        msg(2)="Anyway, according to your System I am not allowed to write to this file:"
                    elseif(thrownEC==1) then
                        msg(2)="Anyway, according to your System I am not allowed to read from this file:"
                    end if
                    msg(3)=adjustl(intEMsg)
                    call WriteLogChArStd(EMsgLen,msg(:3))!write Error Msg to logfile
                case(15) !error while inquire
                    msg(1)="Yep, now I am confused... An error occurred while I tried to inquire the file status."
                    msg(2)="File I tired to inquire: "
                    msg(3)=trim(adjustl(intEMsg))
                    write(cThrownEc,"(I0)") thrownEC
                    msg(4)="IOSTAT = "//trim(adjustl(cThrownEc))
                    msg(5)="IOMSG = "//trim(adjustl(eMsg))
                    call WriteLogChArStd(EMsgLen,msg(:5))!write Error Msg to logfile

                case(17) !File does not exist
                    msg(1)="Bada bada ba beee bop bop bodda bop"
                    msg(2)="Ooops wrong process...anyway the file does not exist. File:"
                    msg(3)=adjustl(intEMsg)
                    call WriteLogChArStd(EMsgLen,msg(:3))!write Error Msg to logfile
                case(18) !wrong filename extension
                    msg(1)="I won't eat it, if you don't tell me what it is. File:"
                    msg(2)=adjustl(intEMsg)
                    select case (thrownEC)
                        case(1)
                            msg(3)="File name ends with an ""."" "
                        case(2)
                            msg(3)="File extension is unknown."
                        case default
                            msg(3)="The file shows an suspicious lack of an extension. ... File ends with "" "" "
                    end select
                    call WriteLogChArStd(EMsgLen,msg(:3))!write Error Msg to logfile
                case(19) !unknown Keyword eMsg=unknown Keyword intEMsg=File
                    msg(1)="What did you say?"
                    msg(2)="I think there is sort of a language barrier between us"
                    msg(3)="You said something I don't understand"
                    msg(4)=eMsg
                    msg(5)="In file: "//trim(adjustl(intEMsg))
                    call WriteLogChArStd(EMsgLen,msg(:5))!write Error Msg to logfile
                case(20) !Input error in file
                    msg(1)="Look who lacks common sense? You made a mistake in: "//trim(adjustl(intEMsg))
                    msg(2)=""
                    select case (thrownEC)
                        case(1)
                            msg(3)="Step weights are not defined properly. Sum of weights is <=0.0 ."
                        case(2)
                            msg(3)="At least one interaction radii is negative."
                        case(3)
                            msg(3)="Invalid temperature (negative)."
                        case(4)
                            msg(3)="Invalid temperature (negative or zero)."
                        case(5)
                            msg(3)="Unknown atom type, SIA"
                        case(6)
                            msg(3)="Unknown atom type, Exch 1"
                        case(7)
                            msg(3)="Unknown atom type, Exch 2"
                        case(8)
                            msg(3)="Error with Matrix section; Exchange interaction."
                        case(9)
                            msg(2)="Number of particles exceeds allowed number, Total number must be smaller than 2.147.483.647."
                            msg(3)=eMsg
                        case default
                            msg(3)="Insert sample text here."
                    end select
                    write(cThrownEc,"(I0)") thrownEC
                    msg(4)="Position: "//trim(adjustl(cThrownEc))
                    msg(5)="Bye!"
                    call WriteLogChArStd(EMsgLen,msg(:5))!write Error Msg to logfile
                case(42) !Reading error  intEMsg=keyword, eMsg=IOMSG, thrownEC=IOSTAT
                    msg(1)="OMG did you really just say that to me?"
                    msg(2)="Ohhhh wait sorry, ... I think... yes I can think,...yes oh yes it's so obvious..."
                    msg(3)="You made a mistake when specifying: "//trim(intEMsg)
                    write(cThrownEc,"(I0)") thrownEC
                    msg(4)="I don't know where you did this mistake but anyway the IOSTAT = "&
                    &//trim(adjustl(cThrownEc))
                    msg(5)="IOMSG = "//trim(adjustl(eMsg))
                    msg(6)="Hope that helps."
                    call WriteLogChArStd(EMsgLen,msg)!write Error Msg to logfile
                case(100:116)!error while opening the file intEC... unique for place where it happened
                    msg(1)="Please pardon the expression, but something is off."
                    msg(2)="An error occurred while opening the darned file:"
                    msg(3)=adjustl(intEMsg)
                    write(cThrownEc,"(I0)") thrownEC
                    msg(4)="IOSTAT = "//trim(adjustl(cThrownEc))//" | IntEC = "
                    write(cThrownEc,"(I0)") intEC
                    msg(4)=trim(msg(4))//cThrownEc
                    msg(5)="IOMSG = "//trim(adjustl(eMsg))
                    msg(6)="Bye!"
                    call WriteLogChArStd(EMsgLen,msg)!write Error Msg to logfile
                case(999)!general Error MSg
                    msg(1)=adjustl(intEMsg)
                    call WriteLogChArStd(EMsgLen,msg)!write Error Msg to logfile
                case default
                    write(*,*) "**Error... Stopping Simulation**"
                    write(*,*) "internal Errorcode: ", thrownEC
                    write(*,*) "This Video might provide helpful Information to solve this unknown problem:"
                    write(*,*) "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                    iEMsg=adjustl(eMsg)

            end select
            flush(output_unit)!only flush Standard output, since we don't know where the error occured
            flush(UNLog)
            stop
        end subroutine HCFIO

        !Halt and Catch Fire, stops program execution and displays errormsg... for general Routines operations
        subroutine HCFR(intEC,thrownEC,eMsg)
            integer(kind=KDefI),intent(in)::intEC!internal Error code to determine where Error occured and customize Message
            integer(kind=KDefI),intent(in)::thrownEC!error condition ...ex. allocation error
            character(len=*),intent(in)::eMsg!Error Message

            character(len=EMsgLen)::iEMsg

            select case(intEC)
                case(101)!negative distance to crystal surface during neighbourhood calculation
                    write(*,*) "**Error... Stopping Simulation**"
                    write(*,*) "Well, well, well, look who we have here!"
                    iEMsg=adjustl(eMsg)
                case(102)!error while allocating data
                    write(*,*) "**Error... Stopping Simulation**"
                    write(*,*) "Welcome to the allocation error section"
                    write(*,*) "Error number: ", thrownEC
                case default
                    write(*,*) "**Error... Stopping Simulation**"
                    write(*,*) "internal Errorcode: ", thrownEC
                    write(*,*) "This Video might provide helpful Information to solve this unknown problem:"
                    write(*,*) "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                    iEMsg=adjustl(eMsg)
            end select
            write(*,*) trim(iEMsg)
            flush(output_unit)!only flush Standard output, since we don't know where the error occured
            flush(UNLog)
            stop
         end subroutine HCFR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! User Messages/Warnings
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         subroutine UMsg(intIC,thrownEC,iMsg)!User Info Messages
            integer(kind=KDefI),intent(in)::intIC!internal Info code
            character(len=*),intent(in)::iMsg!Message
            integer(kind=KDefI),intent(in)::thrownEC!error condition ...ex. allocation error
            character(len=100),dimension(4)::msg
            character(len=10)::iconv
            integer(kind=KDefI)::i

            call WriteWarning()
            msg=""

            select case (intIC)
                case(1)
                    msg(1)=adjustl("**INFO**")
                    msg(2)=adjustl("I think you meant something different, I will just ignore it.")
                    msg(3)=adjustl(trim(iMsg))
                case(2)!error while deallocating data
                    write(iconv,*)thrownEC
                    msg(1)=adjustl("**Error... Pfffff I don't care about that one**")
                    msg(2)=adjustl("No, no nooooo, I won't let you take away my little variables.")
                    msg(3)=adjustl("Deallocation number: "//trim(iMsg)//" ;Error Code: "&
                    &//trim(adjustl(iconv)))
                case(3)
                    msg(1)=adjustl("**Error... Pfffff I don't care about that one**")
                    msg(2)=adjustl("The Random number array " //trim(iMsg)//" is to small."//&
                    &" More numbers were used than it holds.")
                    msg(3)=adjustl("I can deal with it for now. But increase the size in"//&
                    &" the main program and recompile. ASAP")

                case(4)
                    msg(1)=adjustl("**Error... Pfffff I don't care about that one**")
                    msg(2)=adjustl("The world is a torus, I can see myself standing on the horizon waving")
                    msg(3)=adjustl("The current definition of the interaction distances includes at least one case where a Spin")
                    msg(4)=adjustl("will interact with itself in the same unit cell or across the periodic boundary.")
                case default
                    msg(1)=adjustl("**INFO**")
                    msg(2)=adjustl("You made a mistake, but it is OK I can deal with it.")
                    msg(3)=adjustl(trim(iMsg))
            end select
            call WriteLogChAr(len(msg(1)),msg)
            do i=1,2
                write(*,*) msg(i)
            end do
        end subroutine UMsg

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Headers/ASCII art
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !writes Warning Standard output and Logfile
        subroutine WriteWarning()
            character(len=EMsgLen),dimension(5)::msg
            call WriteLogSpacer()!write a spacer to log file
            msg(1)=repeat("WARNING!!!",5)
            msg(2)=adjustl("!!!!")
            msg(3)=adjustl("!!!!"//"May I have your attention please... Or not."//&
            &" I don't care.... I'm just a piece of software")
            msg(4)=adjustl("!!!!")
            msg(5)=repeat("WARNING!!!",5)

            call WriteLogChArStd(EMsgLen,msg)
        end subroutine WriteWarning

        !writes Warning Standard output and Logfile
        subroutine WriteError()
            character(len=EMsgLen),dimension(6)::msg
            call WriteLogSpacer()!write a spacer to log file
            msg(1)=repeat("ERROR",10)
            msg(2)=adjustl("!!!!")
            msg(3)=adjustl("!!!!"//" Listen up! An error occurred, there is no need"//&
            &" to panic now! I will simply stop the Simulation.")
            msg(4)=adjustl("!!!!"//" Now you may panic.")
            msg(5)=adjustl("!!!!")
            msg(6)=repeat("ERROR",10)
            call WriteLogChArStd(EMsgLen,msg)
        end subroutine WriteError

        !Writes ------ Spacer to Log file
        subroutine WriteLogSpacer()
            write(UNLog,*)
            write(UNLog,'(A)')repeat("-",91)
            write(UNLog,*)
        end subroutine WriteLogSpacer
end module NaraError
