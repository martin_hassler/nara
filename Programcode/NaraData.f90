!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Module containing the actual variables, basically everything needed by multiple modules
!!!! Will gradually replace NaraData2
!!!! Global Variables, if possible do not import the whole module, import just the ones needed for that subroutine/function
!!!! This is to provide info what global variables are used in what subroutine.
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module NaraData
    use MHKonstanten
    use NaraTypes


    implicit none
    save
    public


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Global Parameters, read from file
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    type(ParaData),allocatable,dimension(:):: PData!Simulation enviroment Data: Temperature and external magnetic field + number of steps
    type(ParaData),allocatable,dimension(:):: PDataOrig!Contains the original Data (befor Unit Conversion)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Global Simulation parameters
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    real(kind=KCalc),allocatable,dimension(:,:)::Para!Environment Data Temperature and magnetic field
    real(kind=KCalc),parameter::DoubleC=2.0_KCalc!Correction factor for double counting


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Global  Parameters/Constants IO Related
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer(kind=KDefI):: UNLog=6!unit number log file, changes after InitLog is called!
    !allows after Init log was called to write to Logfile directly
    !predefined as 6, is an attempt to catch the error that someone tries to write to logfile before it was initialized...
    !6 is often the standard output (compiler dependent but worth a try)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Global  File Names
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    character(len=*), parameter:: FnRFn="nara.nara"!Name of Main file containing the filenames and paths for the simulation environment
    character(len=*), parameter:: FnLog="LogFileNARA.log"!Name of the log file
    character(len=*), parameter:: FnStats="StatsNARA.dat"!Name of the satistics file
    character(len=*),dimension(3), parameter:: FnDat=(/"DataMagn","DataEner","DataMag2"/)!BaseName of Output file ... File Name looks like this: Data_"Asort"_"SimRun".dat
    character(len=*), parameter:: FEDat=".dat"!File name Ending of Output file
    character(len=*), parameter:: FERawDat=".rdat"!File name Ending of raw-data output files
    integer(kind=KDefI),allocatable,dimension(:,:):: UNDat!Unit numbers of Data files 1st Index Properties (1..Magnetic, 2 Energetic) 2nd Index Atomsort 0:ANum
    integer(kind=KDefI)::UNMea!UnitNumber for measurement file... binary file of the raw measurements.
    integer(kind=KDefI)::UNStats!Unit number for statistics file
!    character(len=*), parameter:: FnMiS="naraMinState.bin"!Name of the minimum State file...used to start simulation from a saved point

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Global; System...Spins exchange matrices positions etc.
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer(kind=KInde):: PNum !Total number of Particles
    integer(kind=KDefI):: ANum !number of AtomSorts (size of Asort
    integer(kind=KDefI),allocatable,dimension(:)::APSNum!number of atoms per sort

    type(SProp),allocatable,dimension(:):: SpinP!Array of the Particles
    real(kind=KSDir),allocatable,dimension(:,:):: CopySpinP!Copy of SpinP state to save state as new starting point
    type(SProp2),allocatable,dimension(:):: AuxSpin!Auxiliary Array of the Particles

    type(ExchVec),allocatable,dimension(:):: ExchMat!Array of Exch Tensors stored independently
    type(SymMat),allocatable,dimension(:):: SIAMat!Array for the Single Ion Anisotropy Tensor starts with 0... 0 all elements are 0.0!

    character(len=AtNL),allocatable,dimension(:):: ASort!Atom Sort Character String
    type(InterData),allocatable,dimension(:):: IData!Array of interaction Data index of IData matches Index of ASort
    type(StructData):: SData!Structure Data
    type(ConfigData):: CData!Configuration Data

    real(kind=KCalc),allocatable,dimension(:):: MagMom!magnetic moment in units of mu_B (index matches Asort)

    !Land� g-factor (index matches Asort) it actually holds LandeG(i)*MagMom(i)  but it is initialized only with LandeG
    real(kind=KCalc),allocatable,dimension(:):: LandeG !I do this beacuse g is only used in CalcHam and so I can use less Multiplications

    real(kind=KCalc):: VolPSpin!Inverse Volume per Spin 1/V0, with: V_0=Volume/#Spins in m^-3
    real(kind=KCalc):: UCMagn! Unit Constant Magnetization mu_B^J/V0   ...[A/m]
    real(kind=KCalc):: UCSuscept! Unit Constant Susceptibility mu_B^J*mu_B^meV*mu_0/V0   ...[meV]
    real(kind=KSDir):: ConeRad!base radius of trial Cone...for trial step small deviation



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Global;  Observables...
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    !1st Index datapoint
    real(kind=KCalc),allocatable,dimension(:)::StagSuscept!Susceptibility
    real(kind=KCalc),allocatable,dimension(:)::MeanStagMag!Mean Staggered Magnetization <sqrt( **2 +...)> ... different sublattices
    real(kind=KCalc),allocatable,dimension(:)::BinderStagMag!Binder Cumulant staggered Magnetization

    !1st index special one, 2nd Index Datapoint
    real(kind=KCMin),allocatable,dimension(:,:)::AccSteps!acceptance rate of the Trial Steps 1st index: 1 Flip, 2 Small deviation , 3 Random, 4 total acceptance rate
    integer(kind=KDefI),allocatable,dimension(:,:)::NMCSteps!number of MC steps per entry;1st index: 1 Thermalization, 2 Measurment not cumulative!

    !1st index Asort 0...system 1:ANum; atomspecific data 2nd Index datapoint
    real(kind=KCalc),allocatable,dimension(:,:)::Ener!Energy
    real(kind=KCalc),allocatable,dimension(:,:)::MeanEner!Mean value Energy
    real(kind=KCalc),allocatable,dimension(:,:)::SpecHeat!Specific Heat
    real(kind=KCalc),allocatable,dimension(:,:)::BinderEner!Binder Cumulant energy
    real(kind=KCalc),allocatable,dimension(:,:)::MeanTotMag!Total Magnetization <sqrt(M_x**2+M_y**2+M_z**2)>
    real(kind=KCalc),allocatable,dimension(:,:)::Suscept!Susceptibility
    real(kind=KCalc),allocatable,dimension(:,:)::BinderTotMag!Binder Cumulant staggered Magnetization

    !Indizes: 1: xyz-Component; 2: Asort  0...system 1->ANum: atomspecific data; 3: Datapoint
    real(kind=KCalc),allocatable,dimension(:,:,:)::Mag!Magnetization
    real(kind=KCalc),allocatable,dimension(:,:,:)::MeanMag!Mean value Magnetization


    !Indizes: 1: xyz-Component; 2: Datapoint
    real(kind=KCalc),allocatable,dimension(:,:)::StagMag!Staggered Magnetization (M^1_x-M^2_x), ... two sublattices defined at input

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Global;  Special purpose variables
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    !Higher statistical moments
    ! 1st 0:ANum; 2nd (Mean,2-,3-,4-Moment... range 1:4)
    real(kind=KCalc),allocatable,dimension(:,:)::HMomEner
    real(kind=KCalc),allocatable,dimension(:,:)::HMomMag
    real(kind=KCalc),dimension(4)::HMomStagMag !only calculated for system so only 2nd index

end module NaraData
