!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Main Program, Nara
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
program Nara
    use MHKonstanten
    use NaraTypes
    use NaraData,only: MeanMag
    use NaraData,only: Ener
    use NaraData,only: Para,PData,PDataOrig
    use NaraData,only: CData,NMCSteps
    use NaraData,only: AccSteps
    use NaraRoutines
    use NaraIO
    use NaraPRNG,only: SetUPPRNG

    implicit none

    integer(kind=KDefI)::i,nPdata,nMCStep
    integer(kind=KQInd)::l,m,maxDataPoint
    integer(kind=KInde),dimension(3)::accCountS

    type(ParaData)::pDataStart,pDataStartO
    real(kind=KCalc),dimension(4)::paraStart

    call DisplayLicense()!write License information
    write(*,*) "Starting Project NARA"
    write(*,*) "Opening Log file"
    call InitLog()!Init Log file
    write(*,*) "Successfully opened Log file"

    !Input
    call ReadFiles()

    !set up PRNG
    call SetUPPRNG()
    !Initialization

    !generate Neighbour list
    call GetNeighR()






    !generating Crystal lattice
    call CreateCryst(CData%BCs)




    call AllocPara(maxDataPoint)!Allocate Para
    call AllocObs(maxDataPoint)!Allocate Observables
    call InitSaveObservable()

    call InitSaveStats()

    call InitUnitConversion()!prepares unit conversion (if necessary) otherwise it initialzes conversion factorxs so that they don't do a thing

    if(CData%saveRaw) call InitSaveMeasurements()

    call WriteLogAndStdO("Finished Initialization")
    call WriteLogAndStdO("I will start the simulation")


    !Other Preparations
    nPdata=size(PData)
    pDataStart%hpoint=0.0_KPara
    pDataStart%temppoint=1.0_KPara
    pDataStart%ndata=0
    pDataStartO=pDataStart

    !Start of calculation
    do i=1,nPdata!loop over all PData curves


        !calculate Parameter list
        if(PData(i)%newsimrun) then!start new Simulation

            !Pre-thermalize of new system
            paraStart(1)=(PData(i+1)%temppoint-PData(i)%temppoint)& !strating point is not included in temperature series.
            &/real(PData(i+1)%ndata,KCalc)+PData(i)%temppoint
            paraStart(2:)=(PData(i+1)%hpoint-PData(i)%hpoint)&
            &/real(PData(i+1)%ndata,KCalc)+PData(i)%hpoint

            call InitSpins(CData%InitSpins)
            call SaveSublat()
            call MCStepM(CData%NPreTherm,paraStart,accCountS)
            call Thermalize(1,paraStart,accCountS,nMCStep)!thermalization


            call SaveStateMin()!save state after prethermalization
            call WriteLogAndStdO("Saved System state.")

            pDataStart=PData(i)
            pDataStartO=PDataOrig(i)
            cycle
        elseif(PData(i)%runlstate) then
            !TODO Save Data
            call RestoreStateMin()
            call WriteLogAndStdO("Loaded System state.")
            call CalcParaArray(pDataStart,PData(i),Para)
        else
            call CalcParaArray(PData(i-1),PData(i),Para)
        end if




        call WriteLogAndStdO("Starting Simulation")
        write(*,*) "Simulating parameter-set: ", i-1, "of :", nPdata-1
        write(*,*) "Total number of data points: ", PData(i)%ndata
        do l=1,PData(i)%ndata
            write(*,*) "Simulating data point: ", l
            accCountS=0
            call OHMomReset()!reset higher moments to 0.0


            call Thermalize(1,Para(:,l),accCountS,NMCSteps(1,l))!thermalization

            accCountS=0

            MeanMag(:,:,l)=0.0_KCalc !necessary since there could be an old value from the last run
            do m=1,CData%NAver!number of measurements
                call MCStepM(CData%NMCSweep,Para(:,l),accCountS)

                !gather Data
                call ObsMag(m)
                call ObsEner(Ener(:,m),l)
            end do

            call OHMomEner(CData%NAver,0)!calculate higher moments of the energy
            call OExHMomEner(l,CData%NAver)
            call OExHMomEnerC(l)

            call OCalcMeanMag(CData%NAver,0,l)!necessary since no higher moments for the magnetisation
            call OHMomToTMag(CData%NAver,0)
            call OExHMomToTMag(l,CData%NAver)

            call OHMomStagMag(CData%NAver,0)
            call OExHMomStagMag(l,CData%NAver)

            if(CData%saveRaw) call SaveMeasurements()

            nMCStep=CData%NAver*CData%NMCSweep
            AccSteps(:3,l)=real(accCountS,KCMin)/nMCStep
            NMCSteps(2,l)=nMCStep!number of MC steps for measurement
        end do

        !normalization and unit conversion (if necessary)
        call ONormMag(PData(i)%ndata)

        call ONormTotMag(PData(i)%ndata)
        call ONormStagMag(PData(i)%ndata)

        call ONormSuscept(PData(i)%ndata)
        call ONormStagSuscept(PData(i)%ndata)

        call ONormEner(PData(i)%ndata)
        call ONormSpecHeat(PData(i)%ndata)

        !unit conversion
        !calculate parameter array with original units to save them to outputfile
        if(CData%unitConv) then
            if(PData(i)%runlstate) then
                call CalcParaArray(pDataStartO,PDataOrig(i),Para)
            else
                call CalcParaArray(PDataOrig(i-1),PDataOrig(i),Para)
            end if
        end if


        !Save Data
        call WriteLogAndStdO("Saving Data")
        call SaveObservables(PData(i)%ndata)
        call NormAcc(PData(i)%ndata)
        call SaveStats(PData(i)%ndata)

        if(PData(i)%savestate) then!save state
            pDataStart=PData(i) !save parameters
            call SaveStateMin()!save state
            call WriteLogAndStdO("Saved System state.")
        end if
    end do



    !finalize simulation

    call WriteLogAndStdO("Closing Data files")
    call FinFiles()
    call WriteLogAndStdO("Deallocating Variables")
    call Finalize()
    call FinalizePRNG()
    !TODO deallocate routines
    call WriteLogAndStdO(">>Bye Log file!<<")

    call FinLog()

end program Nara
