!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Module containing interface routines with PRNG
!!!! This includes also the dummy routines which will call the default PRNG
!!!! If you want to add a custom PRNG here is the place...
!!!!
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


module NaraPRNG
    use MHKonstanten
    use MixMax256Sm1
    use ISAAC64N256
    use TrivialPRNG
    use NaraError, only: WriteLogAndStdO, WriteWarning
    implicit none


    integer(kind=KDefI),parameter::Nbuffer=1024!size of buffer array
    integer(kind=iprecd)::DPCounter,SPCounter
    real(kind=rprecd),dimension(Nbuffer)::DPBuffer!buffer array
    real(kind=rprecs),dimension(Nbuffer)::SPBuffer!buffer array

    abstract interface
        subroutine InitPRNG_T(seed,jmp)
            import iprecd
            integer(kind=iprecd),intent(in),optional::seed!additional seed value
            integer(kind=iprecd),intent(in),optional::jmp! jmp to different stream... default = 1
        end subroutine InitPRNG_T

        subroutine GetReal32_T(rvec)
            import rprecs
            real(kind=rprecs),intent(out)::rvec
        end subroutine GetReal32_T

        subroutine GetReal32x2_T(rvec)
            import rprecs
            real(kind=rprecs),intent(out)::rvec(2)
        end subroutine GetReal32x2_T

        subroutine GetReal64_T(rvec)
            import rprecd
            real(kind=rprecd),intent(out)::rvec
        end subroutine GetReal64_T

        subroutine GetInt32UpBound_T(rvec,upbound)
            import iprecd
            integer(kind=iprecd),intent(out)::rvec
            integer(kind=iprecd),intent(in)::upbound
        end subroutine GetInt32UpBound_T

!        subroutine FinPRNG_T() !causes problems when call and nullify are in the same subroutine
!        end subroutine FinPRNG_T
    end interface

    procedure(InitPRNG_T), pointer::InitPRNG => null()
    procedure(GetReal32_T), pointer::GetReal32 => null()
    procedure(GetReal32x2_T), pointer::GetReal32x2 => null()
    procedure(GetReal64_T), pointer::GetReal64 => null()
    procedure(GetInt32UpBound_T), pointer::GetInt32UpBound => null()
!    procedure(FinPRNG_T), pointer::FinPRNG => null()



    private
    public:: GetReal32x2, GetReal32, GetReal64, GetInt32UpBound
    public:: SetUPPRNG, FinalizePRNG

    contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!!    Routines for setting up the PRNG
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    subroutine SetUPPRNG()
        use NaraData,only: CData

        select case(CData%PRNG)
            case(-2)
                InitPRNG => InitLCGLFGstate
                GetReal32 => LFGRCReal32
                GetReal32x2 => LFGRCReal32x2S
                GetReal64 => LFGRCReal64
                GetInt32UpBound => LFGRCInt32UpBound
                call WriteWarning()
                call WriteLogAndStdO("Using PRNG: RCARRY")
            case(-1)
                InitPRNG => InitLCGLFGstate
                GetReal32 => LCGRReal32
                GetReal32x2 => LCGRReal32x2S
                GetReal64 => LCGRReal64
                GetInt32UpBound => LCGRInt32UpBound
                call WriteWarning()
                call WriteLogAndStdO("Using PRNG: RANDU")

!            case(0)!do not implement... intended for default case
            case(1)
                InitPRNG => MixMaxInit
                GetReal32 => MixMaxReal32
                GetReal32x2 => MixMaxReal32x2S
                GetReal64 => MixMaxReal64
                GetInt32UpBound => MixMaxInt32UpBound
!                FinPRNG => DefFinPRNG
                call WriteLogAndStdO("Using PRNG: MixMax256Sm1")
            case(2:9)
                InitPRNG => MixMaxInit
                GetReal32 => MixMaxReal32_R
                GetReal32x2 => MixMaxReal32x2S_R
                GetReal64 => MixMaxReal64_R
                GetInt32UpBound => MixMaxInt32UpBound
!                FinPRNG => DefFinPRNG
                call WriteLogAndStdO("Using PRNG: MixMax256Sm1 with Bit Recycling")
            case(10)
                InitPRNG => MixMaxInitStatic
                GetReal32 => MixMaxReal32
                GetReal32x2 => MixMaxReal32x2S
                GetReal64 => MixMaxReal64
                GetInt32UpBound => MixMaxInt32UpBound
!                FinPRNG => DefFinPRNG
                call WriteLogAndStdO("Using PRNG: MixMax256Sm1 with Static Seed")
                call WriteWarning()
                call WriteLogAndStdO("You are using a static seed, this function is only intended for testing purposes!")
            case(11)
                InitPRNG => ISAAC64Init
                GetReal32 => ISAAC64Real32
                GetReal32x2 => ISAAC64Real32x2S
                GetReal64 => ISAAC64Real64
                GetInt32UpBound => ISAAC64Int32UpBound
!                FinPRNG => DefFinPRNG
                call WriteLogAndStdO("Using PRNG: ISAAC64N256")
            case(12:19)
                InitPRNG => ISAAC64Init
                GetReal32 => ISAAC64Real32_R
                GetReal32x2 => ISAAC64Real32x2S_R
                GetReal64 => ISAAC64Real64_R
                GetInt32UpBound => ISAAC64Int32UpBound
!                FinPRNG => DefFinPRNG
                call WriteLogAndStdO("Using PRNG: ISAAC64N256 with Bit Recycling")
            case(20)
                InitPRNG => ISAAC64InitStatic
                GetReal32 => ISAAC64Real32
                GetReal32x2 => ISAAC64Real32x2S
                GetReal64 => ISAAC64Real64
                GetInt32UpBound => ISAAC64Int32UpBound
!                FinPRNG => DefFinPRNG
                call WriteLogAndStdO("Using PRNG: ISAAC64N256 with Static Seed")
                call WriteWarning()
                call WriteLogAndStdO("You are using a static seed, this function is only intended for testing purposes!")
            case default!initialize with compiler default PRNG
                InitPRNG => DefInitPRNG
                GetReal32 => DefGetReal32
                GetReal32x2 => DefGetReal32x2S
                GetReal64 => DefGetReal64
                GetInt32UpBound => DefGetInt32UpBound
!                FinPRNG => DefFinPRNG
                call WriteLogAndStdO("Using PRNG: Compiler Standard")
        end select
        call InitPRNG()
    end subroutine SetUPPRNG

    !finalize PRNG... disassociate pointers and call finalization routine.
    subroutine FinalizePRNG()
!        call FinPRNG()
        nullify(InitPRNG, GetReal32x2, GetReal32, GetReal64, GetInt32UpBound)
    end subroutine FinalizePRNG


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!!    Dummy Routines/Routines for the compilers default PRNG
!!!!    If you use a custom PRNG make sure to provide interfaces to the following routines
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    !Init PRNG
    subroutine DefInitPRNG(seed,jmp)
        integer(kind=iprecd),intent(in),optional::seed!additional seed value
        integer(kind=iprecd),intent(in),optional::jmp! jmp to different stream... default = 1
        call random_seed()
        call random_number(DPBuffer)
        call random_number(SPBuffer)
    end subroutine DefInitPRNG

    !Get SP real [0,1)
    subroutine DefGetReal32(rvec)
        real(kind=rprecs),intent(out)::rvec

        if(SPCounter<=Nbuffer)then
            rvec=SPBuffer(SPCounter)
            SPCounter=SPCounter+1
        else
            call random_number(SPBuffer)
            rvec=SPBuffer(1)
            SPCounter=2
        end if
    end subroutine DefGetReal32


!
!    !Get 2xSP real [0,1)
!    subroutine DefGetReal32x2(rvec)
!        real(kind=rprecs),intent(out),dimension(2)::rvec
!
!        if(SPCounter+1<=Nbuffer)then
!            rvec=SPBuffer(SPCounter:SPCounter+1)
!            SPCounter=SPCounter+2
!        elseif(SPCounter<=Nbuffer)then
!            rvec(1)=SPBuffer(SPCounter)
!            call random_number(SPBuffer)
!            rvec(2)=SPBuffer(1)
!            SPCounter=2
!        else
!            call random_number(SPBuffer)
!            rvec=SPBuffer(:2)
!            SPCounter=3
!        end if
!    end subroutine DefGetReal32x2

    !Get 2xSP real
    ! first one in the interval [0.0;2Pi)
    ! second one in interval [-1.0;1.0)
    subroutine DefGetReal32x2S(rvec)
        real(kind=rprecs),intent(out),dimension(2)::rvec

        if(SPCounter+1<=Nbuffer)then
            rvec=SPBuffer(SPCounter:SPCounter+1)
            SPCounter=SPCounter+2
        elseif(SPCounter<=Nbuffer)then
            rvec(1)=SPBuffer(SPCounter)
            call random_number(SPBuffer)
            rvec(2)=SPBuffer(1)
            SPCounter=2
        else
            call random_number(SPBuffer)
            rvec=SPBuffer(:2)
            SPCounter=3
        end if
        rvec(1)=rvec(1)*Pi2_s!NOTE maybe problem with range since sould be rvec[-1,1] and rvec[0,2Pi)
        rvec(2)=rvec(2)*2.0_rprecs-1.0_rprecs
    end subroutine DefGetReal32x2S

    !Get DP real [0,1)
    subroutine DefGetReal64(rvec)
        real(kind=rprecd),intent(out)::rvec
        if(DPCounter>Nbuffer) then
            call random_number(DPBuffer)
            DPCounter=1
        end if
        rvec=DPBuffer(DPCounter)
        DPCounter=DPCounter+1
    end subroutine DefGetReal64

    !Get Double Word Integer with upper bound
    subroutine DefGetInt32UpBound(rndint,upbound)
        integer(kind=iprecd),intent(out)::rndint
        integer(kind=iprecd),intent(in)::upbound!upper bound

        if(DPCounter>Nbuffer) then
            call random_number(DPBuffer)
            DPCounter=1
        end if
        rndint=int(DPBuffer(DPCounter)*upbound)+1_iprecd
        DPCounter=DPCounter+1
    end subroutine DefGetInt32UpBound

    !dummy routine for finalization of PRNG... not needed for default PRNG
!    subroutine DefFinPRNG()
!        return
!    end subroutine DefFinPRNG

end module NaraPRNG
