!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------

!For the Conversion from Int to Real this PRNG fills the fraction of the real with a random bit pattern and
! sets the exponent far a value between 1.0 and 2.0 ... then it substracts 1.0 to fit it into the range [0.0,1.0)
! the multiplication with the inverse of the base is not done since this would lead to a range (0.0,1,0]
! also it is apparently a tiny bit faster (at least on my machine)



!TODO fix all Int32 and Int64 collisions in bit operations
!TODO fix all constants to include kind parameter
!TODO add jump ahead routine

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! An implementation of the MIXMAX PRNG with N=256, s=1 and a luxury level of 4 (4 calls are discarded 5th call is used).
!!!!
!!!! Relevant Papers:
!!!!
!!!! K. G. Savvidy, �The MIXMAX random number generator,� vol. 196, pp. 161�165, Nov. 2015.
!!!! F. James and L. Moneta, �Review of High-Quality Random Number Generators,� vol. 4, no. 1, Jan. 2020.
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module MixMax256Sm1
    use MHKonstanten, only : rprecs,rprecd,iprecd,iprecq!Precision stuff
    use MHKonstanten, only : Pi2_s !2Pi
    use MHKonstanten, only : r32expA2t4,r32expA,r64exp,r32mantA !constants for integer to float conversion
    implicit none
    !Todo check if state can reach either 2**61-1 or 0. If the former need to shift bits/substract one
    !FIXME Cite papers
    !TODO MVBITS vs ieor, ior, iand... for copying bits

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Definition of the Kind parameters
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer(kind=iprecd),parameter::N=256!,S=-1
    integer(kind=iprecq),parameter::Mers2h61=2_iprecq**61-1_iprecq
    integer(kind=iprecd),parameter::Bits61=61

    integer(kind=iprecq),dimension(N)::PRNGstate!state Vector
    integer(kind=iprecq)::Bsum!B_N in the Algorithm
    integer(kind=iprecd)::Counter!Counter of PRNG

    integer(kind=iprecq)::BitStore!Storage for unused Bits
    integer(kind=iprecd)::NBitsStorage!Number of bits in Storage

    private
    public :: MixMaxInit, MixMaxInitStatic, MixMaxNextState
    public :: MixMaxInt32UpBound
    public :: MixMaxReal32,MixMaxReal64
    public :: MixMaxReal64_R,MixMaxReal32_R !, MixMaxReal32x2,MixMaxReal32x2_R
    public :: MixMaxReal32x2S,MixMaxReal32x2S_R



    contains


    !seeds MixMax using system time, date and clock
    ! and digits of pi, eor operations and circular shifts, and eor with previouse states
    subroutine MixMaxInit(seed,jmp)
        use SleeveNumber, only: SleevePi
        integer(kind=iprecd),intent(in),optional::seed!additional seed value
        integer(kind=iprecd),intent(in),optional::jmp! jmp to different stream... default = 1
        integer(kind=iprecd)::i
        integer(kind=iprecq),dimension(9)::timedate

        !generate seed
        call date_and_time(values=timedate(:8))
        call system_clock(count=timedate(9))

        Bsum=0
        PRNGstate(1)=sum(timedate)+1234!get pseudo "random" seed

        if(present(seed)) PRNGstate(1)=PRNGstate(1)+seed!if seed is present add to first state

        do i=2,N
            PRNGstate(i)=ieor(timedate(modulo(i,9)+1),SleevePi(modulo(i,30)+1))
            PRNGstate(i)=ieor(PRNGstate(i),ishftc(PRNGstate(i-1),i))
        enddo
        PRNGstate(1)=ibits(PRNGstate(1),0,bit_size(PRNGstate)-3)
        do i=2,N
            PRNGstate(i)=ibits(PRNGstate(i),0,bit_size(PRNGstate)-3)!Discard sign bit and upper bit so it is positive and we emulate the modulo
            Bsum=ModMers2h61(Bsum+PRNGstate(i))!Bsum does not include PRNGstate(1)
        end do

        do i=1,10000
            call MixMaxNextState()
        end do

        BitStore=0
        NBitsStorage=0
    end subroutine MixMaxInit

    !seeds MixMax using the static seed 0, 1 ,2 ,3 , 4 etc...
    subroutine MixMaxInitStatic(seed,jmp)
        integer(kind=iprecd),intent(in),optional::seed!additional seed value
        integer(kind=iprecd),intent(in),optional::jmp! jmp to different stream... default = 1
        integer(kind=iprecd)::i

        Bsum=0
        PRNGstate(1)=0

        if(present(seed)) PRNGstate(1)=PRNGstate(1)+seed!if seed is present add to first state

        do i=2,N
            PRNGstate(i)=i-1
            Bsum=ModMers2h61(Bsum+PRNGstate(i))!Bsum does not include PRNGstate(1)
        end do

        do i=1,10000
            call MixMaxNextState()
        end do

        BitStore=0
        NBitsStorage=0
    end subroutine MixMaxInitStatic

    !make one iteration
    subroutine MixMaxNextState()
        integer(kind=iprecq)::btemp,atemp
        integer(kind=iprecd)::i,j
        Counter=1_iprecd

        do j=1,5
            PRNGstate(1)=ModMers2h61(PRNGstate(1)+Bsum)

            atemp=PRNGstate(2)
            btemp=PRNGstate(2)
            PRNGstate(2)=ModMers2h61(PRNGstate(2)+PRNGstate(1))
            Bsum=PRNGstate(2)

            btemp=ModMers2h61(btemp+PRNGstate(3))
            PRNGstate(3)=ModMers2h61(btemp+PRNGstate(2))

            btemp=ModMers2h61(btemp+PRNGstate(4))
            PRNGstate(4)=ModMers2h61(btemp+PRNGstate(3))
            Bsum=ModMers2h61(Bsum+PRNGstate(4))

            PRNGstate(3)=ModMers2h61(PRNGstate(3)+(Mers2h61-atemp))!correction with magic value s=-1
            Bsum=ModMers2h61(Bsum+PRNGstate(3))
            do i=5,N
                btemp=ModMers2h61(btemp+PRNGstate(i))
                PRNGstate(i)=ModMers2h61(btemp+PRNGstate(i-1))
                Bsum=ModMers2h61(Bsum+PRNGstate(i))
            end do
        end do
    end subroutine MixMaxNextState

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Extraction Routines
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!------------Real-32-Bit---------------------------------------------------------------------------

!    subroutine GetReal32(rndreal)
!        real(kind=rprecs),intent(out)::rndreal
!        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
!        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
!        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
!        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
!        ! and the sign bit is zero in the state vector.
!        rndreal=transfer(ior(iand(int(PRNGstate(Counter),kind(r32mant)),r32mant),r32exp),1.0_rprecs)-1.0_rprecs
!        Counter=Counter+1
!    end subroutine GetReal32


    subroutine MixMaxReal32(rndreal)
        real(kind=rprecs),intent(out)::rndreal
        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
        ! and the sign bit is zero in the state vector.
        rndreal=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        Counter=Counter+1
    end subroutine MixMaxReal32

    subroutine MixMaxReal32_R(rndreal)
        real(kind=rprecs),intent(out)::rndreal
        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
        ! and the sign bit is zero in the state vector.
        rndreal=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        call RecycleBits(38)
        Counter=Counter+1
    end subroutine MixMaxReal32_R

!    !extracts two SP reals from the same state
!    subroutine MixMaxReal32x2(rndreal)
!        real(kind=rprecs),intent(out)::rndreal(2)
!        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
!        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
!        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
!        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
!        ! and the sign bit is zero in the state vector.
!        rndreal(1)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        rndreal(2)=transfer(ior(iand(shiftr(PRNGstate(Counter),23),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        Counter=Counter+1
!    end subroutine MixMaxReal32x2
!
!
!    !extracts two SP reals from the same state
!    subroutine MixMaxReal32x2_R(rndreal)
!        real(kind=rprecs),intent(out)::rndreal(2)
!        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
!        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
!        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
!        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
!        ! and the sign bit is zero in the state vector.
!        rndreal(1)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        rndreal(2)=transfer(ior(iand(shiftr(PRNGstate(Counter),23),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        call RecycleBits(15)
!        Counter=Counter+1
!    end subroutine MixMaxReal32x2_R

    !extracts two SP reals from the same state SPECIAL ROUTINE
    ! first one in the interval [0.0;2Pi)
    ! second one in interval [-1.0;1.0)
    subroutine MixMaxReal32x2S(rndreal)
        real(kind=rprecs),intent(out)::rndreal(2)
        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
        rndreal(1)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        rndreal(1)=rndreal(1)*Pi2_s
        rndreal(2)=transfer(ior(iand(shiftr(PRNGstate(Counter),23),r32mantA),r32expA2t4),1.0_rprecs)-3.0_rprecs
        Counter=Counter+1
    end subroutine MixMaxReal32x2S

    !extracts two SP reals from the same state SPECIAL ROUTINE
    ! first one in the interval [0.0;2Pi)
    ! second one in interval [-1.0;1.0)
    !with bit recycling
    subroutine MixMaxReal32x2S_R(rndreal)
        real(kind=rprecs),intent(out)::rndreal(2)
        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
        rndreal(1)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        rndreal(1)=rndreal(1)*Pi2_s
        rndreal(2)=transfer(ior(iand(shiftr(PRNGstate(Counter),23),r32mantA),r32expA2t4),1.0_rprecs)-3.0_rprecs
        call RecycleBits(15)
        Counter=Counter+1
    end subroutine MixMaxReal32x2S_R


!    !extracts three SP reals
!    subroutine MixMaxReal32x3(rndreal)
!        real(kind=rprecs),intent(out)::rndreal(3)
!        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
!        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
!        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
!        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
!        ! and the sign bit is zero in the state vector.
!        rndreal(1)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        rndreal(2)=transfer(ior(iand(shiftr(PRNGstate(Counter),23),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        call RecycleBits(15)
!        Counter=Counter+1
!        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
!        rndreal(3)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        call RecycleBits(38)
!        Counter=Counter+1
!    end subroutine MixMaxReal32x3


!------------Real-64-Bit---------------------------------------------------------------------------

    subroutine MixMaxReal64(rndreal)
        real(kind=rprecd),intent(out)::rndreal
        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
        ! and the sign bit is zero in the state vector.
        rndreal=transfer(ior(PRNGstate(Counter),r64exp),1.0_rprecd)-1.0_rprecd
        Counter=Counter+1
    end subroutine MixMaxReal64

    subroutine MixMaxReal64_R(rndreal)
        real(kind=rprecd),intent(out)::rndreal
        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
        ! and the sign bit is zero in the state vector.
        rndreal=transfer(ior(PRNGstate(Counter),r64exp),1.0_rprecd)-1.0_rprecd
        call RecycleBits(9)
        Counter=Counter+1
    end subroutine MixMaxReal64_R




!    subroutine MixMaxReal64Vec(rndreal)
!        real(kind=rprecd),intent(out),dimension(:)::rndreal
!
!        integer(kind=iprecd)::asize,eElem,sindex,findex,f2index!number of elements, empty elements in rndreal, start and end index of rnd real; end index of PRNG buffer
!        integer(kind=iprecd)::i
!
!        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
!
!        asize=size(rndreal)
!        f2index = asize + Counter - 1
!        if(f2index <= N) then!enough values in buffer array
!            !explanation see scalar equivalent
!            rndreal=transfer(ior(PRNGstate(Counter:f2index),r64exp),1.0_rprecd,asize)-1.0_rprecd
!
!        else!buffer array is either too small or has not enough values left
!            !use rest of buffer array
!            findex = N-Counter+1
!            rndreal(:findex)=transfer(ior(PRNGstate(Counter:),r64exp),1.0_rprecd,findex)-1.0_rprecd
!            eElem=asize-findex
!            call MixMaxNextState()
!
!            !multiple full buffer arrays are needed
!            do i = N,eElem,N
!                sindex=findex+1
!                findex=findex+N
!                rndreal(sindex:findex)=transfer(ior(PRNGstate,r64exp),1.0_rprecd,N)-1.0_rprecd
!                call MixMaxNextState()
!            end do
!
!            !fill last bit with a new buffer array
!            sindex=findex+1
!            f2index=asize-findex
!            rndreal(sindex:)=transfer(ior(PRNGstate(:f2index),r64exp),1.0_rprecd,f2index)-1.0_rprecd
!        end if
!
!        Counter = f2index + 1
!    end subroutine MixMaxReal64Vec


!------------Integer-32-Bit---------------------------------------------------------------------------

!    !return random 32bit integer (positive and negative)
!    subroutine MixMaxInt32(rndint)
!        integer(kind=iprecd),intent(out)::rndint
!        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
!        rndint=int(PRNGstate(Counter),kind(rndint)) !copy only lower 32 bits
!!        rndint=iand(not(0_iprecd),PRNGstate(Counter))!copy only lower 32 bits
!        Counter= Counter + 1
!    end subroutine MixMaxInt32


    !return random a positive 32 bit integer between [1;upbound]
    !using rejection method and bit recycling
    !TODO add calling RecycleBits(freebits)
    subroutine MixMaxInt32UpBound(rndint,upbound)
        integer(kind=iprecd),intent(out)::rndint
        integer(kind=iprecd),intent(in)::upbound

        integer(kind=iprecd),parameter::bits=bit_size(bits)
        integer(kind=iprecd):: bmask,neededbits,usedbits
        if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.

        !get next larger base 2
        neededbits=bits-leadz(upbound)
        bmask=ibset(0,neededbits)-1!get next larger power of 2 ... mask is all lower bits set to 1
        rndint=iand(bmask,int(PRNGstate(Counter),iprecd))

        rej_loop: do while(rndint>=upbound)!rndint can be 0 so I have to add later 1 to be in the desired range
            do usedbits=neededbits,60,neededbits!Todo check if it is 60 or 61
                rndint=iand(bmask,int(shiftr(PRNGstate(Counter),usedbits),iprecd))
                if (rndint<upbound) exit rej_loop
            end do
            Counter = Counter + 1
            if(Counter>N) call MixMaxNextState()!Buffer array is empty refill it.
            rndint=iand(bmask,int(PRNGstate(Counter),iprecd))
        end do rej_loop
        rndint=rndint+1
        Counter = Counter + 1
    end subroutine MixMaxInt32UpBound


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Auxiliary Math Routines and functions
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    !modulo Mersenne 2**61-1
    elemental function ModMers2h61(dividend)
        integer(kind=iprecq):: ModMers2h61
        integer(kind=iprecq),intent(in):: dividend
        ModMers2h61 = (iand(dividend,Mers2h61)+shiftr(dividend,Bits61))

        !only one call needed, since dividend should not exceed 2*Mers2h61
      !  ModMers2h61 = (iand(ModMers2h61,Mers2h61)+shiftr(ModMers2h61,Bits61))!necessary if result > 2**61-1 !TODO check whether dividend is always <2**61-1

      !technically I would also need to check whether ModMers2h61==Mers2h61 and set it then to 0
      !But doesn't matter too much, since this way we include ModMers2h61 instead of 0.
    end function ModMers2h61

    !add unused Bits to a Buffer, for later use
    !if Buffer is full replace last used state
    !recycles top most bits.
    !intended to be called after usage of PRNGstate(Counter) but before increment of Counter
    !best call it immediately before increment
    subroutine RecycleBits(nbits)
        integer(kind=iprecd),intent(in)::nbits!number of topmost unused bits, not counting the ones discarded by emptyshift

        integer(kind=iprecd)::shiftB
        integer(kind=iprecq)::interstorage!intermediate Bit storage
        integer(kind=iprecd),parameter::emptyshift=3!shift needed to align unused bits to the left

        if(nbits+NBitsStorage<=Bits61) then!is there still place in our buffer?
            BitStore=dshiftl(BitStore,shiftl(PRNGstate(Counter),emptyshift),nbits)
            NBitsStorage=NBitsStorage+nbits
            if(NBitsStorage>=Bits61) then
                PRNGstate(Counter)=BitStore
                BitStore=0
                NBitsStorage=0
                Counter=Counter-1
            end if
        else
            shiftB=Bits61-NBitsStorage
            BitStore=dshiftl(BitStore,shiftl(PRNGstate(Counter),emptyshift),shiftB)
            interstorage=BitStore
            BitStore=0
            NBitsStorage=nbits-shiftB
            BitStore=dshiftl(BitStore,shiftl(PRNGstate(Counter),emptyshift+shiftB),NBitsStorage)
            PRNGstate(Counter)=interstorage
            Counter=Counter-1
        end if

    end subroutine RecycleBits

end module MixMax256Sm1
