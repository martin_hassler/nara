!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------

!For the Conversion from Int to Real this PRNG fills the fraction of the real with a random bit pattern and
! sets the exponent far a value between 1.0 and 2.0 ... then it substracts 1.0 to fit it into the range [0.0,1.0)
! the multiplication with the inverse of the base is not done since this would lead to a range (0.0,1,0]
! it is apparently a tiny bit faster (at least on my machine).


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Module containing interface routines with CSPRNG ISAAC64
!!!! Interface definition for the (modified) standard implementation of ISAAC64 by Bob Jenkins.
!!!!
!!!!
!!!! Relevant Papers: R. J. Jenkins, �ISAAC,� in Fast Software Encryption, Berlin, Heidelberg, 1996, pp. 41�49.
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module ISAAC64N256
    use,intrinsic :: iso_c_binding
    use MHKonstanten, only : rprecs,rprecd,iprecd,iprecq!Precision stuff
    use MHKonstanten, only : Pi2_s !2Pi
    use MHKonstanten, only : r32expA2t4,r32expA,r64exp,r32mantA,r64mant !constants for integer to float conversion
    implicit none

    integer(kind=iprecd),parameter::N=shiftl(1,8)!FIXME make portable... bind to original definition  in isaac64.h
    integer(kind=c_int_fast64_t),dimension(N), bind(C,name='randrsl')::PRNGstate

    integer(kind=iprecd)::Counter!Number of Random numbers used
    integer(kind=iprecq)::BitStore!Storage for unused Bits
    integer(kind=iprecd)::NBitsStorage!Number of bits in Storage

    private
    public :: ISAAC64Init, ISAAC64InitStatic, ISAAC64NextState
    public :: ISAAC64Int32UpBound
    public :: ISAAC64Real32,ISAAC64Real64! ,ISAAC64Real32x2,ISAAC64Real32x2_R,
    public :: ISAAC64Real32_R,ISAAC64Real64_R
    public :: ISAAC64Real32x2S,ISAAC64Real32x2S_R

    interface
        subroutine ISAACInit(flag) bind(C,name='randinit')
            use,intrinsic :: iso_c_binding, only : c_int_fast32_t
            integer(kind=c_int_fast32_t),value,intent(in)::flag
        end subroutine ISAACInit

        subroutine ISAAC64NextState() bind(C,name='isaac64')
        end subroutine ISAAC64NextState

    end interface

    contains

    !seeds ISAAC using the static seed 0, 1 ,2 ,3 , 4 etc...
    subroutine ISAAC64InitStatic(seed,jmp)
        integer(kind=iprecd),intent(in),optional::seed!additional seed value
        integer(kind=iprecd),intent(in),optional::jmp! jmp to different stream... default = 1
        integer(kind=iprecd)::i

        PRNGstate(1)=0
        if(present(seed)) PRNGstate(1)=PRNGstate(1)+seed!if seed is present add to first state

        do i=2,N
            PRNGstate(i)=i-1
        end do

        !initialize CSPRNG
        call ISAACInit(1_c_int_fast32_t)
        do i=1,10
            call ISAAC64NextState()
        end do

        Counter=1
    end subroutine ISAAC64InitStatic

    !seeds ISAAC using system time, date and clock
    ! and digits of pi, eor operations and circular shifts, and eor with previous states
    subroutine ISAAC64Init(seed,jmp)
        use SleeveNumber, only: SleevePi
        integer(kind=iprecd),intent(in),optional::seed!additional seed value
        integer(kind=iprecd),intent(in),optional::jmp! jmp to different stream... default = 1
        integer(kind=iprecd)::i
        integer(kind=iprecq),dimension(9)::timedate

        !generate seed
        call date_and_time(values=timedate(:8))
        call system_clock(count=timedate(9))

        PRNGstate(1)=sum(timedate)+1234!get pseudo "random" seed

        if(present(seed)) PRNGstate(1)=PRNGstate(1)+seed!if seed is present add to first state

        do i=2,N
            PRNGstate(i)=ieor(timedate(modulo(i,9)+1),SleevePi(modulo(i,30)+1))
            PRNGstate(i)=ieor(PRNGstate(i),ishftc(PRNGstate(i-1),i))
        enddo

        do i=1,N
            PRNGstate(i)=ibits(PRNGstate(i),0,bit_size(PRNGstate)-2)!Discard sign bit and uppermost bit, probably not needed
        enddo

        !initialize CSPRNG
        call ISAACInit(1_c_int_fast32_t)
        do i=1,10
            call ISAAC64NextState()
        end do

        Counter=1
    end subroutine ISAAC64Init

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Extraction Routines
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!------------Real-32-Bit---------------------------------------------------------------------------

    subroutine ISAAC64Real32(rndreal)
        real(kind=rprecs),intent(out)::rndreal
        if(Counter>N) then!Buffer array is empty refill it.
            call ISAAC64NextState()
            Counter = 1
        end if
        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
        ! and the sign bit is zero in the state vector.
        rndreal=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        Counter=Counter+1
    end subroutine ISAAC64Real32

    subroutine ISAAC64Real32_R(rndreal)
        real(kind=rprecs),intent(out)::rndreal
        if(Counter>N) then!Buffer array is empty refill it.
            call ISAAC64NextState()
            Counter = 1
        end if
        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
        ! and the sign bit is zero in the state vector.
        rndreal=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        call RecycleBits(41)
        Counter=Counter+1
    end subroutine ISAAC64Real32_R

!    !extracts two SP reals from the same state
!    subroutine ISAAC64Real32x2(rndreal)
!        real(kind=rprecs),intent(out)::rndreal(2)
!        if(Counter>N) then!Buffer array is empty refill it.
!            call ISAAC64NextState()
!            Counter = 1
!        end if
!        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
!        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
!        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
!        ! and the sign bit is zero in the state vector.
!        rndreal(1)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        rndreal(2)=transfer(ior(iand(shiftr(PRNGstate(Counter),23),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        Counter=Counter+1
!    end subroutine ISAAC64Real32x2
!
!
!    !extracts two SP reals from the same state
!    subroutine ISAAC64Real32x2_R(rndreal)
!        real(kind=rprecs),intent(out)::rndreal(2)
!        if(Counter>N) then!Buffer array is empty refill it.
!            call ISAAC64NextState()
!            Counter = 1
!        end if
!        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
!        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
!        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
!        ! and the sign bit is zero in the state vector.
!        rndreal(1)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        rndreal(2)=transfer(ior(iand(shiftr(PRNGstate(Counter),23),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        call RecycleBits(18)
!        Counter=Counter+1
!    end subroutine ISAAC64Real32x2_R

    !extracts two SP reals from the same state SPECIAL ROUTINE
    ! first one in the interval [0.0;2Pi)
    ! second one in interval [-1.0;1.0)
    subroutine ISAAC64Real32x2S(rndreal)
        real(kind=rprecs),intent(out)::rndreal(2)
        if(Counter>N) then!Buffer array is empty refill it.
            call ISAAC64NextState()
            Counter = 1
        end if
        rndreal(1)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        rndreal(1)=rndreal(1)*Pi2_s
        rndreal(2)=transfer(ior(iand(shiftr(PRNGstate(Counter),23),r32mantA),r32expA2t4),1.0_rprecs)-3.0_rprecs
        Counter=Counter+1
    end subroutine ISAAC64Real32x2S


    !extracts two SP reals from the same state SPECIAL ROUTINE
    ! first one in the interval [0.0;2Pi)
    ! second one in interval [-1.0;1.0)
    !with bit recycling
    subroutine ISAAC64Real32x2S_R(rndreal)
        real(kind=rprecs),intent(out)::rndreal(2)
        if(Counter>N) then!Buffer array is empty refill it.
            call ISAAC64NextState()
            Counter = 1
        end if
        rndreal(1)=transfer(ior(iand(PRNGstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        rndreal(1)=rndreal(1)*Pi2_s
        rndreal(2)=transfer(ior(iand(shiftr(PRNGstate(Counter),23),r32mantA),r32expA2t4),1.0_rprecs)-3.0_rprecs
        call RecycleBits(18)
        Counter=Counter+1
    end subroutine ISAAC64Real32x2S_R


!------------Real-64-Bit---------------------------------------------------------------------------

    subroutine ISAAC64Real64(rndreal)
        real(kind=rprecd),intent(out)::rndreal
        if(Counter>N) then!Buffer array is empty refill it.
            call ISAAC64NextState()
            Counter = 1
        end if
        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
        ! and the sign bit is zero in the state vector.
        rndreal=transfer(ior(iand(PRNGstate(Counter),r64mant),r64exp),1.0_rprecd)-1.0_rprecd
        Counter=Counter+1
    end subroutine ISAAC64Real64

    subroutine ISAAC64Real64_R(rndreal)
        real(kind=rprecd),intent(out)::rndreal
        if(Counter>N) then!Buffer array is empty refill it.
            call ISAAC64NextState()
            Counter = 1
        end if
        !copy bits for mantissa and set exponent for values between 1.0 and 2.0
        !I can use ior because r64exp has only bits set to 0 in the region where the mantissa is,
        ! all bits in exponent are set to 1 so it copies the mantissa and sets the exponent in one go.
        ! and the sign bit is zero in the state vector.
        rndreal=transfer(ior(iand(PRNGstate(Counter),r64mant),r64exp),1.0_rprecd)-1.0_rprecd
        call RecycleBits(12)
        Counter=Counter+1
    end subroutine ISAAC64Real64_R

!------------Integer-32-Bit---------------------------------------------------------------------------

    !return random a positive 32 bit integer between [1;upbound]
    !using rejection method and bit recycling
    !TODO add calling RecycleBits(freebits)
    subroutine ISAAC64Int32UpBound(rndint,upbound)
        integer(kind=iprecd),intent(out)::rndint
        integer(kind=iprecd),intent(in)::upbound

        integer(kind=iprecd),parameter::bits=bit_size(bits)
        integer(kind=iprecd):: bmask,neededbits,usedbits
        if(Counter>N) then!Buffer array is empty refill it.
            call ISAAC64NextState()
            Counter = 1
        end if

        !get next larger base 2
        neededbits=bits-leadz(upbound)
        bmask=ibset(0,neededbits)-1!get next larger power of 2 ... mask is all lower bits set to 1
        rndint=iand(bmask,int(PRNGstate(Counter),iprecd))

        rej_loop: do while(rndint>=upbound)!rndint can be 0 so I have to add later 1 to be in the desired range
            do usedbits=neededbits,63,neededbits!Todo check if it is 60 or 61
                rndint=iand(bmask,int(shiftr(PRNGstate(Counter),usedbits),iprecd))
                if (rndint<upbound) exit rej_loop
            end do
            Counter = Counter + 1
            if(Counter>N) then!Buffer array is empty refill it.
            call ISAAC64NextState()
            Counter = 1
        end if
            rndint=iand(bmask,int(PRNGstate(Counter),iprecd))
        end do rej_loop
        rndint=rndint+1
        Counter = Counter + 1
    end subroutine ISAAC64Int32UpBound


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Auxiliary Math Routines and functions
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    !add unused Bits to a Buffer, for later use
    !if Buffer is full replace last used state
    !recycles top most bits.
    !intended to be called after usage of PRNGstate(Counter) but before increment of Counter
    !best call it immediately before increment
    subroutine RecycleBits(nbits)
        integer(kind=iprecd),intent(in)::nbits!number of topmost unused bits

        integer(kind=iprecd)::shiftB
        integer(kind=iprecq)::interstorage!intermediate Bit storage

        if(nbits+NBitsStorage<=64) then!is there still place in our buffer?
            BitStore=dshiftl(BitStore,PRNGstate(Counter),nbits)
            NBitsStorage=NBitsStorage+nbits
            if(NBitsStorage>=64) then
                PRNGstate(Counter)=BitStore
                BitStore=0
                NBitsStorage=0
                Counter=Counter-1
            end if
        else
            shiftB=64-NBitsStorage
            BitStore=dshiftl(BitStore,PRNGstate(Counter),shiftB)
            interstorage=BitStore
            BitStore=0
            NBitsStorage=nbits-shiftB
            BitStore=dshiftl(BitStore,shiftl(PRNGstate(Counter),shiftB),NBitsStorage)
            PRNGstate(Counter)=interstorage
            Counter=Counter-1
        end if

    end subroutine RecycleBits


end module ISAAC64N256
