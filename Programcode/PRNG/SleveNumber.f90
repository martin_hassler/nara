!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! This module contains arrays of Numbers used for the seeding of PRNGs
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module SleeveNumber
    use MHKonstanten, only: iprecq
    implicit none

    !contains digits of Pi
    integer(kind=iprecq), parameter ,dimension(30)::SleevePi = [&
        314159265358979323_iprecq,&
        846264338327950288_iprecq,&
        419716939937510582_iprecq,&
        097494459230781640_iprecq,&
        628620899862803482_iprecq,&
        534211706798214808_iprecq,&
        651328230664709384_iprecq,&
        460955058223172535_iprecq,&
        940812848111745028_iprecq,&
        410270193852110555_iprecq,&
        964462294895493038_iprecq,&
        196442881097566593_iprecq,&
        344612847564823378_iprecq,&
        678316527120190914_iprecq,&
        564856692346034861_iprecq,&
        045432664821339360_iprecq,&
        726024914127372458_iprecq,&
        700660631558817488_iprecq,&
        152092096282925409_iprecq,&
        171536436789259036_iprecq,&
        001133053054882046_iprecq,&
        652138414695194151_iprecq,&
        160943305727036575_iprecq,&
        959195309218611738_iprecq,&
        193261179310511854_iprecq,&
        807446237996274956_iprecq,&
        735188575272489122_iprecq,&
        793818301194912983_iprecq,&
        367336244065664308_iprecq,&
        602139494639522473_iprecq]

    private
    public :: SleevePi
end module SleeveNumber
