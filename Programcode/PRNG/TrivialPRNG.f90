!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! An implementation of several "Trivial" PRNG-functions
!!!!
!!!!
!!!! Relevant Papers:
!!!! RCARRY: G. Marsaglia and A. Zaman, �A New Class of Random Number Generators,� vol. 1, no. 3, pp. 462�480, 1991.
!!!! RANDU: J. E. Gentle, Random Number Generation and Monte Carlo Methods, 2nd ed. Springer New York, 2004.
!!!!
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module TrivialPRNG
    use,intrinsic :: iso_c_binding
    use MHKonstanten, only : rprecs,rprecd,iprecd,iprecq!Precision stuff
    use MHKonstanten, only : Pi2_s !2Pi
    use MHKonstanten, only : r32expA2t4,r32expA,r64exp,r32mantA,r64mant !constants for integer to float conversion
    implicit none

    private
    public:: InitLCGLFGstate,LFGRCInt32UpBound, LFGRCReal32, LFGRCReal64
    public:: LCGRInt32UpBound,LCGRReal32, LCGRReal64
    public:: LFGRCReal32x2S, LCGRReal32x2S

    integer,parameter::N=100!state size
    integer(kind=iprecq),dimension(N):: rngstate
    integer:: Counter

    contains


    !init state used by randu and rcarry
    subroutine InitLCGLFGstate(seed,jmp)
        use SleeveNumber, only: SleevePi
        integer(kind=iprecd),intent(in),optional::seed!additional seed value
        integer(kind=iprecd),intent(in),optional::jmp! jmp to different stream... default = 1
        integer(kind=iprecd)::i
        integer(kind=iprecq),dimension(9)::timedate

        !generate seed
        call date_and_time(values=timedate(:8))
        call system_clock(count=timedate(9))

        rngstate(1)=sum(timedate)+1234!get pseudo "random" seed

        if(present(seed)) rngstate(1)=rngstate(1)+seed!if seed is present add to first state

        do i=2,N
            rngstate(i)=ieor(timedate(modulo(i,9)+1),SleevePi(modulo(i,30)+1))
            rngstate(i)=ieor(rngstate(i),ishftc(rngstate(i-1),i))
        enddo

        do i=1,N
            rngstate(i)=ibits(rngstate(i),0,22)!Discard sign bit and uppermost bits, probably not needed
            rngstate(i)=ibset(rngstate(i),0)!make uneven
        enddo

        Counter=N+1
    end subroutine InitLCGLFGstate

    !Yay it's the horrible RANDU
    subroutine LCGRandu()
        integer::i
        rngstate(1)=iand(65539_iprecq*rngstate(N),2_iprecq**31-1_iprecq)
        do i=2,N
            rngstate(i)= iand(65539_iprecq*rngstate(i-1),2_iprecq**31-1_iprecq)
        end do
        Counter=1
    end subroutine LCGRandu

    subroutine LFGRcarry()
        integer(kind=iprecq),parameter::modulus=2_iprecq**24
        integer(kind=iprecq),parameter::modulus2=2_iprecq**24-1_iprecq
        integer::j
        rngstate(N-24:)=rngstate(:25)
        do j=N-25,1,-1
            rngstate(j)=rngstate(j+25)-rngstate(j+10)
            if(rngstate(j)<0) rngstate(j)=rngstate(j) + modulus
            rngstate(j)=iand(rngstate(j),modulus2)!is basically the same as the modulo operation
        end do
        rngstate(26:)=rngstate(:N-25)
        do j=24,1,-1
            rngstate(j)=rngstate(j+25)-rngstate(j+10)
            if(rngstate(j)<0) rngstate(j)=rngstate(j) + modulus
            rngstate(j)=iand(rngstate(j),modulus2)
        end do
        Counter=1
    end subroutine LFGRcarry

!------------Extraction Routines---------------------------------------------------------------------------


     subroutine LFGRCReal32(rndreal)
        real(kind=rprecs),intent(out)::rndreal
        if(Counter>N) call LFGRcarry()
        rndreal=transfer(ior(iand(rngstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        Counter=Counter+1
    end subroutine LFGRCReal32

!    !extracts two SP reals from the same state
!    subroutine LFGRCReal32x2(rndreal)
!        real(kind=rprecs),intent(out)::rndreal(2)
!        if(Counter>N) call LFGRcarry()
!        rndreal(1)=transfer(ior(iand(int(rngstate(Counter),iprecd),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        Counter=Counter+1
!        if(Counter>N) call LFGRcarry()
!        rndreal(2)=transfer(ior(iand(int(rngstate(Counter),iprecd),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        Counter=Counter+1
!    end subroutine LFGRCReal32x2

     subroutine LCGRReal32(rndreal)
        real(kind=rprecs),intent(out)::rndreal
        if(Counter>N) call LCGRandu()
        rndreal=transfer(ior(iand(rngstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        Counter=Counter+1
    end subroutine LCGRReal32
!
!    !extracts two SP reals from the same state
!    subroutine LCGRReal32x2(rndreal)
!        real(kind=rprecs),intent(out)::rndreal(2)
!        if(Counter>N) call LCGRandu()
!        rndreal(1)=transfer(ior(iand(int(rngstate(Counter),iprecd),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        Counter=Counter+1
!        if(Counter>N) call LCGRandu()
!        rndreal(2)=transfer(ior(iand(int(rngstate(Counter),iprecd),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
!        Counter=Counter+1
!    end subroutine LCGRReal32x2

    !extracts two SP reals from the same state SPECIAL ROUTINE
    ! first one in the interval [0.0;2Pi)
    ! second one in interval [-1.0;1.0)
    subroutine LFGRCReal32x2S(rndreal)
        real(kind=rprecs),intent(out)::rndreal(2)
        if(Counter>N) call LFGRcarry()!Buffer array is empty refill it.
        rndreal(1)=transfer(ior(iand(rngstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        rndreal(1)=rndreal(1)*Pi2_s
        Counter=Counter+1
        if(Counter>N) call LFGRcarry()
        rndreal(2)=transfer(ior(iand(rngstate(Counter),r32mantA),r32expA2t4),1.0_rprecs)-3.0_rprecs
        Counter=Counter+1
    end subroutine LFGRCReal32x2S

    subroutine LCGRReal32x2S(rndreal)
        real(kind=rprecs),intent(out)::rndreal(2)
        if(Counter>N) call LCGRandu()
        rndreal(1)=transfer(ior(iand(rngstate(Counter),r32mantA),r32expA),1.0_rprecs)-1.0_rprecs
        rndreal(1)=rndreal(1)*Pi2_s
        Counter=Counter+1
        if(Counter>N) call LCGRandu()
        rndreal(2)=transfer(ior(iand(rngstate(Counter),r32mantA),r32expA2t4),1.0_rprecs)-3.0_rprecs
        Counter=Counter+1
    end subroutine LCGRReal32x2S


!------------Real-64-Bit---------------------------------------------------------------------------

    subroutine LFGRCReal64(rndreal)
        real(kind=rprecd),intent(out)::rndreal
        integer(kind=iprecq)::state
        if(Counter-1>N) call LFGRcarry()
        state=ieor(rngstate(Counter),shiftl(rngstate(Counter-1),23))
        rndreal=transfer(ior(state,r64exp),1.0_rprecd)-1.0_rprecd
        Counter=Counter+2
    end subroutine LFGRCReal64

    subroutine LCGRReal64(rndreal)
        real(kind=rprecd),intent(out)::rndreal
        integer(kind=iprecq)::state
        if(Counter-1>N) call LCGRandu()
        state=ieor(rngstate(Counter),shiftl(rngstate(Counter-1),23))
        rndreal=transfer(ior(state,r64exp),1.0_rprecd)-1.0_rprecd
        Counter=Counter+2
    end subroutine LCGRReal64

!------------Integer-32-Bit---------------------------------------------------------------------------

    subroutine LCGRInt32UpBound(rndint,upbound)
        integer(kind=iprecd),intent(out)::rndint
        integer(kind=iprecd),intent(in)::upbound

        integer(kind=iprecd),parameter::bits=bit_size(bits)
        integer(kind=iprecd):: bmask,neededbits,usedbits
        if(Counter>N) call LCGRandu()

        !get next larger base 2
        neededbits=bits-leadz(upbound)
        bmask=ibset(0,neededbits)-1!get next larger power of 2 ... mask is all lower bits set to 1
        rndint=iand(bmask,int(rngstate(Counter),iprecd))

        rej_loop: do while(rndint>=upbound)!rndint can be 0 so I have to add later 1 to be in the desired range
            do usedbits=neededbits,31,neededbits
                rndint=iand(bmask,int(shiftr(rngstate(Counter),usedbits)))
                if (rndint<upbound) exit rej_loop
            end do
            Counter = Counter + 1
            if(Counter>N) call LCGRandu()
            rndint=iand(bmask,int(rngstate(Counter),iprecd))
        end do rej_loop
        rndint=rndint+1
        Counter = Counter + 1
    end subroutine LCGRInt32UpBound

    subroutine LFGRCInt32UpBound(rndint,upbound)
        integer(kind=iprecd),intent(out)::rndint
        integer(kind=iprecd),intent(in)::upbound

        integer(kind=iprecd),parameter::bits=bit_size(bits)
        integer(kind=iprecd):: bmask,neededbits,usedbits,state

        if(Counter-1>N) call LFGRcarry()
        state=int(ieor(rngstate(Counter),shiftl(rngstate(Counter-1),20)),iprecd)

        !get next larger base 2
        neededbits=bits-leadz(upbound)
        bmask=ibset(0,neededbits)-1!get next larger power of 2 ... mask is all lower bits set to 1
        rndint=iand(bmask,state)

        rej_loop: do while(rndint>=upbound)!rndint can be 0 so I have to add later 1 to be in the desired range
            do usedbits=neededbits,31,neededbits
                rndint=iand(bmask,int(shiftr(state,usedbits)))
                if (rndint<upbound) exit rej_loop
            end do
            Counter = Counter + 2
            if(Counter-1>N) call LFGRcarry()
            state=int(ieor(rngstate(Counter),shiftl(rngstate(Counter-1),20)),iprecd)
            rndint=iand(bmask,state)
        end do rej_loop
        rndint=rndint+1
        Counter = Counter + 2
    end subroutine LFGRCInt32UpBound


end module TrivialPRNG
