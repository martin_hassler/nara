/*
!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------
! Dummy file for ISAAC64 [Original By Bob Jenkins, 1996.  Public Domain. (https://burtleburtle.net/bob/rand/isaacafa.html)]
! till I have a license
!--------------------------------------------------------------------------------------------------------------------
!  Standard definitions and types, Martin Hassler
!--------------------------------------------------------------------------------------------------------------------
*/
#ifndef STANDARD
# define STANDARD

#  include <stdint.h>
typedef  uint_fast64_t ub8;
typedef  uint_fast64_t ub8;
typedef  int_fast64_t  sb8;
typedef  uint_fast32_t ub4;
typedef  int_fast32_t  sb4;
typedef  uint_fast16_t ub2;
typedef  int_fast16_t  sb2;
typedef  uint_fast8_t  ub1;
typedef  int_fast8_t   sb1;
typedef  int_fast32_t  word;
#endif

