/*
!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------
! Dummy file for ISAAC64 [Original By Bob Jenkins, 1996.  Public Domain. (https://burtleburtle.net/bob/rand/isaacafa.html)]
! till I have a license
!--------------------------------------------------------------------------------------------------------------------
*/
#ifndef STANDARD
#include "standard.h"
#endif
#ifndef ISAAC64
#include "isaac64Dummy.h"
#endif
extern    ub8 randrsl[256];

void isaac64()
{

}

#
void randinit(flag)
sb4 flag; /* if (flag==TRUE), then use the contents of randrsl[i:i+7] to initialize mm[]. */
{
   printf("Due to licensing issues (one cannot release stuff to the public domain in Austria) the original sourcecode for ISAAC64\n");
   printf("is not included with NARA, if you want to use ISAAC64 download isaac64.c and isaac64.h\n");
   printf("from https://burtleburtle.net/bob/rand/isaacafa.html\n");
   printf("and recompile the project.");
   exit(0);
}


