!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Module containing Input and output operations. Including feeding Data into the arrays
!!!!
!!!! using Unit numbers starting with 11, Added parameters to manual 4.10.2018
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module NaraIO
    use MHKonstanten
    use NaraTypes
    use NaraError
    implicit none

    private!TODO replace asort with ASort

    public::ReadFiles,SaveStateMin,RestoreStateMin,SaveObservables,InitSaveObservable!TODO decide what has to be private.
    public:: FinFiles,InitSaveStats,SaveStats,SaveMeasurements, InitSaveMeasurements

    integer(kind=KDefI), parameter:: MaxLen=20!maximum line length if needed
    integer(kind=KDefI), parameter:: MaxLenLong=100!maximum line length if needed
    integer(kind=KDefI), parameter:: FnML=255!Maximum length of file name including path
    integer(kind=KDefI), parameter:: FnEL=5!Number of characters for file extension
    real(kind=KCalc),parameter:: DefMagMom=1.0_KCalc!default magnetic moment
    real(kind=KCalc),parameter:: DefLandeG=1.0_KCalc!default Land� g Factor
    character(len=1),parameter:: CommentChar=achar(35)! "#" is comment character
    character(len=1),parameter:: KeyWChar=achar(37)! "%" as Keyword character
    character(len=*),parameter:: StartFKW="%START%"!Start File KW
    real(kind=KSDir),parameter:: MaxConeAngle=1.1344640137963142_KSDir! Maximum Half opening angle is 65� theoretical <90� would be Ok but then there can be a problem if the new state is in (0,0,0)...


    contains
!TODO replace string comparison with indexing

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!!    Auxiliary Subroutines for IO Operations         Added to manual 4.10.2018
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !Subroutine InitOpenW() is part of NaraError, since it also needed there
        !Subroutine InitOpenR() is part of NaraError, since InitOpenW is also there




        !Get File name extension... at maximum the first FnEL letters of extension are used to identify filetype.
        ! ex.: FnEL=5: => ".POSCAR" ... only ".POSCA" is relevant.
        subroutine GetFnExt(fN,fNE)
            character(len=*),intent(in)::fN!file Name
            character(len=FnEL),intent(out)::fNE!fileName extension
            integer(kind=KDefI)::ind,lfNE
            fNE=""
            ind=0
            ind = index(fN,".",.TRUE.)!search for last "."
            lfNE=len_trim(adjustl(fN))

            if(ind==lfNE) then!filename ends with an "."
                call HCFIO(18,1,fN,"")
            elseif(ind==0) then!no file extension
                 call HCFIO(18,-2,fN,"")
            end if
            lfNE=lfNE-ind!length of file extension
            if(lfNE>5) lfNE=5!extension has more than 5 letters
            fNE(:lfNE)=fN(ind+1:ind+lfNE)! if lfNE <5 the remaining part should fill up with " "

        end subroutine GetFnExt


        !searches the next Keyword and returns the data in the same line
        !till the comment Character.
        subroutine GetNextKW(uNr,keyWord,sdata,state)
            integer(kind=KDefI),intent(in):: uNr!Unit number of file we want to read from
            character(len=MaxLen),intent(out):: keyWord!returns the Keyword found
            character(len=MaxLenLong),intent(out):: sdata!data in same line till comment character
            logical(kind=KDefL),intent(out):: state!if .T. end of  file reached

            integer(kind=KDefI)::comPos,kWPosS,kWPosE!comment position and keyword position start and end
            integer(kind=KDefI):: ioEStat,i
            character(len=IOEMsgLen):: ioEMsg,ioEMsg2
            character(len=MaxLenLong)::info
            logical(kind=KDefL)::fexists,fopened


            state = .FALSE.
            ioEStat=0
            keyWord=""
            sdata=""
            do
                read(unit=uNr,fmt='(A)',iostat=ioEStat,iomsg=ioEMsg) info
                if(ioEStat>0) then!error handling
                    inquire(unit=uNr,iostat=i,iomsg=ioEMsg2,name=info,exist=fexists,opened=fopened)
                    if(i/=0) call HCFIO(4,i,info,ioEMsg2)
                    if(.NOT.fopened) call HCFIO(2,i,info,"")
                    if(.NOT.fexists) call HCFIO(3,i,info,"")
                    call HCFIO(1,ioEStat,info,ioEMsg)!no new error
                elseif(ioEStat<0) then!end of file
                    state=.TRUE.
                    info=""
                    keyWord="%END%"
                    return
                end if

                !test if line contains comment
                compos=index(info,CommentChar)!get first comment character
                kWPosS=index(info,KeyWChar)!get first key word indicating character
                kWPosE=index(info(kWPosS+1:),KeyWChar)!get second key word indicating character
                kWPosE=kWPosE+kWPosS
                if(kWPosE==0) cycle !no valid line
                if((comPos/=0).AND.(comPos<kWPosE)) cycle !comment line
                if(comPos==0) comPos=MaxLenLong+1

                !retrieve Keyword
                keyWord=adjustl(info(kWPosS:kWPosE))
                sdata=adjustl(info(kWPosE+1:comPos-1))!if kWPosE+1>comPos-1 should return "" according to 2008 standard

                !convert Keyword to upper case...
                do i=2,kWPosE!exclude comment characters
                    if(("a"<=keyWord(i:i)).AND.(keyWord(i:i)<="z")) then!compare according to collating sequence
                        keyWord(i:i)=achar(iachar(keyWord(i:i))-32)!convert to upper case... ASCII convention
                    end if
                end do
                exit!found keyword everything is fine
            end do
        end subroutine GetNextKW


        !searches the passed Keyword and returns the data in the same line
        !till the comment Character.
        !Positions file in the next line, next read is in the next line.
        subroutine FindKW(uNr,keyWord,sdata,state)
            integer(kind=KDefI),intent(in):: uNr!Unit number of file we want to read from
            character(len=*),intent(in):: keyWord!returns the Keyword found
            character(len=MaxLenLong),intent(out):: sdata!data in same line till comment character
            logical(kind=KDefL),intent(out):: state!if .T. couldn't find KW

            character(len=MaxLen)::keyWordFile

            state = .FALSE.
            sdata=""
            do
                call GetNextKW(uNr,keyWordFile,sdata,state)
                if(adjustl(keyWord)==keyWordFile)then!found Keyword
                    state=.FALSE.
                    exit
                end if

                if(state) exit!couldn't find Keyword in file.
            end do
        end subroutine FindKW


        !Positions the file after Start KW
        subroutine PositionStrtF(uNr,FiNa)
            integer(kind=KDefI),intent(in):: uNr!Unit number of file we want to read from
            character(len=*),intent(in)::FiNa!FileName... only used for Error Code

            logical(kind=KDefL):: state!if .T. couldn't find KW
            character(len=MaxLenLong) :: info

            state=.FALSE.
            call FindKW(uNr,StartFKW,info,state)
            if(state) then!couldn't find KW
                call HCFIO(999,0,"Couldn't find Start KW in File: "//trim(FiNa),"")
            end if
            call WriteLogAndStdO("Found StartKW in: "//trim(FiNa))
        end subroutine PositionStrtF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Subroutines to read from Nara files!TODO handle reading errors
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        subroutine ReadMainF(fNA,fNEA)!Read Filenames from FnRFn (nara.nara)
            use NaraData,only: FnRFn
            character(len=FnML),dimension(:),allocatable,intent(out):: fNA!Filename Array
            character(len=FnEL),dimension(:),allocatable,intent(out):: fNEA!Filename extension array
            character(len=FnML),dimension(:),allocatable:: fNACopy!Filename Array
            character(len=FnEL),dimension(:),allocatable:: fNEACopy!Filename extension array
            integer(kind=KDefI):: uNRFN,ioEStat,j,ind,fLines
            character(len=FnEL)::fNE
            character(len=IOEMsgLen):: ioEMsg
            character(len=FnML):: rFN,rTemp

            ioEStat=0
            uNRFN=11
            call WriteLogAndStdO("Trying to read from main Input file.")

            call InitOpenR(FnRFn)!test if present and init open

            open(newunit=uNRFN,file=FnRFn,status="old",action="read",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(101,ioEStat,FnRFn,ioEMsg)!error while opening the file


            ioEStat=0
            fLines=0

            do while (ioEStat==0)!get file length without blank lines
                read(uNRFN,*,iostat=ioEStat) rTemp
                if(ioEStat/=0) exit!end of file ... !TODO handle read errors
                if(trim(rTemp)=="")then!empty line or comment line
                    cycle
                end if
                fLines=fLines+1
            end do


            if(fLines<=0) call HCF(-100,1,"")!TODO Handle empty file

            rewind(uNRFN)!jump to beginning of file
            call PositionStrtF(uNRFN,FnRFn)!Position after StartKW

            allocate(fNA(fLines),fNEA(fLines))
            j=0
            do
                read(unit=uNRFN,fmt=*,iostat=ioEStat) rTemp
                if(ioEStat/=0) exit!end of file ... !TODO handle read errors
                !check for comment
                ind=index(rTemp,"#")
                if(ind/=0)then !line contains Comment
                    rFN=adjustl(rTemp(:ind-1))
                else
                    rFN=adjustl(rTemp)
                end if
                if(trim(rFN)=="")cycle!empty line or comment line
                ind=index(rTemp,"%END%")!reached end of file
                if(ind/=0) exit

                call GetFnExt(rFN,fNE)
                j=j+1
                fNA(j)=rFN
                fNEA(j)=fNE
            end do
            close(uNRFN)

            if(j<fLines) then !remove unnecessary empty lines
                allocate(fNACopy(j),fNEACopy(j))
                fNACopy=fNA(:j)
                fNEACopy=fNEA(:j)
                deallocate(fNA,fNEA)
                allocate(fNA(j),fNEA(j))
                fNA=fNACopy
                fNEA=fNEACopy
                deallocate(fNACopy,fNEACopy)
            end if

            call WriteLogAndStdO("Finished Reading: "//FnRFn)
            call WriteLog("Found the following files:")
            call WriteLogChAr(FnML,fNA)

        end subroutine ReadMainF




        subroutine ReadConfig(cFN)!Read Configuration File Cnara !TODO add sanity check
            use NaraData,only:CData,ConeRad
            character(len=*),intent(in):: cFN!Config file Name
            real(kind=KSDir):: fry !I don't know what name would describe its function... nothing came to my mind...
            real(kind=KCalc):: leela !same here
            integer(kind=KDefI)::ioEStat,uNRCF,i,j,k
            character(len=EMsgLen) :: ioEMsg
            character(len=MaxLenLong) :: info
            character(len=MaxLen):: keyword
            logical(kind=KDefL):: eof


            call WriteLogAndStdO("Trying to read from Configuration file "//trim(cFN))
            ioEStat=0
            uNRCF=12
            call InitOpenR(cFN)!test and init open

            open(newunit=uNRCF,file=cFN,status="old",action="read",iostat=ioEStat,iomsg=ioEMsg)

            if(ioEStat/=0) call HCFIO(102,ioEStat,cFN,ioEMsg)!error while opening the file

            call PositionStrtF(uNRCF,cFN)!Position after StartKW

            do
                call GetNextKW(uNRCF,keyword,info,eof)
                if(eof) exit !if end of file reached exit
                select case(keyword)
                    case("%AUTTHERM%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%autTherm
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%AUTTHERM%",ioEMsg)
                        if((CData%autTherm<0).OR.CData%autTherm>3) CData%autTherm=0!set dot valid value
                        if(CData%autTherm/=0) CData%advTherm=.TRUE.
                    case("%BOUNDCON%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%BCs
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%BOUNDCON%",ioEMsg)
                    case("%CONEANGLE%")
                        !no read here since Info holds already the necessary data
                        i=index(info,transfer(char([int(Z'C2'),int(Z'B0')]),repeat('A',2)))!Possible error with UTF-8...remove degree character
                        if(i/=0) then
                            info=info(:i-1)//info(i+1:)
                        else
                            i=index(info, "�")!rad or deg?
                        end if
                        if(i/=0)then!deg
                            j=index(info, "'")
                            k=index(info, "''")
                            read(info(:i-1),*,iostat=ioEStat,iomsg=ioEMsg) CData%ConeAngle
                            if(ioEStat/=0) call HCFIO(42,ioEStat,"%CONEANGLE%",ioEMsg)
                            if(k==j) j=0!no minutes
                            if(j/=0) then!found minutes
                                read(info(i+1:j-1),*,iostat=ioEStat,iomsg=ioEMsg) fry
                                if(ioEStat/=0) call HCFIO(42,ioEStat,"%CONEANGLE%",ioEMsg)
                                CData%ConeAngle=CData%ConeAngle+fry/60.0_KSDir!convert to �
                            end if

                            if(k/=0) then !found seconds
                                if(j==0) j=i
                                read(info(j+1:k-1),*,iostat=ioEStat,iomsg=ioEMsg) fry
                                if(ioEStat/=0) call HCFIO(42,ioEStat,"%CONEANGLE%",ioEMsg)
                                CData%ConeAngle=CData%ConeAngle+fry/3600.0_KSDir!convert to �
                            end if
                            CData%ConeAngle=CData%ConeAngle*(Pi/180.0_KSDir)

                        else!rad
                            read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%ConeAngle
                            if(ioEStat/=0) call HCFIO(42,ioEStat,"%CONEANGLE%",ioEMsg)
                        end if

                        if(CData%ConeAngle>MaxConeAngle)then!CA is larger than 40�...reduce to 40�
                            CData%ConeAngle=MaxConeAngle
                            call UMsg(-1,0,"%CONEANGLE% exceeds 40�, used maximum value of 40�")
                        end if
                    case("%END%")
                        exit
                    case("%ENERBINDER%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%enerBinder
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%ENERBINDER%",ioEMsg)
                    case("%INITSPINS%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%InitSpins
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%INITSPINS%",ioEMsg)
                    case("%NAVER%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%NAver
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%NAVER%",ioEMsg)
                        if(CData%NAver<0) CData%NAver=-CData%NAver!ensure that it is positive
!                        if(mod(CData%NAver,2)/=0) CData%NAver=CData%NAver+1!TODO Check if still needed... I highly doubt it.
                    case("%NMAV%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%NmAv
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%NMAV%",ioEMsg)
                        if(CData%NmAv<0) CData%NmAv=-CData%NmAv!ensure that it is positive
                    case("%NMAVDEL%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%NmAvDel
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%NMAVDEL%",ioEMsg)
                        if(CData%NmAvDel<0) CData%NmAvDel=-CData%NmAvDel!ensure that it is positive
                    case("%NMAXTHERM%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%NMaxTherm
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%NMAXTHERM%",ioEMsg)
                        if(CData%NMaxTherm<0) CData%NMaxTherm=-CData%NMaxTherm!ensure that it is positive
                    case("%NMCSWEEP%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%NMCSweep
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%NMCSWEEP%",ioEMsg)
                        if(CData%NMCSweep<0) CData%NMCSweep=-CData%NMCSweep!ensure that it is positive
                    case("%NMINTHERM%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%NMinTherm
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%NMINTHERM%",ioEMsg)
                        if(CData%NMinTherm<0) CData%NMinTherm=-CData%NMinTherm!ensure that it is positive
                    case("%NPRETHERM%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%NPreTherm
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%NPRETHERM%",ioEMsg)
                        if(CData%NPreTherm<0) CData%NPreTherm=-CData%NPreTherm!ensure that it is positive
                    case("%PRNG%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%PRNG
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%PRNG%",ioEMsg)
                    case("%RELCHOPA%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%RelChOPa
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%RELCHOPA%",ioEMsg)
                    case("%SAVERAWDATA%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%saveRaw
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%SAVERAWDATA%",ioEMsg)
                    case("%SIMAGUNIT%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%siMagUnit
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%SIMAGUNIT%",ioEMsg)
                    case("%SMAGBINDER%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%stagMBinder
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%SMAGBINDER%",ioEMsg)
                    case("%STRADBINDER%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%stradMBinder
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%STRADBINDER%",ioEMsg)
                        if(CData%stradMBinder) CData%stagMBinder = .TRUE.!enable binder calculation
                    case("%SUBLATGEN%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%subLatgen
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%SUBLATGEN%",ioEMsg)
                    case("%TMAGBINDER%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%totMBinder
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%TMAGBINDER%",ioEMsg)
                    case("%TTRADBINDER%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%ttradMBinder
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%TTRADBINDER%",ioEMsg)
                        if(CData%ttradMBinder) CData%totMBinder = .TRUE.!enable binder calculation
                    case("%UNITCONV%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg) CData%unitConv
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%UNITCONV%",ioEMsg)
                    case("%WEIGHTS%")
                        read(info,*,iostat=ioEStat,iomsg=ioEMsg)CData%WMFlip,CData%WMDevi,CData%WMRand
                        if(ioEStat/=0) call HCFIO(42,ioEStat,"%WEIGHTS%",ioEMsg)

                        leela=0.0_KCalc!normalize weights !NOTE Maybe it is necessery that weight=1 if only one weight is nonzero?
                        if(CData%WMFlip>0.0_KCalc) leela = leela + CData%WMFlip
                        if(CData%WMDevi>0.0_KCalc) leela = leela + CData%WMDevi
                        if(CData%WMRand>0.0_KCalc) leela = leela + CData%WMRand
                        if(leela<=0.0_KCalc) call HCFIO(20,1,cFN,"")! weights for the  Steps add up to a value smaller than 0.0
                        if(CData%WMFlip>0.0_KCalc) then
                            CData%WMFlip=CData%WMFlip/leela
                        else
                            CData%WMFlip=0.0_KCalc
                        end if
                        if(CData%WMDevi>0.0_KCalc) then
                            CData%WMDevi=CData%WMDevi/leela
                        else
                            CData%WMDevi=0.0_KCalc
                        end if
                        if(CData%WMRand>0.0_KCalc) then
                            CData%WMRand=CData%WMRand/leela
                        else
                            CData%WMRand=0.0_KCalc
                        end if
                    case default
                        call HCFIO(19,0,cFN,keyword)!unknown keyword
                end select
                call WriteLogAndStdO(trim(adjustl(keyword))//", read data: "//trim(adjustl(info)))
            end do

            ConeRad=sin(CData%ConeAngle)!calc base radius of cone

            if (CData%advTherm)then
                if (CData%NMinTherm>(2*CData%NmAv+CData%NmAvDel))then
                    CData%NMinTherm=CData%NMinTherm-(2*CData%NmAv+CData%NmAvDel)
                else
                    CData%NMinTherm=0
                end if
                CData%NMaxTherm=CData%NMaxTherm-CData%NMinTherm! otherwise more thermalization steps as requested

            else
                CData%NMinTherm=CData%NMaxTherm!to make LOG more understandable if no advanced thermalization
                CData%NmAv=0!to make LOG more understandable if no advanced thermalization
                CData%NmAvDel=0!to make LOG more understandable if no advanced thermalization
            end if

            if(.NOT.CData%unitConv) CData%siMagUnit=.FALSE.!overwrite if necessary.

            call WriteLogAndStdO("Finished Reading Configurational file")

            close(uNRCF)
        end subroutine ReadConfig


        subroutine ReadInterac(iFN)!reads interaction parameters from Inara file
            use NaraData,only: ExchMat,SIAMat,ANum,ASort,IData

            character(len=*),intent(in):: iFN!Interaction file Name

            type(InterData)::iDataCopy
            type(SymMat),dimension(:),allocatable:: matCopy
            type(ExchVec),dimension(:),allocatable:: vecCopy
            integer(kind=KDefI),allocatable,dimension(:,:)::indList,indListCopy
            real(kind=KPosi),dimension(2)::radii
            real(kind=KPosi):: radcopy
            integer(kind=KDefI)::ioEStat,uNRIF,i,j,k,m,n
            character(len=EMsgLen) :: ioEMsg
            logical(kind=KDefL)::sym,eof
            character(len=MaxLenLong) :: info,info2
            character(len=MaxLen):: keyword

            call WriteLogAndStdO("Trying to read from Interaction file "//trim(iFN))

            if(.NOT.allocated(IData)) then
                allocate(IData(ANum))
            end if

            ioEStat=0
            call InitOpenR(iFN)!test and init open

            open(newunit=uNRIF,file=iFN,status="old",action="read",iostat=ioEStat,iomsg=ioEMsg)

            if(ioEStat/=0) call HCFIO(103,ioEStat,iFN,ioEMsg)!error while opening the file


            call PositionStrtF(uNRIF,iFN)!Position after StartKW


            rIntfile: do while (ioEStat==0)

                call GetNextKW(uNRIF,keyword,info,eof)
                write(*,*) "Found KW in Interaction File: ", keyword
                if(eof) exit rIntfile!end of file ...
                select case (keyword)
                    case("%END%")!TODO ensure initialisation of matrices/ full init of IDATA!!
                        exit rIntfile

                    case("%SIA%")
                        siainter: do while (ioEStat==0)
                            if(allocated(indList)) deallocate(indList)!cleanup if necessary
                            do while (ioEStat==0)
                                read(uNRIF,fmt='(A)',iostat=ioEStat,iomsg=ioEMsg) info
                                if(ioEStat/=0) call HCFIO(19,0,iFN,"Problem with SIA input.")
                                info=adjustl(info)!adjustl necessary to see if it is a comment line or a line with a comment...
                                k=index(info,"#")!get comment position
                                if(k==1) cycle  !if comment line is encountered cycle
!                                if (k/=0) info=info(:k-1)!remove comment if present

                                j=index(info,"%END%")
                                if(j/=0) exit rIntfile !end of interaction file

                                j=index(info,"%ESIA%")
                                if(j/=0) exit siainter !end of SIA section

                                if(len_trim(info)==0) cycle  !if blank line is encountered cycle

                                if(allocated(indList)) then !already one pass
                                    if(index(info,"%M%")/=0) exit!matrix section follows
                                end if

                                j=-1
                                do i=1,ANum!search for matching atom type
                                    if(index(info,ASort(i))/=0) then!found matching atom?
                                        j=i
                                        exit
                                    end if
                                end do
                                if(j==-1) call HCFIO(20,5,iFN,"")!no matching atom found

                                !add match to index List
                                if(allocated(indList))then
                                        m=size(indList,1)
                                        allocate(indListCopy(m+1,1))
                                        indListCopy(:m,1)=indList(:,1)
                                        indListCopy(m+1,1)=j
                                        deallocate(indList)
                                        call move_alloc(indListCopy,indList)
                                        !move_alloc deallocates indListCopy
                                else
                                    allocate(indList(1,1))
                                    indList(1,1)=j
                                end if
                            end do

                        if(ioEStat/=0) call HCFIO(1,ioEStat,"SIA 1",ioEMsg)

                        if(allocated(SIAMat)) then
                            m=size(SIAMat)
                            allocate(matCopy(0:m))
                            matCopy(:m-1)=SIAMat!0:m-1 = m elements
                            deallocate(SIAMat)
                            call move_alloc(matCopy,SIAMat)
                        else
                            allocate(SIAMat(0:1))
                            SIAMat(0)%intMat=0.0_KInMa!wee need one matrix with 0 for atoms with no SIA
                            m=1
                        end if

                        !read SIA Mat
                        read(uNRIF,fmt=*,iostat=ioEStat,iomsg=ioEMsg) SIAMat(m)%intMat

                        !add data to IData
                        do i=1,size(indList,1)
                            IData(indList(i,1))%pInde=m
                        end do
                    end do siainter

                    case ("%2EXCH%")
                        twointer: do while (ioEStat==0)
                            if(allocated(indList)) deallocate(indList)!cleanup if necessary
                            do while (ioEStat==0)
                                read(uNRIF,fmt='(A)',iostat=ioEStat,iomsg=ioEMsg) info
                                if(ioEStat/=0) call HCFIO(19,0,iFN,"Problem with Exchange input.")

                                info=adjustl(info)!adjustl necessary to see if it is a comment line or a line with a comment...
                                k=index(info,"#")!get comment position
                                if(k==1) cycle  !if comment line is encountered cycle
!                                if (k/=0) info=info(:k-1)!remove comment if present

                                j=index(info,"%END%")
                                if(j/=0) exit rIntfile !end of interaction file

                                j=index(info,"%E2EXCH%")
                                if(j/=0) exit twointer !end of two particle exchange interaction section

                                if(len_trim(info)==0) cycle  !if blank line is encountered cycle

                                if(allocated(indList)) then !already one pass
                                    if(index(info,"%R%")/=0) exit
                                end if

                                j=index(info,"%%")!get spacer
                                k=index(info,"%S%")!symmetric interaction

                                sym = (k/=0)!symmetric interaction
                                if(sym) then
                                    k=k-1!end of string
                                else
                                    k=len_trim(info)!In case of no %S% get string length
                                end if

                                !get interaction partners
                                info2=info(:j-1)! centre atom
                                info=info(j+2:k)!interaction partner

                                j=-1
                                do i=1,ANum!search for matching atom type centre atom
                                    if(index(info2,ASort(i))/=0) then!found matching atom?
                                        j=i
                                        exit
                                    end if
                                end do
                                if(j==-1) call HCFIO(20,6,iFN,"")!no matching atom found

                                k=-1
                                do i=1,ANum!search for matching atom type
                                    if(index(info,ASort(i))/=0) then!found matching atom?
                                        k=i
                                        exit
                                    end if
                                end do
                                if(k==-1) call HCFIO(20,7,iFN,"")!no matching atom found

                                !increase IData(j) by 1
                                if(allocated(IData(j)%twopart)) then
                                    m=size(IData(j)%twopart)
                                    allocate(iDataCopy%twopart(m+1))
                                    iDataCopy%twopart(:m)=IData(j)%twopart
                                    deallocate(IData(j)%twopart)
                                    call move_alloc(iDataCopy%twopart,IData(j)%twopart)
                                    m=m+1
                                else
                                    allocate(IData(j)%twopart(1))
                                    m=1
                                end if
                                IData(j)%twopart(m)%iaSort=k

                                !add match and interaction index to index List Part 1
                                !indList: 1st index: 1 centre atom index, 2: interaction index, 2nd index: interaction number
                                if(allocated(indList))then!update Indlist
                                    n=size(indList,2)
                                    if(sym) then
                                        allocate(indListCopy(2,n+2))
                                    else
                                        allocate(indListCopy(2,n+1))
                                    end if
                                    indListCopy(:,:n)=indList
                                    deallocate(indList)
                                    indListCopy(1,n+1)=j
                                    indListCopy(2,n+1)=m
                                    call move_alloc(indListCopy,indList)
                                else
                                    if(sym) then
                                        allocate(indList(2,2))
                                        n=0!needed for Update IndList Part 2
                                    else
                                        allocate(indList(2,1))
                                    end if
                                    indList(1,1)=j
                                    indList(2,1)=m
                                end if

                                if(sym) then !symmetric interaction
                                   if(allocated(IData(k)%twopart)) then
                                        m=size(IData(k)%twopart)
                                        allocate(iDataCopy%twopart(m+1))
                                        iDataCopy%twopart(:m)=IData(k)%twopart
                                        deallocate(IData(k)%twopart)
                                        call move_alloc(iDataCopy%twopart,IData(k)%twopart)
                                        m=m+1
                                    else
                                        allocate(IData(k)%twopart(1))
                                        m=1
                                    end if
                                    IData(k)%twopart(m)%iaSort=j

                                    !Update Indlist Part 2
                                    indList(1,n+2)=k
                                    indList(2,n+2)=m
                                end if
                            end do

                            if(ioEStat/=0) call HCFIO(1,ioEStat,"Exch",ioEMsg)

                            !Read interaction radii
                            read(uNRIF,fmt=*,iostat=ioEStat,iomsg=ioEMsg) radii!TODO allow multiple Radii
                            if((radii(1)<0.0_KPosi).OR.(radii(2)<0.0_KPosi)) call HCFIO(20,2,iFN,"") !negative interaction radius
                            if(radii(1)>radii(2)) then!make sure that input is valid
                                radcopy=radii(1)
                                radii(1)=radii(2)
                                radii(2)=radcopy
                            end if

                            !read interaction Matrix
                            read(uNRIF,fmt='(A)',iostat=ioEStat,iomsg=ioEMsg) info
                            j=index(info,"%M%")
                            if(j==0) call HCFIO(20,8,iFN,"")

                            if(allocated(ExchMat)) then
                                m=size(ExchMat)
                                allocate(vecCopy(m+1))
                                vecCopy(:m)=ExchMat
                                deallocate(ExchMat)
                                call move_alloc(vecCopy,ExchMat)
                                m=m+1
                            else
                                allocate(ExchMat(1))
                                m=1
                            end if

                            read(uNRIF,fmt=*,iostat=ioEStat,iomsg=ioEMsg) ExchMat(m)%exTen

                            do i=1, size(indList,2)
                                IData(indList(1,i))%twopart(indList(2,i))%eInd=m
                                IData(indList(1,i))%twopart(indList(2,i))%rad=radii
                            end do

                        end do twointer

                end select
            end do rIntfile

            close(uNRIF)

            !Make Sure that there is an initialized SIA Mat:
            if(.NOT.allocated(SIAMat)) then
                allocate(SIAMat(0:0))! 0:0 necessary otherwise it would be a zero sized array
                SIAMat(0)%intMat=0.0_KInMa!wee need one matrix with 0 for atoms with no SIA
            end if

            call WriteLogAndStdO("Finished Reading Interaction File")

        end subroutine ReadInterac


!TODO Rewrite with propper select case structure.
        subroutine ReadParam(pFN,pData)!read system parameters Temperature and Magnetic field and the corresponding steps from Pnara file
            character(len=*),intent(in):: pFN!Parameter file Name
            type(ParaData),allocatable,dimension(:),intent(out)::pData!Parameter Data


            integer(kind=KDefI)::ioEStat,uNRPF,i, philip, fry, cubert, hubert
            character(len=EMsgLen) :: ioEMsg
            character(len=100) :: info
            logical(kind=KDefL):: saved
            type(ParaData),allocatable,dimension(:)::pDataCopy!Parameter Data Copy


            call WriteLogAndStdO("Trying to read from Parameter file "//trim(pFN))

            ioEStat=0
            uNRPF=12
            call InitOpenR(pFN)!test and init open

            open(newunit=uNRPF,file=pFN,status="old",action="read",iostat=ioEStat,iomsg=ioEMsg)

            if(ioEStat/=0) call HCFIO(104,ioEStat,pFN,ioEMsg)!error while opening the file

            call PositionStrtF(uNRPF,pFN)!Position after StartKW

            !How Long is the file?
            fry=0
            do while (ioEStat==0)
                read(uNRPF,'(A)',iostat=ioEStat) info !read line and decode
                hubert=index(info,"#")
                if(hubert==1) cycle !found comment line
                if(len_trim(info)==0) cycle !skip empty line
                cubert=index(info,"%END%")
                if((cubert/=0).AND.((hubert==0).OR.(cubert<hubert))) exit!check if uncommented %END% occurs

                !skip keywords
                cubert=index(info,"%SAVE%")
                if((cubert/=0).AND.((hubert==0).OR.(cubert<hubert))) cycle
                cubert=index(info,"%RSTART%")
                if((cubert/=0).AND.((hubert==0).OR.(cubert<hubert))) cycle
                cubert=index(info,"%NEW%")
                if((cubert/=0).AND.((hubert==0).OR.(cubert<hubert))) cycle

                !line is probably data line
                fry=fry+1
            end do
            rewind(uNRPF)!jump to start of file

            if(fry<=1) call HCF(108,0,"Pnara file is empty")
            fry=fry
            allocate(pData(fry))

            call PositionStrtF(uNRPF,pFN)!Position after StartKW

            i=1
            philip=i
            saved=.FALSE.

            read(uNRPF,*) pData(i)%temppoint, pData(i)%hpoint !read initial Temperature and H field
            if(pData(i)%temppoint<0.0_KPara) call HCFIO(20,3,pFN,"")!negative temperature
            pData(i)%ndata=0
            pData(i)%newsimrun=.TRUE.

            do while (ioEStat==0)
                read(uNRPF,'(A)',iostat=ioEStat) info !read line and decode
                hubert=index(info,"#")
                if(hubert==1) cycle !found comment line

                if(hubert/=0) info=adjustl(info(:hubert-1))!cut off comments
                if(len_trim(info)==0) cycle !skip empty line

                cubert=index(info,"%END%")
                if(cubert/=0) then!check if uncommented %END% occurs
                    if(i>=philip)then !initial state...ignore
                        !TODO Handle Error
                    end if
                    exit
                end if

                cubert=index(info,"%SAVE%")
                if(cubert/=0) then!check if uncommented %SAVE% occurs
                    if(i==philip)then !initial state...ignore
                        call UMsg(1,0,"You've tried to save the initial state.")
                    else
                        pData(i)%savestate=.TRUE.
                        saved=.TRUE.

                    end if
                    cycle
                end if

                i=i+1

                cubert=index(info,"%RSTART%")!FIXME Rstart KW
                if(cubert/=0) then!check if uncommented %START% occurs
!                    if(.NOT.saved) then! Make sure DAU didn't do something stupid...ignore him/her!TODO is this piece still needed?
!                        call UMsg(1,"You've tried to start the simulation from  an unsaved state.")
!                    elseif(i==philip)then !initial state
!                        call UMsg(1,"You've tried to start the simulation from the initial state.")
!                    else
!                pData(i)%runlstate=.TRUE.
                    call WriteLogAndStdO("%RSTART% is not supported at the moment, KW is ignored.")
!                    end if
                    cycle
                end if

                cubert=index(info,"%NEW%")!Count number of new runs
                if(cubert/=0) then!check if uncommented %SAVE% occurs
                    philip=i
                    saved=.FALSE.
                    read(uNRPF,*) pData(i)%temppoint, pData(i)%hpoint !read initial Temperature and H field
                    if(pData(i)%temppoint<=0.0_KPara) call HCFIO(20,4,pFN,"")!negative or zero temperature

                    pData(i)%ndata=0
                    pData(i)%newsimrun=.TRUE.
                    cycle
                end if

                read(info,fmt=*,iostat=cubert,iomsg=ioEMsg) pData(i)%temppoint,&
                & pData(i)%hpoint, pData(i)%ndata
                if(cubert/=0) call HCF(100,cubert,"ReadParamData; iomsg: "//trim(adjustl(ioEMsg)))
                if(pData(i)%temppoint<=0.0_KPara) call HCFIO(20,4,pFN,"")!negative or zero temperature
                if(pData(i)%ndata<=0) call HCF(107,1,"Invalid Number of steps")!negative or zero temperature
            end do

            if(i>fry) call HCF(-1,0,"Error while allocating data from Pnara")
            if(i<fry)then! reallocate data
                allocate(pDataCopy(i))
                pDataCopy=pData(:i)
                deallocate(pData)
                allocate(pData(i))
                pData=pDataCopy
                deallocate(pDataCopy)
            end if
            call WriteLogAndStdO("Finished Reading parameter file")
        end subroutine ReadParam

        !reads auxiliary structure file Snara and adds the data to the structure!TODO handle read errors
        subroutine ReadAuxStru(sFN)
            use NaraData,only:Anum,SData,MagMom,LandeG,PNum
            character(len=*),intent(in):: sFN!auxiliary structure file Name

            integer(kind=KDefI)::ioEStat,uNRSF,i,allocstat
            character(len=MaxLenLong):: rTemp
            character(len=MaxLen):: keyword
            character(len=EMsgLen):: ioEMsg
            logical(kind=KDefL):: eof!eof:state of GetNextKW


            !Allocate and initialize MagMom and LandeG Array
            allocate(MagMom(ANum),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating MagMom")
            MagMom=DefMagMom
            allocate(LandeG(ANum),stat=allocstat)
            if(allocstat/=0) call HCFR(102,allocstat,"Error while allocating LandeG")
            LandeG=DefLandeG


            call WriteLogAndStdO("Trying to read from auxiliary structure file "//trim(sFN))


            ioEStat=0
            call InitOpenR(sFN)!test and init open
            open(newunit=uNRSF,file=sFN,status="old",action="read",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(105,ioEStat,sFN,ioEMsg)!error while opening the file
            call PositionStrtF(uNRSF,sFN)!Position after StartKW

            read(uNRSF,*) SData%NCells!read number of unit cells for simulation

            !make sure total number of particles is not too large / no overflow happens
            if(huge(PNum)<(int(SData%nAtoms,iprecq)*int(SData%NCells(1),iprecq)*&
            &int(SData%NCells(2),iprecq)*int(SData%NCells(3),iprecq)))then

                write(ioEMsg,'(A,X,I0)') "Proposed #Particles:",(int(SData%nAtoms,iprecq)*int(SData%NCells(1),iprecq)*&
                &int(SData%NCells(2),iprecq)*int(SData%NCells(3),iprecq))
                call HCFIO(20,9,trim(adjustl(sFn)),ioEMsg)
            end if
            !calculate number of particles:
            PNum=0
            PNum=SData%nAtoms*SData%NCells(1)*SData%NCells(2)*SData%NCells(3)

            write(ioEMsg,'(I0)') PNum
            call WriteLog("Total number of Spins: "//trim(adjustl(ioEMsg)))

            ioEStat=0
            readloop: do while (ioEStat==0)
                call GetNextKW(uNRSF,keyword,rTemp,eof)
                if(eof) exit readloop!end of file ...
                select case (trim(keyword))
                    case ("%END%")
                        exit readloop
                    case ("%MAGMOM%")!nondefault magnetic moments

                        do i=1,SData%nAtoms!for each atom in UC read new Magnetic moment
                            read(uNRSF,*,iostat=ioEStat) rTemp
                            if(ioEStat/=0) then
                                if(i<SData%nAtoms) call UMsg(-1,2457,"")!if not all magnetic moments could be set...!FIXME errormessage
                                exit
                            endif

                            if(trim(adjustl(rTemp))=="%END%") then
                                if(i<SData%nAtoms) call UMsg(-1,2400,"")!if not all magnetic moments could be set...
                                exit readloop
                            end if
                            if(trim(adjustl(rTemp))=="%EMAGMOM%") then
                                if(i<SData%nAtoms) call UMsg(-1,2456,"")!if not all magnetic moments could be set...
                                exit
                            end if

                            read(rTemp,*,iostat=ioEStat) MagMom(SData%aData(i)%iaSort)
                            if(ioEStat/=0) then
                                if(i<SData%nAtoms) call UMsg(-1,2453,"")!if not all magnetic moments could be set...!TODO error handling
                                exit
                            endif
                        end do

                    case ("%LANDE%")!nondefault Land� g factor
                        do i=1,SData%nAtoms
                            read(uNRSF,*,iostat=ioEStat) rTemp
                            if(ioEStat/=0) then
                                if(i<SData%nAtoms) call UMsg(-1,2458,"")!if not all Land� g factor could be set...!TODO fix errormessage
                                exit
                            endif

                            if(trim(adjustl(rTemp))=="%END%") then
                                if(i<SData%nAtoms) call UMsg(-1,2401,"")!if not all Land� g factor  could be set...
                                exit readloop
                            end if
                            if(trim(adjustl(rTemp))=="%ELANDE%") then
                                if(i<SData%nAtoms) call UMsg(-1,2459,"")!if not all Land� g factor could be set...
                                exit
                            end if

                            read(rTemp,*,iostat=ioEStat) LandeG(SData%aData(i)%iaSort)
                            if(ioEStat/=0) then
                                if(i<SData%nAtoms) call UMsg(-1,2460,"")!if not all magnetic moments could be set...!TODO error handling
                                exit
                            endif
                        end do

                    case ("%SUBLAT%")!Set default Sub-lattice
                        do i=1,SData%nAtoms
                            read(uNRSF,*,iostat=ioEStat) rTemp
                            if(ioEStat/=0) then
                                if(i<SData%nAtoms) call UMsg(-1,2461,"")!if not all Sub-lattice could be set...!TODO fix errormessage
                                exit
                            endif
                            if(trim(adjustl(rTemp))=="%END%") then
                                if(i<SData%nAtoms) call UMsg(-1,2402,"")!if not all Sub-lattice could be set...
                                exit readloop
                            end if
                            if(trim(adjustl(rTemp))=="%ESUBLAT%") then
                                if(i<SData%nAtoms) call UMsg(-1,2462,"")!if not all Sub-lattice could be set...
                                exit
                            end if
                            read(rTemp,*,iostat=ioEStat) SData%aData(i)%subLat

                            if(ioEStat/=0) then
                                if(i<SData%nAtoms) call UMsg(-1,2463,"")!if not all Sub-lattice could be set...!TODO error handling
                                exit
                            endif
                        end do
                    case ("%SPINDIR%")!Set default Spin directions
                        do i=1,SData%nAtoms
                            read(uNRSF,'(A)',iostat=ioEStat,iomsg=ioEMsg) rTemp
                            if(ioEStat/=0) then
                                if(i<SData%nAtoms) call UMsg(-1,2501,"")!if not all Spin directions could be set...!TODO fix errormessage
                                exit
                            endif

                            if(trim(adjustl(rTemp))=="%END%") then
                                if(i<SData%nAtoms) call UMsg(-1,2403,"")!if not all Spin directions could be set...
                                exit readloop
                            end if
                            if(trim(adjustl(rTemp))=="%ESPINDIR%") then
                                    if(i<SData%nAtoms) call UMsg(-1,2502,"")!if not all Spin directions could be set...
                                    exit
                            end if
                            read(rTemp,*,iostat=ioEStat,iomsg=ioEMsg) SData%aData(i)%defSdir(:)

                            if(ioEStat/=0) then
                                if(i<SData%nAtoms) call UMsg(-1,2503,"")!if not all Spin directions could be set...!TODO error handling
                                exit
                            endif

                            !norm spindirection to 1
                            SData%aData(i)%defSdir=SData%aData(i)%defSdir/&
                            &sqrt(sum(SData%aData(i)%defSdir**2)) !FIXME use hypot()
                        end do

                    case ("%SLSPINDIR%")
                        read(uNRSF,'(A)',iostat=ioEStat) rTemp
                        if(ioEStat/=0) then
                            call UMsg(-1,2500,"")!could not read crystal direction...!TODO fix errormessage
                            exit
                        endif

                        if(trim(adjustl(rTemp))=="%END%") exit readloop

                        read(rTemp,*,iostat=ioEStat) SData%sublatSpindir
                        if(ioEStat/=0) then
                            call UMsg(-1,2500,"")!could not read crystal direction...!TODO fix errormessage
                            exit
                        endif

                    case ("%SLPLANES%")
                        read(uNRSF,'(A)',iostat=ioEStat) rTemp
                        if(ioEStat/=0) then
                            call UMsg(-1,2504,"f")!could not crystal plane...!TODO fix errormessage
                            exit
                        endif

                        if(trim(adjustl(rTemp))=="%END%") exit readloop

                        read(rTemp,*,iostat=ioEStat) SData%sublatPlane
                        if((SData%sublatPlane(1)==0).AND.&
                        &(SData%sublatPlane(2)==0).AND.&
                        &(SData%sublatPlane(3)==0)) SData%sublatPlane=(/1,1,1/) !make sure initialized correctly!TODO throw error instead
                        if(ioEStat/=0) then
                            call UMsg(-1,2505,rTemp)!could not read crystal direction...!TODO fix errormessage
                            exit
                        endif
                end select
            end do readloop

            close(uNRSF)

            call WriteLogAndStdO("Finished Reading Auxiliary Structure File")
        end subroutine ReadAuxStru

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Subroutines to read from Structure files!TODO handle reading errors
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !Reads Structure from extended XYZ file... or xyz file....
        subroutine ReadEXYZ(struFN)
            use NaraData,only:ANum,ASort,SData
            character(len=*),intent(in):: struFN!Structure file Name

            integer(kind=KDefI):: ioEStat,uNRSF, i, j
            integer(kind=KDefI):: inde!first empty slot in asortTemp
            character(len=EMsgLen):: ioEMsg
            character(len=3):: aType
            character(len=30):: keywords
            logical(kind=KDefL):: pbc,virt
            real(kind=KPosi),dimension(3):: maxlen! get max coordinate of atoms
            character(len=3),allocatable,dimension(:)::asortTemp!Temp Array for ASort

            call WriteLogAndStdO("Trying to read from Structure file "//trim(struFN))


            maxlen=0.0_KPosi
            pbc=.FALSE.
            virt=.FALSE.
            inde=1

            ioEStat=0
            call InitOpenR(struFN)!test and init open
            open(newunit=uNRSF,file=struFN,status="old",action="read",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(106,ioEStat,struFN,ioEMsg)!error while opening the file

            read(uNRSF,*) SData%nAtoms!get number of atoms

            allocate(SData%aData(SData%nAtoms),asortTemp(SData%nAtoms))



            read(uNRSF,'(A)') keywords
            if(index(keywords,"%PBC")>0) pbc=.TRUE.
            if(index(keywords,"%VIRTUAL")>0) virt=.TRUE.!TODO implement virtual atom handling

            do i=1,SData%nAtoms
                read(uNRSF,*) aType,SData%aData(i)%aPos(:)
                maxlen(1)=max(maxlen(1),SData%aData(i)%aPos(1))!get maximum postion of atoms for the case that no UC is specified
                maxlen(2)=max(maxlen(2),SData%aData(i)%aPos(2))
                maxlen(3)=max(maxlen(3),SData%aData(i)%aPos(3))

                do j=1,inde!check if one atom with the same sort already exists.
                    if(aType==asortTemp(j))then!already in ASort
                        SData%aData(i)%iaSort=j
                        exit
                    elseif(j==inde)then!add new type to ASort
                        asortTemp(j)=aType
                        SData%aData(i)%iaSort=j
                        inde=inde+1!next empty slot in ASort
                    end if
                end do
            end do

            if(pbc)then
                read(uNRSF,*)!skip blank line
                do i=1,3
                    read(uNRSF,*)keywords,SData%uCVectors(:,i)!read unit cell vectors
                enddo
                read(uNRSF,*)keywords,SData%uCOffset(:)!read unit cell offset
            else
                SData%uCVectors(:,:)=0.0_KPosi!set default values
                SData%uCOffset(:)=0.0_KPosi!set default values
                do i=1,3
                    SData%uCVectors(i,i)=maxlen(i)+1.0_KPosi !set default values
                enddo
            endif
            close(uNRSF)

            !copy Temp Array to ASort Array
            allocate(ASort(inde-1))
            ASort=asortTemp(:inde-1)
            deallocate(asortTemp)

            ANum=size(ASort)!Get number of different atom sorts
            call WriteLogAndStdO("Finished Reading Structure File")

        end subroutine ReadEXYZ


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!
!!!!! Main Reading Subroutines
!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !User callable version of read:
        subroutine ReadFiles()!Calls seeral subroutines to get the file names and read them in appropriate order.
            use NaraData, only: PData,PDataOrig

            character(len=FnML),dimension(:),allocatable:: fNA!Filename Array
            character(len=FnEL),dimension(:),allocatable:: fNEA!Filename extension array
            character(len=FnML)::fNAux!working copy to get auxiliary structure file name
            integer(kind=KDefI):: i, j, k,anzfiles
            integer(kind=KDefI):: strFileindex
            integer(kind=KDefI),allocatable,dimension(:):: intF,intFCopy!interaction files
!    integer(kind=KDefI),allocatable,dimension(:):: strF,strFCopy
            logical(kind=KDefL),dimension(4):: allFiles!checks if at least one file from  each required type is present


            allFiles=.FALSE.

            call ReadMainF(fNA,fNEA)!get all filenames and filename extensions

            anzfiles=size(fNEA)!number of recognized files
            if(anzfiles<4) call HCF(104,4-anzfiles,"At least one File is missing.")
            do i=1,anzfiles
                select case (trim(adjustl(fNEA(i))))
                    case("Cnara")
                        if(allFiles(1)) call HCF(105,1,"More than one Cnara files were specified.")
                        allFiles(1)=.TRUE.
                        call ReadConfig(fNA(i))
                        call WriteLogConfig()
                    case("Pnara")
                        if(allFiles(4)) call HCF(105,1,"More than one Pnara files were specified.")
                        allFiles(4)=.TRUE.
                        call ReadParam(fNA(i),PData)
                        !copy PData
                        allocate(PDataOrig(size(PData)))
                        PDataOrig=PData
                        call WriteLogPara()
                    case("Inara")
                        allFiles(2)=.TRUE.
                        !save index in array for later, since interaction files have to be read after structure files
                        if(allocated(intF)) then
                            j=size(intF)
                            allocate(intFCopy(j))
                            intFCopy=intF
                            deallocate(intF)
                            allocate(intF(j+1))
                            intF(:j)=intFCopy
                            deallocate(intFCopy)
                            j=j+1
                        else
                            allocate(intF(1))
                            j=1
                        end if
                        intF(j)=i!
                    case("xyz")
                        if(allFiles(3)) call HCF(105,1,"More than one Structure files were specified.")
                        allFiles(3)=.TRUE.
                        strFileindex=i!copy only index, to read structure and auxiliary structure files later
                    case("Snara")!ignore for now sice they should have the same file name as the structure file

                    case default
                        call HCFIO(18,2,trim(fNA(i)),"")!unrecognisable file extension
                end select
            end do

            if(.NOT.allFiles(1)) call HCF(104,-1,"Missing Configuration file")
            if(.NOT.allFiles(2)) call HCF(104,-2,"Missing Interaction file")
            if(.NOT.allFiles(3)) call HCF(104,-3,"Missing Structure file")
            if(.NOT.allFiles(4)) call HCF(104,-4,"Missing Parameter file")

            !Read structure and auxiliary structure files
            select case (fNEA(strFileindex))!maybe in future I'll add support for additional structure files
                case("xyz")
                    call ReadEXYZ(fNA(strFileindex))
                    k=index(fNA(strFileindex),"xyz")
                    fNAux=fNA(strFileindex)(:k-1)//"Snara"!Get Auxiliary Structure file name
                    call ReadAuxStru(fNAux)
                case default
            end select

            call WriteLogStructure()

            j=size(intF)!is initialized...
            do i=1,j
                call ReadInterac(fNA(intF(i)))
            end do
            deallocate(intF)
            call WriteLogInter()

        end subroutine ReadFiles


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! subroutines write and read system state to binary file
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        subroutine SaveStateMin()!Saves the state of the system to RAM (Spin direction)
            use NaraData,only:CopySpinP,SpinP,PNum
            integer(kind=KInde)::i

            if(.NOT.allocated(CopySpinP)) allocate(CopySpinP(3,PNum))
            do i=1,PNum!copy data
                CopySpinP(:,i)=SpinP(i)%sDir
            end do
        end subroutine SaveStateMin

        subroutine RestoreStateMin()!Resets the state of the system (Spin direction) to the previously saved state
            use NaraData,only:CopySpinP,SpinP,PNum
            integer(kind=KInde)::i

            if(.NOT.allocated(CopySpinP)) return!if SaveStateMin was not called previously ignore call
            do i=1,PNum!restore data
                SpinP(i)%sDir=CopySpinP(:,i)
            end do
        end subroutine RestoreStateMin


        subroutine SaveStateFile(fname)!Saves the state of the system to a Binary File (Spin direction only)
            use NaraData,only:SpinP,PNum
            character(len=*),intent(in)::fname

            integer(kind=KInde)::i,unBF
            integer(kind=KDefI):: ioEStat
            character(len=IOEMsgLen):: ioEMsg

            call InitOpenW(fname)
            open(newunit=unBF,file=fname,access="stream",action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(115,ioEStat,fname,ioEMsg)!error while opening the file

            do i=1,PNum
                write(unBF) SpinP(i)%sDir
            end do

            close(unBF)
        end subroutine SaveStateFile

        subroutine RestoreStateFile(fname)!Restores the state of the system from a Binary File (Spin direction only)!TODO add checks
            use NaraData,only:SpinP,PNum
            character(len=*),intent(in)::fname

            integer(kind=KInde)::i,unBF
            integer(kind=KDefI):: ioEStat
            character(len=IOEMsgLen):: ioEMsg

            call InitOpenR(fname)
            open(newunit=unBF,file=fname,access="stream",action="read",status="old",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(116,ioEStat,fname,ioEMsg)!error while opening the file

            do i=1,PNum
                read(unBF) SpinP(i)%sDir
            end do

            close(unBF)
        end subroutine RestoreStateFile

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! subroutines to write header for save files
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        subroutine HeaderWNaraVersion(uNFile)!Writes line with Naras version number to file
            integer(kind=KDefI),intent(in)::uNFile!unit number
            write(uNFile,*) "Nara Version: "//VERSION
        end subroutine HeaderWNaraVersion


        subroutine HeaderWColumnNames(uNFile,fType)!Writes line with column names
            use NaraData,only: CData
            integer(kind=KDefI),intent(in)::uNFile!unit number
            integer(kind=KDefI),intent(in)::fType!file type: 0 Statistics;  1 System Energy; 2 atom Energy; 3 System Magnetic1; 4 Atom Magnetic1; 5 System Magnetic2; 6+ Atom Magnetic2
            character(len=300)::headerstring

            headerstring=""

            if(CData%unitConv)then
                headerstring="Temperature[K] Hfield_x[A/m] Hfield_y[A/m] Hfield_z[A/m]"
            else
                headerstring="Temperature Hfield_x Hfield_y Hfield_z"
            end if

            select case(fType)
                case(0)!statistics file
                    headerstring=trim(headerstring)//" MCStepsTherm MCStepsMeasur AccFlip AccDeviation AccRandom AccTotal"
                case(1:2)!System & Atom energy file
                    if(CData%unitConv)then
                        headerstring=trim(headerstring)//&
                        &" Energy[meV] SpecificHeat[meV/K] BinderCEner[1]"
                    else
                        headerstring=trim(headerstring)//&
                        &" Energy SpecificHeat BinderCEner"
                    end if
                case(3:4)!System & atom Magnetic 1 File
                    if(CData%siMagUnit)then
                        headerstring=trim(headerstring)//&
                        &" Magnetization_x[A/m] Magnetization_y[A/m] Magnetization_z[A/m]"
                    else
                        headerstring=trim(headerstring)//&
                        &" Magnetization_x Magnetization_y Magnetization_z"
                    end if
                case(5)!System Magnetic 2 file
                    if(CData%siMagUnit)then
                        headerstring=trim(headerstring)//&
                        &" TotMagnetization[A/m] Susceptibility[1] BinderCTMag[1]"//&
                        &" Stagg.Magnet.[A/m] Stagg.Suscept.[1] BinderCSMag[1]"
                    else
                        headerstring=trim(headerstring)//&
                        &" TotMagnetization Susceptibility BinderCTMag"//&
                        &" Stagg.Magnet. Stagg.Suscept. BinderCSMag"
                    end if

                case default!Atom Magnetic 2 file
                    if(CData%siMagUnit)then
                        headerstring=trim(headerstring)//&
                        &" TotMagnetization[A/m] Susceptibility[1] BinderCTMag[1]"
                    else
                        headerstring=trim(headerstring)//&
                        &" TotMagnetization Susceptibility BinderCTMag"
                    end if
            end select

            write(uNFile,*)headerstring
        end subroutine HeaderWColumnNames

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! subroutines to save data
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        subroutine InitSaveStats()!initilizes Stats file
            use NaraData,only: FnStats,UNStats

            integer(kind=KDefI):: ioEStat
            character(len=IOEMsgLen):: ioEMsg

            call InitOpenW(FnStats)
            open(newunit=UNStats,file=FnStats,action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(107,ioEStat,FnStats,ioEMsg)!error while opening the file


            write(UNStats,*) "Statistics File Contains Information on acceptance rate of trial steps and number of used MCSteps"
            call HeaderWNaraVersion(UNStats)
            call HeaderWColumnNames(UNStats,0)
        end subroutine InitSaveStats


        subroutine InitSaveObservable()!initializes the output file for the observables
            use NaraData,only:FEDat,FnDat,UNDat,ANum,ASort
            integer(kind=KDefI)::i
            character(len=2+AtNL+len(FnDat(1))+len(FEDat))::fname
            integer(kind=KDefI):: ioEStat
            character(len=IOEMsgLen):: ioEMsg

            allocate(UNDat(3,0:ANum))
            do i=1,ANum
                !open file Magnetic Properties
                fname=FnDat(1)//"_"//trim(adjustl(ASort(i)))//FEDat
                call InitOpenW(fname)
                open(newunit=UNDat(1,i),file=fname,action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
                if(ioEStat/=0) call HCFIO(108,ioEStat,fname,ioEMsg)!error while opening the file
                !write header
                write(UNDat(1,i),*)"Atom-specific Magnetic observables: "//ASort(i)
                call HeaderWNaraVersion(UNDat(1,i))
                call HeaderWColumnNames(UNDat(1,i),4)

                !open file Energy Properties
                fname=FnDat(2)//"_"//trim(adjustl(ASort(i)))//FEDat
                call InitOpenW(fname)
                open(newunit=UNDat(2,i),file=fname,action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
                if(ioEStat/=0) call HCFIO(109,ioEStat,fname,ioEMsg)!error while opening the file
                !write header
                write(UNDat(2,i),*)"Atom-specific Energy observables: "//ASort(i)
                call HeaderWNaraVersion(UNDat(2,i))
                call HeaderWColumnNames(UNDat(2,i),2)

                !open file 2nd Magnetic Properties..susceptibility and total magnetization
                fname=FnDat(3)//"_"//trim(adjustl(ASort(i)))//FEDat
                call InitOpenW(fname)
                open(newunit=UNDat(3,i),file=fname,action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
                if(ioEStat/=0) call HCFIO(110,ioEStat,fname,ioEMsg)!error while opening the file
                !write header
                write(UNDat(3,i),*)"Atom-specific Magnetic observables: "//ASort(i)
                call HeaderWNaraVersion(UNDat(3,i))
                call HeaderWColumnNames(UNDat(3,i),6)
            end do
            !explicitly for the System

            !open file Magnetic Properties
            fname=FnDat(1)//"_System"//FEDat
            call InitOpenW(fname)
            open(newunit=UNDat(1,0),file=fname,action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(111,ioEStat,fname,ioEMsg)!error while opening the file
            !write header
            write(UNDat(1,0),*)"Magnetic observables of the System"
            call HeaderWNaraVersion(UNDat(1,0))
            call HeaderWColumnNames(UNDat(1,0),3)

            !open file Energy Properties
            fname=FnDat(2)//"_System"//FEDat
            call InitOpenW(fname)
            open(newunit=UNDat(2,0),file=fname,action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(112,ioEStat,fname,ioEMsg)!error while opening the file
            !write header
            write(UNDat(2,0),*)"Energy observables of the System"
            call HeaderWNaraVersion(UNDat(2,0))
            call HeaderWColumnNames(UNDat(2,0),1)

            !open file 2nd Magnetic Properties..susceptibility and total magnetization
            fname=FnDat(3)//"_System"//FEDat
            call InitOpenW(fname)
            open(newunit=UNDat(3,0),file=fname,action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(113,ioEStat,fname,ioEMsg)!error while opening the file
            !write header
            write(UNDat(3,0),*)"Magnetic observables of the System"
            call HeaderWNaraVersion(UNDat(3,0))
            call HeaderWColumnNames(UNDat(3,0),5)
        end subroutine InitSaveObservable


        subroutine SaveStats(ndata)!saves Statistics data
            use NaraData,only: UNStats,NMCSteps,AccSteps
            use NaraData,only: Para
            integer(kind=KDefI),intent(in)::ndata!number of dp to save

            integer(kind=KQInd)::i

            do i=1,ndata
                write(UNStats,*) Para(:,i), NMCSteps(:,i),AccSteps(:,i)
            end do
        end subroutine SaveStats

        subroutine SaveObservables(ndata)!writes data to the output file
            use NaraData,only:MeanEner,SpecHeat
            use NaraData,only:MeanMag,MeanTotMag,Suscept
            use NaraData,only:MeanStagMag,StagSuscept
            use NaraData,only:UNDat,ANum,Para
            use NaraData,only:BinderEner,BinderStagMag,BinderTotMag
            integer(kind=KQInd),intent(in)::ndata!number of dp to save
            integer(kind=KQInd)::j
            integer(kind=KDefI)::i

            if(.NOT. allocated(UNDat)) call InitSaveObservable()!if never initialized initialize them first.

            !write data to files
            do i=0,ANum
                do j=1,ndata
                    write(UNDat(1,i),*) Para(:,j), MeanMag(:,i,j)
                end do
                do j=1,ndata
                    write(UNDat(2,i),*) Para(:,j), MeanEner(i,j), SpecHeat(i,j),BinderEner(i,j)
                end do
            end do

            do j=1,ndata
                write(UNDat(3,0),*) Para(:,j), MeanTotMag(0,j), Suscept(0,j),BinderTotMag(0,j)&
                &,MeanStagMag(j),StagSuscept(j),BinderStagMag(j)
            end do
            do i=1,ANum
                do j=1,ndata
                    write(UNDat(3,i),*) Para(:,j), MeanTotMag(i,j), Suscept(i,j),BinderTotMag(i,j)
                end do
            end do

        end subroutine SaveObservables

        !Creates Descreptive File for Raw Measurement Data
        subroutine InitSaveMeasurements()
            use NaraData,only: UNMea,ANum,CData,FEDat,APSNum,UCMagn,ASort
            integer(kind=KDefI):: ioEStat
            character(len=IOEMsgLen):: ioEMsg
            character(len=15)::fname

            write(fname,*)"RawDatDes"// FEDat
            call InitOpenW(fname)
            open(newunit=UNMea,file=fname,action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(114,ioEStat,fname,ioEMsg)!error while opening the file

            write(UNMea,*) CData%NAver, ANum+1!number of measurements and number of different data entries
            write(UNMea,*) APSNum!number of atoms per type
            write(UNMea,*) "System ",ASort!descriptive text of Atom sort
            write(UNMea,*) UCMagn!unitconversion factor for magnetization and staggered magnetization
            write(UNMea,'(z0)') UCMagn!additional write hex representation (Big Endian) to get full precision.
            close(UNMea)
        end subroutine InitSaveMeasurements




        !Subroutine Saves the Measurements to a binary file
        !Layout: #Measurements,#Different Measurements per Data
        !Columns: Mag-xyz component: System + all atoms; StagMag-xyz, Ener: System + all atoms
        subroutine SaveMeasurements()
            use NaraData,only: UNMea,FERawDat
            use NaraData,only: Ener,StagMag,Mag

            integer(kind=KDefI),save::counter=0!counts the number of calls to this subroutine and determines the file name
            integer(kind=KDefI):: ioEStat
            character(len=IOEMsgLen):: ioEMsg
            character(len=15)::fname

            counter=counter+1

            write(fname,'(I8.8,A5)')counter,FERawDat
            call InitOpenW(fname)
            open(newunit=UNMea,file=fname,access="stream",action="write",status="replace",iostat=ioEStat,iomsg=ioEMsg)
            if(ioEStat/=0) call HCFIO(114,ioEStat,fname,ioEMsg)!error while opening the file

            !write Data block: Mag-xyz component:System....all atoms; StagMag-xyz,EnerSystem...Asort
            write(UNMea) Mag,StagMag,Ener
            close(UNMea)
        end subroutine SaveMeasurements

        subroutine FinFiles()!closes files Observables Files and stats file (if opened)
            use NaraData,only: UNDat,ANum,UNStats
            integer(kind=KDefI)::i
            logical(kind=KDefL)::isopen

            do i=0,ANum
                close(UNDat(1,i))
                close(UNDat(2,i))
                close(UNDat(3,i))
            end do
            deallocate(UNDat)

            inquire(unit=UNStats,opened=isopen)!close Stats file if it was opened
            if(isopen) close(UNStats)

        end subroutine FinFiles

end module NaraIO!
