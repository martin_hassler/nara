!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Collection of general constants
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!FIXME KInde, KDefI, KQInd...usage
module MHKonstanten
    implicit none
    save
    public
!    private :: rprecs, rprecd, rprece, rprecq
!    private :: iprecb, iprecw, iprecd, iprecq, ipreco


    character(len=*),parameter::VERSION="0.6.0"
    character(len=*),parameter::CYEARS="2018-2021"

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Definition of the Kind parameters, if possible do not use directly
!!!! Deliberately not using iso_fortran_env kinds because they may not be portable:
!!!! https://stevelionel.com/drfortran/2017/03/27/doctor-fortran-in-it-takes-all-kinds/
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!! Real Kinds
    integer, parameter:: rprecs = selected_real_kind(6,37)!Single
    integer, parameter:: rprecd = selected_real_kind(15,307)!Double Gfortran Standard
!    integer, parameter:: rprece = selected_real_kind(18)!Extended....Do not use
    integer, parameter:: rprecq = selected_real_kind(33,4931)!Quadruple
!!! Integer KINDs
    integer, parameter:: iprecb = selected_int_kind(0)!Byte -128 to 127
    integer, parameter:: iprecw = selected_int_kind(4)!Word -32.768 to 32.767
    integer, parameter:: iprecd = selected_int_kind(9)!Double Word -2.147.483.648 to 2.147.483.647 Gfortran Standard
    integer, parameter:: iprecq = selected_int_kind(18)!Quad Word -9.223.372.036.854.775.808 to 9.223.372.036.854.775.807
!    integer, parameter:: ipreco = selected_int_kind(30)!Octa Word ca. -1,70141*10**38 to ca. 1,70141*10**38; NOT Supported by every compiler
!!! No Char Kinds since it is not part of F95 Standard


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Used Kind Parameters
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer, parameter:: KDefI = iprecd !default Kind integer
    integer, parameter:: KDefR = rprecd !default Kind real
    integer, parameter:: KDefL = iprecd !default Kind logical
    integer, parameter:: KDefC = rprecd !default Kind complex

    !FIXME drawn random numbers for the spin direction are currently limited to 32 bit float... if you change rprecs to rprecd you also have to change the called routines
    !FIXME alternatively I could provide an interface to the routine... might do that in the future
    integer, parameter:: KSDir = rprecs !Kind Spin direction ATTENTION if changed you have to change the kind of Pi in NaraData

    integer, parameter:: KInMa = KSDir  !Kind of the Interaction Matrices
    integer, parameter:: KCalc = rprecd !Kind to calculate quantities, should be larger than KSDir
    integer, parameter:: KCMin = rprecs !Kind to calculate unimportant quantities, such as acceptance probability
    integer, parameter:: KPara = KCalc  !Kind for input Parameters Temperature and H field
    integer, parameter:: KSpar = KCalc  !Kind for stuff that should not be a default kind and may need its own parameter in the future.

    integer, parameter:: KPosi = rprecd !Kind for position of particles
    integer, parameter:: KInde = iprecd !Kind for the index of the AtomList and related stuff
    integer, parameter:: KQInd = iprecd !Kind for the index of saved quantities

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Definition of Constants
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!! Pi in different precisions
    real(kind=rprecs),parameter::Pi_s=16.0_rprecs*atan(0.2_rprecs)-4.0_rprecs*atan(1.0_rprecs/239.0_rprecs)
    real(kind=rprecs),parameter::Pi2_s=32.0_rprecs*atan(0.2_rprecs)-8.0_rprecs*atan(1.0_rprecs/239.0_rprecs)!single precision 2*Pi
    real(kind=rprecd),parameter::Pi_d=16.0_rprecd*atan(0.2_rprecd)-4.0_rprecd*atan(1.0_rprecd/239.0_rprecd)
!    real(kind=rprece),parameter::Pi_e=16.0_rprece*atan(0.2_rprece)-4.0_rprece*atan(1.0_rprece/239.0_rprece)
    real(kind=rprecq),parameter::Pi_q=16.0_rprecq*atan(0.2_rprecq)-4.0_rprecq*atan(1.0_rprecq/239.0_rprecq)

    !magic numbers for integer to float conversion....
    !corresponds to the bitpattern of the float representing 1.0
    integer(kind=iprecq),parameter::r64exp=4607182418800017408_iprecq !hex 3FF0000000000000
    integer(kind=iprecq),parameter::r32expA=1065353216_iprecq !hex 3F800000
    integer(kind=iprecq),parameter::r32expA2t4=int(z'40000000',iprecq)!exponent to for values between in Interval [2,4)

    integer(kind=iprecq),parameter::r64mant=2_iprecq**52-1_iprecq!all bits of the mantissa set in a corresponding float
    integer(kind=iprecq),parameter::r32mantA=2_iprecq**23-1_iprecq!all bits of the mantissa set in a corresponding float


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Physical of Constants (Source NIST/CODATA)
!!!! Title:         The 2018 CODATA Recommended Values of the Fundamental Physical Constants
!!!! Author:        Eite Tiesinga and Peter J. Mohr and David B. Newell and Barry N. Taylor
!!!! Date:          2019
!!!! Organization   National Institute of Standards and Technology, Gaithersburg, MD 20899
!!!! Url:           https://physics.nist.gov/cuu/Constants/index.html
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    real(kind=rprecd),parameter::KBoltzEV=8.617333262E-5_rprecd!k_B Boltzmann constant in eVK^-1 20.05.2019 https://physics.nist.gov/cgi-bin/cuu/Value?kev
    real(kind=rprecd),parameter::KBoltzmEV=8.617333262E-2_rprecd!k_B Boltzmann constant in meVK^-1

    real(kind=rprecd),parameter::BmagnetJ=927.40100783E-26_rprecd!mu_B Bohr magneton in JT^-1=Am^2 20.05.2019 https://physics.nist.gov/cgi-bin/cuu/Value?mub
    real(kind=rprecd),parameter::BmagnetEV=5.7883818060E-5_rprecd!mu_B Bohr magneton in eVT^-1 20.05.2019 https://physics.nist.gov/cgi-bin/cuu/Value?mubev
    real(kind=rprecd),parameter::BmagnemEV=5.7883818060E-2_rprecd!mu_B Bohr magneton in meVT^-1 20.05.2019 https://physics.nist.gov/cgi-bin/cuu/Value?mu0

    real(kind=rprecd),parameter::VacuumPer=1.25663706212E-6_rprecd!mu_0 magnetic constant/Vacuum permeability (TmA^-1) 20.05.2019 https://physics.nist.gov/cgi-bin/cuu/Value?mu0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Other Parameters
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer(kind=KDefI), parameter:: AtNL=6!maximum Atom name Length in Cell includes elementsymbol+unique atom
    real(kind=KSDir),parameter:: Pi=Pi_s!TODO New Kind !FIXME get right precision automatically

    real(kind=KCalc),parameter::EpsKCalc=epsilon(1.0_KCalc)*1000

end module MHKonstanten
