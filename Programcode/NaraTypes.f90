!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Ha�ler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!! Module containing the declaration of derived types
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module NaraTypes
    use MHKonstanten

    implicit none

    public


    !Properties used in calculation of the current Energy
    type:: SProp
        real(kind=KSDir), dimension(3):: sDir !XYZ-Spin Component
        !TODO *
        integer(kind=KInde):: anzNeigh!Number of Neighbours
        integer(kind=KInde),allocatable,dimension(:,:):: indList!Index List Contains List with indices to Neighbouring Spins (1,:) and the used ExchTensor (2,:)
        integer(kind=KInde),dimension(1)::pInde!parameter Indices to certain arrays: 1: index of SIA Matrix, 2:
        integer(kind=KDefI)::iaSort!Particle sort, index of aSort array
        logical(kind=KDefL)::subLat!.T. sublattice 1;  .F. sublattice 2
    end type SProp


    !Properties not that often used than the ones in SProp
    type:: SProp2
        real(kind=KPosi), dimension(3):: partPos!XYZ-Position of Atom
        integer(kind=KDefI)::aDatInd !index to AtomData 1: structuradata index, 2: atom data index
    end type SProp2

    !only the ExchangeTensor... to define mixed array with allocatable dimension...
    type:: ExchTen
        real(kind=KInMa),dimension(3,3)::exTen!Exchange Tensor
    end type ExchTen

    !only the symmetric 3x3 matrix only upper triangle is stored... used for SIA
    type:: SymMat
        real(kind=KInMa),dimension(6)::intMat!Upper triangular Interaction Matrix [m(1,1);m(1,2);m(1,3);m(2,2,);m(2,3);m(3,3)]
    end type SymMat

    type:: ExchVec!diagonal of exchange matrix for Heisenberg exchange
        real(kind=KInMa),dimension(3)::exTen!Exchange Tensor
    end type ExchVec

    type:: ConfigData!Contains the raw data read from files before setting up the simulation environment
        integer(kind=KQInd):: NAver=1000!Number of measurments to average over for each datapoint
        integer(kind=KDefI):: NMinTherm=1200!minimum Number of iterations before collecting data thermalization
        integer(kind=KDefI):: NMaxTherm=100000!maximum Number of iterations before collecting data thermalization
        integer(kind=KDefI):: NPreTherm=1000!Number of additional iterations for the first thermalization of a new system
        integer(kind=KDefI):: NmAv=100!number of runs for moving averages
        integer(kind=KDefI):: NmAvDel=1000!delay between moving averages
        integer(kind=KDefI):: NMCSweep=3!Number of MCSweep per measurement

        real(kind=KCalc):: RelChOPa=0.001_KCalc! maximum relative change in order Parameter between current run and moving average as criteria for thermalization

        real(kind=KSDir):: ConeAngle=0.5235987755982988730_KSDir!Angle (half of the opening angle) of the cone intersecting with the sphere in 30� radiants
        real(kind=KCalc):: WMDevi=0.5_KCalc!weights to determine which MC trial step is used
        real(kind=KCalc):: WMFlip=0.2_KCalc!weights to determine which MC trial step is used
        real(kind=KCalc):: WMRand=0.3_KCalc!weights to determine which MC trial step is used

        integer(kind=KDefI),dimension(3)::BCs=(/1,1,1/)!Boundary conditions for the x, y and z axis 1:PBC; 0:no interaction

        integer(kind=KDefI):: autTherm=0!sets type of thermalization 0: forced therm.. fixed no of Mc steps,else automatic therm: 1 considering only TotMag, 2 StagMag, 3 Mag+StagMag
        integer(kind=KDefI):: InitSpins=10!parameter to specify how the spins should be initialized
        integer(kind=KDefI):: PRNG=1!PRNG to use... 0=language default 1=supplied dedicated one
        integer(kind=KDefI):: subLatgen=1!describes on what basis the sublattice is generated/determined

        logical(kind=KDefL):: advTherm=.FALSE.!if .T. advanced thermalization is enabled.... the number of MC steps during therm can change... NMinTherm is still met. if .F. NMaxTherm steps are carried out.
        logical(kind=KDefL):: enerBinder=.FALSE.!.T. calculate binder cumulant of energy
        logical(kind=KDefL):: siMagUnit=.TRUE.!.T. if units of mag. obs are  SI units; .F. it uses arbitrary units for mag obs (tot mag is between 0 and 1).... only effective if unitConv=.T.
        logical(kind=KDefL):: stagMBinder=.FALSE.!.T. calculate binder cumulant of staggered magnetization
        logical(kind=KDefL):: stradMBinder=.FALSE.!.F. calculates connected Binder instad of the traditional binder cumulant
        logical(kind=KDefL):: saveRaw=.FALSE.!.T. save raw data of measurements, .F. don't save them
        logical(kind=KDefL):: totMBinder=.FALSE.!.T. calculate binder cumulant of total magnetization
        logical(kind=KDefL):: ttradMBinder=.FALSE.!.F. calculates connected Binder instad of the traditional binder cumulant
        logical(kind=KDefL):: unitConv=.TRUE.!.T. if units should be converted; .F. if no conversion is wanted
    end type ConfigData

    type:: AtomPos!contains Information on one Atom
        real(kind=KPosi),dimension(3)::aPos!Atom Position in Cartesian coordinates
        integer(kind=KDefI)::iaSort!index of aSort array
        integer(kind=KInde),allocatable,dimension(:,:):: neighAt!each line contains three indices to describe the neighbouring cell and one to determine which atom of the basis, the last one indicates which Exch Mat has to be used.
        logical(kind=KDefL)::subLat=.False. !.T. sublattice 1;  .F. sublattice 2
        real(kind=KSDir),dimension(3)::defSdir=(/0.0_KSdir,0.0_KSdir,1.0_KSdir/)!default spin direction (can flip with sublattice) is used to initilize lattice if corresponding option was used in Cnara
    end type

    type:: StructData!Contains the data read from structure files
        integer(kind=KInde)::nAtoms!Number of Atoms in Structure File
        real(kind=KPosi),dimension(3,3)::uCVectors!unit cell vectors in Cartesian coordinates stored row-wise
        real(kind=KPosi),dimension(3)::uCOffset!unit cell Offset in Cartesian coordinates
        type(AtomPos),allocatable,dimension(:)::aData!Atom Data
        integer(kind=KInde),dimension(3):: NCells!Number of unit cells in crystal for each lattice base vector
        integer(kind=KDefI),dimension(3):: sublatSpindir=(/0,0,1/)!Crystal direction in miller indices, spins are parallel or anti parallel to this direction initialized, (depending on the sublattice and if the corresponding option in Cnara is chosen
        integer(kind=KDefI),dimension(3):: sublatPlane=(/0,0,1/)!Sublattice depending on crystal planes in miller indices, (if the corresponding option in Cnara is chosen)
    end type

    type:: IntSh!contains the necessary interaction data ... interaction within a spherical shell
        real(kind=KPosi),dimension(2)::rad!minimum and maximum interaction radius
        integer(kind=KDefI)::eInd!index of interaction matrix
        integer(kind=KDefI)::iaSort!index of aSort array
    end type

    type:: InterData!contains interaction Data
        type(IntSh),allocatable,dimension(:)::twopart!two particle interaction within spherical shell; index matches interacting particle in ASort
        integer(kind=KInde)::pInde = 0!parameter Indices to certain arrays: 1: index of SIA Matrix... default value points to zero matrix
        !future addition: self interaction multi particle interaction etc.
    end type

    type:: ParaData!contains parameter Data
        logical(kind=KDefL)::savestate = .FALSE. !if .T. saves the state of the system after finishing this set Parameters
        logical(kind=KDefL)::runlstate = .FALSE. !if .T. this set of parameters will take the last saved state as starting point
        logical(kind=KDefL)::newsimrun = .FALSE. !indicates if this is new set of data points. starts new simulation
        integer(kind=KQInd)::ndata!Number of datapoints including endpoint excluding the starting point...
        real(kind=KPara)::temppoint! Temperature
        real(kind=KPara),dimension(3)::hpoint!external Magnetic field
    end type ParaData

end module NaraTypes
