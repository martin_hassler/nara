# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 21:28:42 2020

!--------------------------------------------------------------------------------------------------------------------
! Nara version 0.6.0
! An implementation of a classical Heisenberg code with different types of interactions based on Monte Carlo methods.
! Copyright (C) 2018-2021  Martin F.T. Haßler
!
! This file is part of Nara.
!
! Nara is free software: you can redistribute it and/or modify
! it under the terms of the GNU Affero General Public License as
! published by the Free Software Foundation, either version 3 of the
! License, or (at your option) any later version.
!
! Nara is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Affero General Public License for more details.
!
! You should have received a copy of the GNU Affero General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!--------------------------------------------------------------------------------------------------------------------
!
! Very crude script to read the raw data files...
!
!--------------------------------------------------------------------------------------------------------------------
@author: Martin F. T. Haßler
"""

import numpy as np

def ReadRawData(measurements,sorts,fname):
    """
    Extracts Raw Data from Nara raw data file.
    Returns The Measurement data for the Magnetization, Staggered MAgnetization and the Energy
    Parameters
    ----------
    measurements : Integer
        Number of Measurements (CDATA%NAver).
    sorts : Integer
        Number of different Measurements per observable = ANum + 1
    fname : string
        Filename

    Returns
    -------
    Mag : 3D float64 array
        Magnetization Index: 1st Measurmenet; 2nd Sorts; 3rd xyz-Component
    Stag : 2D float64 array
        Staggered Magnetization Index: 1st Measurmenet; 2nd xyz-Component.
    Ener : 2D float64 array
        Energy Index: 1st Measurmenet; 2nd Sorts

    """   
    elemMagn = 3 * measurements * sorts
    elemStag = 3 * measurements
    elemEner = sorts * measurements
    
    with open(fname, 'rb') as f:
        Mag=np.fromfile(f,np.float64,elemMagn).reshape(measurements,sorts,3)
        Stag=np.fromfile(f,np.float64,elemStag).reshape(measurements,3)
        Ener=np.fromfile(f,np.float64,elemEner).reshape(measurements,sorts)
    
    return Mag,Stag,Ener



Mag,Stag,Ener = ReadRawData(100000, 2, 'P1000001.rdat')

print(np.mean(Ener[:,0])/192.)#dived by particle number
print(np.mean(Mag[:,0,0])/192.)
print(np.mean(Mag[:,0,1])/192.)
print(np.mean(Mag[:,0,2])/5625.)
